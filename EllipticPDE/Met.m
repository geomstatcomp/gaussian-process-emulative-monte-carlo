%% Metric %%
function [G,invG,dG,ChS1,dlogrtG] = Met(theta,N,PDE,opt,sigma2y,sigma2theta)
if(nargin<4)
    opt=0;sigma2y=.01;sigma2theta=1;
elseif(nargin<5)
    sigma2y=.01;sigma2theta=1;
end
G=[];invG=[];dG=[];ChS1=[];dlogrtG=[];

D=length(theta);
Nmesh=sqrt(size(PDE.p,2)); % mesh size has to be (Nmesh,Nmesh)
obs_idx=linspace(1,Nmesh,sqrt(N));
[obs_mesh1,obs_mesh2]=meshgrid(obs_idx,obs_idx);
idx=sub2ind([Nmesh,Nmesh],obs_mesh2(:),obs_mesh1(:));
% forward model to solve elliptic PDE given theta
[~,dsol,d2sol]=ePDEsolver(theta,PDE,[0,1,2]);
ds=dsol(idx,:); % (N,D)
d2s=d2sol(idx,:,:); % (N,D,D)

if any(opt==0)
    G = (ds'*ds)./sigma2y;
    G(1:D+1:D^2) = G(1:D+1:D^2) + 1/sigma2theta;
end
if any(opt==-1)
    if all(opt~=0)
        error('G has to be calculated first!');
    end
    invG = inv(G);
end
if any(opt==1)
    dG = reshape(ds'*d2s(:,:),repmat(D,1,3));
    dG = dG + permute(dG,[2,1,3]);
    dG = dG./sigma2y;
end
if any(opt==3)
    if any(opt==1)
        ChS1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
    else
        ChS1 = reshape(d2s(:,:)'*ds,repmat(D,1,3))./sigma2y;
    end
end
if any(opt==-3)
    if all(opt~=-1)
        error('invG has to be calculated first!');
    end
    if any(opt==3)
        dlogrtG = sum(repmat(invG(:)',[D,1]).*ChS1(:,:),2);
    elseif any(opt==1)
        dlogrtG = sum(repmat(invG(:),[1,D]).*reshape(dG,D^2,D))'/2;
    else
        error('dG or ChS1 has be calculated first!');
    end
end

end