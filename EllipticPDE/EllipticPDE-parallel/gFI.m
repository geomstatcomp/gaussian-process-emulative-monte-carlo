% This is to compute the generalized empirical Fisher information %

function gFI_til=gFI(y,theta,PDE)

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end

N=length(y);
Nmesh=sqrt(size(PDE.p,2)); % mesh size has to be (Nmesh,Nmesh)
obs_idx=linspace(1,Nmesh,sqrt(N));
[obs_mesh1,obs_mesh2]=meshgrid(obs_idx,obs_idx);
idx=sub2ind([Nmesh,Nmesh],obs_mesh2(:),obs_mesh1(:));
s=zeros(n,N); ds=zeros(n,D*N); % store solutions
for i=1:n
    % forward model to solve elliptic PDE given theta
    [sol,dsol]=ePDEsolver(theta(i,:)',PDE,[0,1]);
    if ~isempty(sol)
        s(i,:)=sol(idx);
    end
    if ~isempty(dsol)
        dsol=dsol(idx,:)';
        ds(i,:)=dsol(:);
    end
end
ds=reshape(ds,[n*D,N]);

Err=s-repmat(y',[n,1]);
xL = -Err.^2/(2*.01); % (n,N)
dxL = -repmat(Err,[D,1]).*ds./.01; % (nD,N)

xL_til=[xL;dxL];
L_til=sum(xL_til,2);
gFI_til=xL_til*xL_til'-L_til*L_til'./N;

end