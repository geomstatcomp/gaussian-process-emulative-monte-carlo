%%%% This is generic GPeRHMC algorithm %%%%

function [q,u,met,acpY] = GPeRHMC(q_cur,u_cur,met_cur,U,Met,EMU,eps,L,Nfpiter)

% initialization
% true values
q = q_cur; D = length(q);
u = u_cur;
cholG=met_cur.cholG;
% emulations
G=met_cur.G;
invG=met_cur.invG;
dG=met_cur.dG;
dphi=met_cur.dphi;

% propose momentum
z = randn(D,1);
p = cholG'*z;

% Calculate current energy value
E_cur  = u + sum(log(diag(cholG))) + (z'*z)/2;

% randomize the number of Leapfrog steps
randL=ceil(rand*L);

% Perform leapfrog steps
for l = 1:randL
    
    %%%%%%%%%%%%%%%%%%%
    % Update momentum %
    %%%%%%%%%%%%%%%%%%%
    % Make a half step for momentum
    p_iter=p; invGp_iter=invG*p_iter;
    dK=zeros(D,1);
    for fpiter=1:Nfpiter
        for d=1:D
            dK(d)=-invGp_iter'*dG(:,:,d)*invGp_iter;
        end
        p_iter=p-eps/2.*(dphi+dK);
        invGp_iter=invG*p_iter;
    end
    p=p_iter;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%
    % Update q parameters %
    %%%%%%%%%%%%%%%%%%%%%%%
    % Make a full step for position
    q_iter=q; invGp_fix=invGp_iter; invGp=invGp_fix;
    for fpiter=1:Nfpiter
        q_iter=q+eps/2.*(invGp_fix+invGp);
        if fpiter~=Nfpiter
            [~,~,~,G] = EMU(q_iter,[1,11]);
        else
            [~,du,~,G,ChS1] = EMU(q_iter,[1,2,11,21]);
        end
        invG = inv(G);
        invGp=invG*p;
    end
    q=q_iter;
    
    % Update the metric and the related quantities using emulations
    dG=permute(reshape(ChS1,[D,D,D]),[1,3,2]); % not accurate, acutually half dG
    dphi = du + sum(repmat(invG(:)',[D,1]).*reshape(ChS1,D,D^2),2);
    
    %%%%%%%%%%%%%%%%%%%
    % Update momentum %
    %%%%%%%%%%%%%%%%%%%
    % Make aother half step for momentum
    for d=1:D
        dK(d)=-invGp'*dG(:,:,d)*invGp;
    end
    p=p-eps/2.*(dphi+dK);
    
end

try
    % Calculate proposed energy value
    % true values
    u = U(q);
    if ~isempty(Met)
        G_true=Met(q);
    else
        G_true=G; % if true G is not available, use the emulated one.
    end
    cholG=chol(G_true);
    E_prp = u + sum(log(diag(cholG))) + (p'/G_true*p)/2;

    % Accept according to ratio
    logRatio = -E_prp + E_cur;

    if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
        met.cholG=cholG; % true values
        met.G=G; met.invG=invG; met.dG=dG; met.dphi=dphi; % emulations
        acpY = 1;
    else
        q=q_cur; u=u_cur; met=met_cur;
        acpY = 0;
    end

catch
    q=q_cur; u=u_cur; met=met_cur;
    acpY = 0;
end

end
