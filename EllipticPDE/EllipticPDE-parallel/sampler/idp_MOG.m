% This is generic independence sampler-- mixture of Gaussians to determine
% the regeneration times when the transition kernel of MCMC can be adapted.

function [theta_idp,u_idp,acpY,regY]=idp_MOG(mog,theta_cur,u_cur,U,logC,reg_test)
if nargin<6
    reg_test=1; % default to do regeneration test when allowed
end

% parameters of mixture of Gaussians
mu=mog.mu; cholinvSigma=mog.cholinvSigma; wts=mog.wts;
[K,D]=size(mu);

% sample from mixture of Gaussians
chosen=datasample(1:K,1,'weights',wts); % choose one of the components
theta_idp=mu(chosen,:)'+cholinvSigma(:,:,chosen)\rand(D,1); % sample from MOG
% acceptance probability
logpiOq_cur=-u_cur-logpdf_MOG(theta_cur',mu,[],cholinvSigma,wts);
u_idp=U(theta_idp);
logpiOq_prp=-u_idp-logpdf_MOG(theta_idp',mu,[],cholinvSigma,wts);
acptPr_idp=min([1,exp(logpiOq_prp-logpiOq_cur)]);

if rand < acptPr_idp % theta_idp accepted
    % accept the indepence proposal
    acpY = 1;
    % do regeneration test
    if reg_test
        S=min([1,exp(-logpiOq_cur+logC)]);
        Qp=min([1,exp(logpiOq_prp-logC)]);
        regPr=S*Qp/acptPr_idp; % regneration probability
%         disp(['Regeneration probability: ',num2str(regPr)]);
        % test regeneration
        if rand<regPr % regeneration occurs
            regY = 1;
        else
            regY=0;
        end
    else
        regY=0;
    end
else
    % reject the indepence proposal
    acpY = 0; regY=0;
end

end

%%%%%%% functions to calcuate log density of MOG %%%%%%%%

function logdensity = logpdf_MOG(X,mu,Sigma,R,wts)
% R: R'*R=inv(Sigma)
% wts: (1,K)

K = size(mu,1);
N = size(X,1);

logp_K = zeros(N,K);
if ~isempty(R)
    for k=1:K
        logp_K(:,k) = logpdf_MVN(X,mu(k,:),[],R(:,:,k));
    end
else
    for k=1:K
        logp_K(:,k) = logpdf_MVN(X,mu(k,:),Sigma(:,:,k));
    end
end

logp_K = logp_K + repmat(log(wts),N,1);
logdensity = logsumexp(logp_K,2);
end

function logpdf = logpdf_MVN(X,mu,Sigma,R)
% X: N x D
% mu: 1 x D
% Sigma/invR: D x D
% R: R'*R=inv(Sigma)
if nargin < 4
    R=[];
end

if ~isempty(R)
    D = size(X, 2);
    X = bsxfun(@minus, X, mu);
    XR = X * R'; % N x D
    logrtdetinvSigma = sum(log(diag(R)));
    quadform = sum(XR.^2, 2);
    logpdf = -quadform./2 + logrtdetinvSigma - D/2*log(2*pi);
else
    logpdf = log(mvnpdf(X,mu,Sigma));
end
end

function s = logsumexp(x, dim)
% Returns log(sum(exp(x),dim)) while avoiding numerical underflow.
% Default is dim = 1 (columns).
% Written by Mo Chen (mochen@ie.cuhk.edu.hk). March 2009.
if nargin == 1, 
    % Determine which dimension sum will use
    dim = find(size(x)~=1,1);
    if isempty(dim), dim = 1; end
end

% subtract the largest in each column
y = max(x,[],dim);
x = bsxfun(@minus,x,y);
s = y + log(sum(exp(x),dim));
i = find(~isfinite(y));
if ~isempty(i)
    s(i) = y(i);
end
end