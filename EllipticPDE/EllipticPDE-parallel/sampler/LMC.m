%%%% This is generic LMC algorithm %%%%

function [q,u,met,acpY] = LMC(q_cur,u_cur,met_cur,U,Met,eps,L)

% initialization
q = q_cur; D = length(q);
u = u_cur;
G=met_cur.G;
cholinvG=met_cur.cholinvG;
ChS1=met_cur.ChS1;
dphi=met_cur.dphi;

% propose velocity
z = randn(D,1);
v = cholinvG'*z;

% Calculate current energy value
E_cur  = u + sum(log(diag(cholinvG))) + (z'*z)/2;

% Accumulate determinant to be adjusted in acceptance rate
dtlogJ = 0;

% randomize the number of Leapfrog steps
randL=ceil(rand*L);

% Perform leapfrog steps
for l = 1:randL
    
    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
    % Make a half step for velocity
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ - log(det(G+eps/2.*vChS1));
    v = (G+eps/2.*vChS1)\(G*v- eps/2.*dphi);
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ + log(det(G-eps/2.*vChS1));
    
    
    %%%%%%%%%%%%%%%%%%%%%%%
    % Update q parameters %
    %%%%%%%%%%%%%%%%%%%%%%%
    % Make a full step for position
    q = q + eps.*v;
    
    
    % Update the metric and the related quantities
    if l~=randL
        [G,~,~,ChS1,dlogrtG] = Met(q);
    else
        [G,invG,~,ChS1,dlogrtG] = Met(q);
    end
    dphi = U(q,1) + dlogrtG;
    
    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
    % Make aother half step for velocity
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ - log(det(G+eps/2.*vChS1));
    v = (G+eps/2.*vChS1)\(G*v- eps/2.*dphi);
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ + log(det(G-eps/2.*vChS1));
    
end

try
    % Calculate proposed energy value
    u = U(q); cholinvG = chol(invG);
    E_prp = u + sum(log(diag(cholinvG))) + (v'*G*v)/2;

    % Accept according to ratio
    logRatio = -E_prp + E_cur + real(dtlogJ);

    if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
        met.G=G; met.cholinvG=cholinvG; met.ChS1=ChS1; met.dphi=dphi;
        acpY = 1;
    else
        q=q_cur; u=u_cur; met=met_cur;
        acpY = 0;
    end
catch
    q=q_cur; u=u_cur; met=met_cur;
    acpY = 0;
end
    
end
