%%%% This is generic GPeLMC algorithm %%%%

function [q,u,met,acpY] = GPeLMC(q_cur,u_cur,met_cur,U,Met,EMU,eps,L)

% initialization
% true values
q = q_cur; D = length(q);
u = u_cur;
cholG=met_cur.cholG;
% emulations
G=met_cur.G;
ChS1=met_cur.ChS1;
dphi=met_cur.dphi;

% propose velocity
z = randn(D,1);
v = cholG\z;

% Calculate current energy value
E_cur = u - sum(log(diag(cholG))) + (z'*z)/2;

% Accumulate determinant to be adjusted in acceptance rate
dtlogJ = 0;

% randomize the number of Leapfrog steps
randL=ceil(rand*L);

% Perform leapfrog steps
for l = 1:randL
    
    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
    % Make a half step for velocity
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ - log(det(G+eps/2.*vChS1));
    v = (G+eps/2.*vChS1)\(G*v- eps/2.*dphi);
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ + log(det(G-eps/2.*vChS1));
    
    
    %%%%%%%%%%%%%%%%%%%%%%%
    % Update q parameters %
    %%%%%%%%%%%%%%%%%%%%%%%
    % Make a full step for position
    q = q + eps.*v;
    
    
    % Update the metric and the related quantities using emulation
    [~,du,~,G,ChS1] = EMU(q); invG = inv(G); ChS1=reshape(ChS1,D,D^2);
    dphi = du + sum(repmat(invG(:)',[D,1]).*ChS1,2);
%     [G,~,ChS1,dlogrtG] = Met(q);
%     dphi = U(q,1) + dlogrtG;
    
    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
    % Make aother half step for velocity
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ - log(det(G+eps/2.*vChS1));
    v = (G+eps/2.*vChS1)\(G*v- eps/2.*dphi);
    vChS1=reshape(v'*ChS1(:,:),D,D)';
    dtlogJ = dtlogJ + log(det(G-eps/2.*vChS1));
    
end

try
    % Calculate proposed energy value
    % true values
    u = U(q);
    if ~isempty(Met)
        G_true=Met(q);
    else
        G_true=G; % if true G is not available, use the emulated one.
    end
    cholG=chol(G_true);
    E_prp = u - sum(log(diag(cholG))) + (v'*G_true*v)/2;
    
    % Accept according to ratio
    logRatio = -E_prp + E_cur + real(dtlogJ);

    if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
        met.cholG=cholG; % true values
        met.G=G; met.ChS1=ChS1; met.dphi=dphi; % emulations
        acpY = 1;
    else
        q=q_cur; u=u_cur; met=met_cur;
        acpY = 0;
    end
    
catch
    q=q_cur; u=u_cur; met=met_cur;
    acpY = 0;
end
    
end
