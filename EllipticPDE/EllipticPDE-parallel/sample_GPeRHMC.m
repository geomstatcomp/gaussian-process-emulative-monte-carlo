%%%% This is MCMC sampling by GPeRHMC %%%%
clear;
addpath('../../../');
addpath('./sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% setup parallel enviroment
if isempty(gcp)
    parpool(6);
end

% % generate data
% theta_truth=[-1 -1 0 0 1 1]'; D=length(theta_truth);
% Nmesh=50;
% % prepare PDE solver
% PDE50=ePDEsetup(Nmesh);
% u=reshape(ePDEsolver(theta_truth,PDE50),Nmesh+1,Nmesh+1);
% obs_Nmesh=10; obs_idx=1+Nmesh/obs_Nmesh.*(0:obs_Nmesh);
% % obs_idx=linspace(1,Nmesh+1,obs_Nmesh+1);
% u=u(obs_idx,obs_idx);
% y=u(:)+.1.*randn((1+obs_Nmesh)^2,1);
% 
% % smallest potential energy
% [theta_hat,u_nadir]=fminunc(@(theta)U(y,theta,PDE50),theta_truth');
% 
% % Design information
% n=30; Ndesg=n;
% % Range=repmat([-3;3],[1,D]); % range of variables
% % De=repmat(Range(1,:),[n,1])+lhsdesign(n,D).*repmat(diff(Range),[n,1]); % Latin hypercube
% De=repmat(theta_hat,[n,1])+.1*randn(n,D);
% [u_D,du_D]=UdU(y,De,PDE50,[0,1]);% du_D=[];
% gFI_D=gFI(y,De,PDE50);

% or load data
load('ellipticPDE_Nmesh50.mat');
Ndesg=n;

% choose number of Design points
% Ndesg=50; % must <= 50
% n=size(De,1); Ndesg=n;
% De=De(1:Ndesg,:);
% u_D=u_D(1:Ndesg,:);
% de_idx=bsxfun(@plus,(1:Ndesg)',(0:D-1).*n);
% du_D=du_D(de_idx(:));
% if isempty(du_D)
%     gFI_D=gFI_D(1:Ndesg,1:Ndesg);
% else
%     de_idx=bsxfun(@plus,(1:Ndesg)',(0:D).*n);
%     gFI_D=gFI_D(de_idx,de_idx);
% end

% build emulator
emu=GPe(De,u_D,du_D,10*ones(D,1),gFI_D,1,[],1);

% sampling setting
TrjL = 2; Nleap = 10; stepsz = TrjL/Nleap;
Nfpiter = 5;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp-NBurnIn,D);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
PDE20=ePDEsetup(meshsz);

% Initialize
% true values
% theta = theta_hat';
theta = theta_truth;
u = U(y,theta,PDE20);
% met.cholG=chol(Met(theta,N,sigma2y,sigma2theta));
% emulations
[~,du,~,G,ChS1] = emu.pred(theta',[1,2,11,21]); invG = inv(G); 
met.G=G; met.invG=invG;
met.cholG = chol(G); % use emulative Fisher information for now
met.dG=permute(reshape(ChS1,[D,D,D]),[1,3,2]); % not accurate, acutually half dG
met.dphi=du + sum(repmat(invG(:)',[D,1]).*reshape(ChS1,D,D^2),2);

disp(' ');
disp('Running GPeRHMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with GPeRHMC
    [theta,u,met,acpt_idx] = GPeRHMC(theta,u,met,@(theta,der)U(y,theta,PDE20,nargin==2),...
                                     [],...%@(theta)Met(theta,N,PDE20,sigma2y,sigma2theta),...% use emulative Fisher information
                                     @(theta,opt)emu.pred(theta',opt),stepsz,Nleap,Nfpiter);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['EllipticPDE_GPeRHMC_D',num2str(D),'_Nleap',num2str(Nleap),'_Ndesg',num2str(Ndesg),'__',curTime];
save(['result/',curfile,'.mat'],'stepsz','Nleap','samp','emu','acpt','time');
disp(' ');
disp(['Accpetance Rate of GPeRHMC: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
fig1=figure(1); set(fig1,'pos',[0 800 900 400]);
idx=floor(linspace(1,size(samp,1),1e4));
subplot(1,2,1);
plot(samp(idx,:));
subplot(1,2,2);
plotmatrix(samp(idx,:));
% plot the posterior
fig=figure('visible','off');
contourmatrix_matlab(samp);
r=600;c=800;
set(fig,'position',[0 0 c r]); 
% set(gca,'position',[0 0 c+1 r+1]);
set(gcf,'papersize',[8 6]);
set(gcf,'paperposition',[0 0 8 6]);
print(fig,'-dpdf',['./figure/',curfile,'.pdf']);
% stop the pool
delete(gcp('nocreate'));