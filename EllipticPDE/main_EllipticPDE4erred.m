%%%% This is the main script to sample from posterior of parameters in Elliptic PDE to compare algorithms in error reducing %%%%
clear;
addpath('../../');
addpath('../sampler/');
% Random Numbers
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% MCMC algorithms
MCMC = {'RWM','HMC','GPeHMC','RHMC','GPeRHMC','LMC','GPeLMC'};

% choose algorithm
for alg=7;

%% load data
% generate data
% theta_truth=[-1 -1 0 0 1 1]'; D=length(theta_truth);
% Nmesh=50;
% % prepare PDE solver
% PDE50=ePDEsetup(Nmesh);
% u=reshape(ePDEsolver(theta_truth,PDE50),Nmesh+1,Nmesh+1);
% Nmesh_obs=10; obs_idx=1+Nmesh/Nmesh_obs.*(0:Nmesh_obs);
% u=u(obs_idx,obs_idx);
% y=u(:)+.1.*randn((1+Nmesh_obs)^2,1);
% or load data
load('ellipticPDE_Nmesh50.mat');

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
PDE20=ePDEsetup(meshsz);

% smallest potential energy
% [theta_hat,u_nadir]=fminunc(@(theta)U(y,theta,PDE50),theta_truth);

data.y=y;data.PDE20=PDE20;data.sigma2y=sigma2y;data.sigma2theta=sigma2theta;
if ismember(alg,[3,5,7])
    data.De=De;data.u_D=u_D;data.du_D=du_D;data.gFI_D=gFI_D;
end

%% setting of sampling
setting.WallTime=1000; setting.Intvl=5;
TrjL=[2/10,2*ones(1,6)]; Nleap=[1,8,10,3,12,2,8];
setting.trjL=TrjL(alg); setting.Nleap=Nleap(alg); setting.stepsz=setting.trjL/setting.Nleap; setting.Nfpiter=5;
setting.regintvl=20;
% switch to stop regeneration test if unnecessary
setting.doREG=0;

%% initialization
% theta = theta_hat; u = u_nadir;
theta = theta_truth;
[u,du,G,invG,dG,ChS1,dlogrtG] = GEOMexact(y,theta,PDE20,sigma2y,sigma2theta,[0,1,2,-2,3,33,-33]);
met.dG=dG; met.cholG=chol(G); met.cholinvG=chol(invG);
% build emulator
emu=[];
if ismember(alg,[3,5,7])
%     emu=GPe(De,u_D,du_D,ones(D,1),gFI_D,sigma2theta,[],1,1);
    emu=GPe_logCL(De,u_D,du_D,ones(D,1),gFI_D,sigma2theta,[],1,1);
    [~,du,~,G,ChS1] = emu.pred(theta',[1,11,21]); invG = inv(G); ChS1=reshape(ChS1,D,D^2);
    dlogrtG = sum(repmat(invG(:)',[D,1]).*ChS1,2);
end
met.e2dG=permute(reshape(ChS1,[D,D,D]),[1,3,2]);
met.G=G; met.invG=invG; met.ChS1=ChS1;
met.dphi=du+dlogrtG;

init.theta=theta; init.u=u; init.du=du;
init.emu=emu; init.met=met;

%% setting of saving
SAVE=1; PRINT=1; PLOT=1;
savepath=[pwd,'/result/'];
filename=['EllipticPDE_',char(MCMC(alg)),'_D',num2str(D),'_Nleap',num2str(Nleap(alg))];

%% sampling
[samp,poten,acpt,time,emu,reg,times,iters]=sampling4erred(data,char(MCMC(alg)),setting,init,PRINT);
if ismember(alg,[3,5,7])
    filename=[filename,'_Ndesg',num2str(reg.Ndesg(reg.Nreg))];
end

%% save results
addpath('./result/');
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=[filename,'__',curTime];
if SAVE
    stepsz=setting.stepsz; Nleap=setting.Nleap;
    save([savepath,curfile,'.mat'],'setting','samp','poten','acpt','time','emu','reg','times','iters');
end
if PRINT
    CalculateStatistics(curfile,savepath);
end
if PLOT
    % some plots
    idx=floor(linspace(1,size(samp,1),1e3));
%     dim=unique([1,2,floor(D/2),D]);
    dim=1:D;
    fig1=figure(1); set(fig1,'pos',[0 800 900 400]);
    subplot(1,2,1);
    plot(samp(idx,dim));
    subplot(1,2,2);
    plotmatrix(samp(idx,dim));
    % plot the posterior
%     fig=figure('visible','off');
%     contourmatrix_matlab(samp(:,dim));
%     r=600;c=800;
%     set(fig,'position',[0 0 c r]); 
%     % set(gca,'position',[0 0 c+1 r+1]);
%     set(gcf,'papersize',[8 6]);
%     set(gcf,'paperposition',[0 0 8 6]);
%     print(fig,'-dpdf',['./figure/',curfile,'.pdf']);
end
end