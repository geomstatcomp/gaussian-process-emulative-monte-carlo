% This is to select an initial set ofdesign points from some result %
addpath('../../');
addpath('../../AutoRefinement/');

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% generate data
theta_truth=[-1 -1 0 0 1 1]'; D=length(theta_truth);
Nmesh=50;
% prepare PDE solver
PDE50=ePDEsetup(Nmesh);
u=reshape(ePDEsolver(theta_truth,PDE50),Nmesh+1,Nmesh+1);
Nmesh_obs=10; obs_idx=1+Nmesh/Nmesh_obs.*(0:Nmesh_obs);
% model parameters
sigma2y=.1^2; sigma2theta=1;
% observations
u=u(obs_idx,obs_idx);
y=u(:)+sqrt(sigma2y).*randn((1+Nmesh_obs)^2,1);
N=length(y);

% % load the result of RWM for a long run
% load('./result/EllipticPDE_RWM_D6_Nleap1__2014_10_14_ 3_12_ 6.mat');
% load the result of HMC for a long run
load('./result/EllipticPDE_HMC_D6_Nleap10__2014_11_11_ 0_21_ 7.mat');


% thinning
idx=floor(linspace(1,size(samp,1),1000));
% select design points
n=50;
De=selectnmaxdist(samp(idx,:),n);
% calculate the information on design points
[u_D,du_D,gFI_D]=geom(y,De,PDE50,[0,1,11],sigma2y,sigma2theta);


% prepare PDE solver for inference
meshsz=20; % mesh size for inference
PDE20=ePDEsetup(meshsz);

% build and refine emulator
samp=samp(idx,:);
emu=GPe(De,u_D,du_D,10.*ones(D,1),gFI_D,sigma2theta,[],1);
for i=1:10
    cand=samp(100*(i-1)+1:100*(i-1)+100,:);
    emu=refine_MICE01(emu,@(theta,opt)UdU(y,theta,PDE20,opt),@(theta)gFI(y,theta,PDE20),cand,10);
end

% refined design
De=emu.De;
u_D=emu.u_D;du_D=emu.du_D;gFI_D=emu.gFI_D;

% save the data
save('ellipticPDE_Nmesh50.mat','seed','D','theta_truth','N','y','sigma2y','sigma2theta','n','De','u_D','du_D','gFI_D');
