%%%% This is MCMC sampling by LMC %%%%
clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% % generate data
% theta_truth=[-1 -1 0 0 1 1]'; D=length(theta_truth);
% Nmesh=50;
% % prepare PDE solver
% PDE50=ePDEsetup(Nmesh);
% u=reshape(ePDEsolver(theta_truth,PDE50),Nmesh+1,Nmesh+1);
% Nmesh_obs=10; obs_idx=1+Nmesh/Nmesh_obs.*(0:Nmesh_obs);
% u=u(obs_idx,obs_idx);
% y=u(:)+.1.*randn((1+Nmesh_obs)^2,1);

% load data
load('ellipticPDE_Nmesh50.mat');

% sampling setting
TrjL = 2; Nleap = 2; stepsz = TrjL/Nleap;

% allocation to save
Nsamp = 110000; NBurnIn = 10000;
samp = zeros(Nsamp-NBurnIn,D);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
PDE20=ePDEsetup(meshsz);

% Initialize
theta = randn(D,1);
% [u,du] = UdU(y,theta,PDE20,[0,1]);
% [G,invG,~,ChS1,dlogrtG] = Met(theta,N,PDE20,[0,-1,3,-3]);
[u,du,G,invG,~,ChS1,dlogrtG] = GEOMexact(y,theta,PDE20,sigma2y,sigma2theta,[0,1,2,-2,33,-33]);
met.G=G; met.cholinvG=chol(invG); met.ChS1=ChS1;
met.dphi=du+dlogrtG;

disp(' ');
disp('Running LMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with LMC
    [theta,u,met,acpt_idx] = LMCc(theta,u,met,@(theta,opt)GEOMexact(y,theta,PDE20,sigma2y,sigma2theta,opt),stepsz,Nleap);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['EllipticPDE_LMC_D',num2str(D),'_Nleap',num2str(Nleap),'__', curTime];
save(['result/',curfile,'.mat'],'stepsz','Nleap','samp','acpt','time');
disp(' ');
disp(['Accpetance Rate of LMC: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
idx=floor(linspace(1,size(samp,1),1e4));
fig1=figure(1); set(fig1,'pos',[0 800 800 400]);
plot(samp(idx,:));
fig2=figure(2); set(fig2,'pos',[100 800 800 400]);
plotmatrix(samp(idx,:));
% plot the posterior
fig=figure('visible','off');
contourmatrix_matlab(samp);
r=600;c=800;
set(fig,'position',[0 0 c r]); 
% set(gca,'position',[0 0 c+1 r+1]);
set(gcf,'papersize',[8 6]);
set(gcf,'paperposition',[0 0 8 6]);
print(fig,'-dpdf',['./figure/',curfile,'.pdf']);