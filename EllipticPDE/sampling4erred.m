%%%%% This is to compare different algorithms in error reducing %%%%

function[samp,poten,acpt,time,emu,reg,times,iters]=sampling4erred(data,alg,setting,init,PRINT)
samp=[];poten=[];acpt=[];time=[];emu=[];reg=[];times=[];iters=[];
% pass the data
y=data.y;PDE20=data.PDE20;sigma2y=data.sigma2y;sigma2theta=data.sigma2theta;
if strfind(alg,'GPe')
    De=data.De;u_D=data.u_D;du_D=data.du_D;gFI_D=data.gFI_D;
end

% MCMC settings
stepsz=setting.stepsz; Nleap=setting.Nleap; Nfpiter=setting.Nfpiter;
doREG=setting.doREG; regintvl=setting.regintvl;

% storage of posterior samples
N=length(y);D=length(init.theta);
WallTime=setting.WallTime; Intvl=setting.Intvl;
SaveLeng=ceil(WallTime/Intvl);
times=zeros(SaveLeng,1);
iters=zeros(SaveLeng,1);
samp=zeros(1e5,D);
poten=zeros(1e5,1);
acpi=zeros(1,2); acpt=zeros(1,2);
if strfind(alg,'GPe')
    n = size(De,1);
    logPi0Q = zeros(1e5,1);
    Nreg=1; % total number of regenerations
    regtime = zeros(ceil(1e5/regintvl),1); regtime(1)=1;% regeneration times
    Entropy = zeros(ceil(1e5/regintvl),1);
    Ndesg = zeros(ceil(1e5/regintvl),1); Ndesg(1)=n;
end

% initialization
theta=init.theta; u=init.u; du=init.du;
emu=init.emu; met=init.met;
sampler=str2func(alg);
times(1)=0;iters(1)=1;
iter=1;counter=1;

% start MCMC run
disp(' ');
disp(['Running ',alg,' sampling...']);
tstart=tic; % starting time;
while counter<SaveLeng&&times(counter)<=WallTime
    
    if PRINT&&(mod(iter,100)==0)
        disp([num2str(iter) ' iterations completed.']);
        disp(['Online acceptance rate of main sampler is ',num2str(acpi(1)/100)]); acpi(1)=0;
        if ~isempty(strfind(alg,'GPe'))&&doREG
            disp(['Online acceptance of independence sampler: ',num2str(acpi(2)/100)]); acpi(2)=0;
        end
    end
    
    % 1st kernel: main MCMC
    try
        switch(alg)
            case 'RWM'
                [theta,u,acpY_MC] = sampler(theta,u,@(theta)U(y,theta,PDE20),stepsz);
            case 'HMC'
                [theta,u,du,acpY_MC]=sampler(theta,u,du,@(theta,der)U(y,theta,PDE20,nargin==2),stepsz,Nleap);
            case 'GPeHMC'
                [theta,u,du,acpY_MC]=sampler(theta,u,du,@(theta,der)U(y,theta,PDE20,nargin==2),...
                                            @(theta)emu.pred(theta',1),stepsz,Nleap);
            case 'RHMC'
                [theta,u,met,acpY_MC]=RHMCc(theta,u,met,@(theta,opt)GEOMexact(y,theta,PDE20,sigma2y,sigma2theta,opt),stepsz,Nleap,Nfpiter);
            case 'GPeRHMC'
                [theta,u,met,acpY_MC]=sampler(theta,u,met,@(theta,der)U(y,theta,PDE20,nargin==2),...
                                             [],...%@(theta)Met(theta,N,PDE20,sigma2y,sigma2theta),...
                                             @(theta,opt)emu.pred(theta',opt),stepsz,Nleap,Nfpiter);
            case 'LMC'
                [theta,u,met,acpY_MC]=LMCc(theta,u,met,@(theta,opt)GEOMexact(y,theta,PDE20,sigma2y,sigma2theta,opt),stepsz,Nleap);
            case 'GPeLMC'
                [theta,u,met,acpY_MC]=sampler(theta,u,met,@(theta,der)U(y,theta,PDE20,nargin==2),...
                                             [],...%@(theta)Met(theta,N,PDE20),...
                                             @(theta)emu.pred(theta',[1,11,21]),stepsz,Nleap);
            otherwise
                error('Not a listed sampling algorithm!');
        end
    catch
        acpY_MC = 0;
        disp('Crashed and Rejected!');
    end
    % online acceptance rate for main sampler
    acpi(1)=acpi(1)+acpY_MC;
    
    if ~isempty(strfind(alg,'GPe'))&&doREG
    % 2nd kernel: independence sampler Q using Mixture of Gaussian
    % mix of Gaussians
    if Iter==1||Iter==regtime(Nreg)+1
%         K=D;
        K=size(emu.De,1);
%         [~,I_nearestK]=pdist2(emu.De,theta','euclidean','smallest',K);
%         [~,I_nearestK]=pdist2(emu.De,theta',@(XI,XJ)(sqrt(bsxfun(@minus,XI,XJ).^2 * exp(-emu.tau))),'smallest',K);
%         I_nearestK=datasample(1:size(emu.De,1),K,'replace',false,'weights',exp(-emu.u_D));
        I_nearestK=1:K;
        mu=emu.De(I_nearestK,:); u_mu = emu.u_D(I_nearestK); %logC=-min(u_mu);
        [~,~,~,G_mu] = emu.pred(mu,11);
%         wts=(exp(-u_mu)./sum(exp(-u_mu)))'; % probabilities at design points are good weights
        wts=exp(-u_mu-logsumexp(-u_mu))'+1e-14;
        G_mu=permute(reshape(G_mu,[K,D,D]),[2,3,1]);
        cholG_mu=reshape(cell2mat(arrayfun(@(i)chol(G_mu(:,:,i)),1:K,'UniformOutput', false)),[D,D,K]);
        mog.mu=mu; mog.cholinvSigma=cholG_mu; mog.wts=wts;
    end
    % prpose an indepence sample and do regeneration test
    logpiOq=-u-logpdf_MOG(theta',mu,[],cholG_mu,wts);
    if Iter==1
        logPi0Q(1)=logpiOq;
    end
    if Nreg==1
        logC=logpiOq;
%         logC=-u_nadir;
    end
    [theta_idp,u_idp,logpiOq_idp,acpY_idp,regY]=idp_MOG(mog,theta,u,logpiOq,@(theta)U(y,theta,sigma2y,sigma2theta),logC);
    if acpY_idp % theta_idp accepted
        % test regeneration
        if regY&&(Iter<=100||mod(Iter,regintvl)==0) % regeneration occurs
            disp([num2str(Nreg),'-th regeneration happens!']);
            % update design pool and emulator
            [emu,Ent]=emu.refine(samp(regtime(Nreg):Iter-1,:),poten(regtime(Nreg):Iter-1),@(theta,opt)geom(y,theta,sigma2y,sigma2theta,opt),10);
%             logPi0Q_btreg=logPi0Q(regtime(Nreg):Iter-1);offset=max(logPi0Q_btreg);logC=log(mode(exp(logPi0Q_btreg-offset)))+offset; % update logC
            Nreg=Nreg+1; regtime(Nreg)=Iter; Entropy(Nreg)=gather(Ent); Ndesg(Nreg)=size(emu.De,1);% record regeneration time and entropy
            % discard theta_idp, resampling from independence kernel Q
            acpY_idp=0;
            while ~acpY_idp
                [theta_idp,u_idp,logpiOq_idp,acpY_idp]=idp_MOG(mog,theta,u,logpiOq,@(theta)U(y,theta,sigma2y,sigma2theta),[],0);
            end
            % test if entropy converges by testing if linear regression slope is 0
            if Nreg>2
%                 s=cov(regtime(1:Nreg)/regintvl,Entropy(1:Nreg));
                s=cov(1:Nreg,Entropy(1:Nreg)); % more conservative
                df=Nreg-2; t=s(1,2)/sqrt((s(1,1)*s(2,2)-s(1,2)^2)/df);
                if tcdf(abs(t),df,'upper')<.05/2
                    doREG=0; % stop regeneration
                    disp(['Entropy reaches stationarity at iteration ', num2str(Iter),' after ',num2str(Nreg),' regnerations... Now stop adaption.']);
                end
%                 % try testing data
%                 testing=datasample(1:Iter-1,min([Iter-1,100]),'replace',false);
% %                 testing=1:Iter-1;
%                 mse=mean((emu.pred(samp(testing,:),0)-poten(testing)).^2);
%                 disp(['Mean Squared Error: ',num2str(mse)]);
%                 if mse<1e-2
%                     doREG=0; % stop regeneration
%                     disp(['MSE falls below threshold at iteration ', num2str(Iter),' after ',num2str(Nreg),' regnerations... Now stop adaption.']);
%                 end
            end
        end
        % update current values
        theta=theta_idp; u=u_idp; logpiOq=logpiOq_idp;
        [~,du,~,G,ChS1] = emu.pred(theta',[1,11,21]); invG = inv(G); ChS1=reshape(ChS1,D,D^2);
        met.G=G; met.ChS1=ChS1;
        met.dphi = du + sum(repmat(invG(:)',[D,1]).*ChS1,2);
        G_true=Met(theta,N,sigma2y,sigma2theta);
        met.cholG=chol(G_true);
%         met.cholG=chol(met.G); % use emulative Fisher information for now
    end
    % online acceptance rate for independence sampler
    acpi(2) = acpi(2) + acpY_idp;
    end
    
    time=toc(tstart);
    if time-times(counter)>Intvl
        counter=counter+1;
        times(counter)=time;
        iters(counter)=iter;
    end
    
    samp(iter,:) = theta';
    poten(iter) = u;
    if ~isempty(strfind(alg,'GPe'))&&doREG
        logPi0Q(iter) = logpiOq;
    end
    acpt(1) = acpt(1) + acpY_MC;
    if ~isempty(strfind(alg,'GPe'))&&doREG
        acpt(2) = acpt(2) + acpY_idp;
    end
    
    iter=iter+1;
end
time=toc(tstart);

samp=samp(1:iter-1,:);
poten=poten(1:iter-1);
if ~isempty(strfind(alg,'GPe'))
    logPi0Q=logPi0Q(1:iter-1);
    reg.logPi0Q=logPi0Q;reg.Nreg=Nreg;reg.regtime=regtime;reg.Entropy=Entropy;reg.Ndesg=Ndesg;
end

disp(' ');
disp(['Time consumed: ',num2str(time)]);
acpt=acpt/(iter-1);
disp(['Final acceptance rate: ',num2str(acpt)]);
disp(' ');

end