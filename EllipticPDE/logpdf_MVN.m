function logpdf = logpdf_MVN(X,mu,Sigma,R)
% X: N x D
% mu: 1 x D
% Sigma/invR: D x D
% R: R'*R=inv(Sigma)
if nargin < 4
    R=[];
end

if ~isempty(R)
    D = size(X, 2);
    X = bsxfun(@minus, X, mu);
    XR = X * R'; % N x D
    logrtdetinvSigma = sum(log(diag(R)));
    quadform = sum(XR.^2, 2);
    logpdf = -quadform./2 + logrtdetinvSigma - D/2*log(2*pi);
else
    logpdf = log(mvnpdf(X,mu,Sigma));
end
end
