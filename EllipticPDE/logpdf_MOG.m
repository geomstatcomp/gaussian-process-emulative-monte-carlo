function logdensity = logpdf_MOG(X,mu,Sigma,R,wts)
% R: R'*R=inv(Sigma)
% wts: (1,K)

K = size(mu,1);
N = size(X,1);

logp_K = zeros(N,K);
if ~isempty(R)
    for k=1:K
        logp_K(:,k) = logpdf_MVN(X,mu(k,:),[],R(:,:,k));
    end
else
    for k=1:K
        logp_K(:,k) = logpdf_MVN(X,mu(k,:),Sigma(:,:,k));
    end
end

logp_K = logp_K + repmat(log(wts),N,1);
logdensity = logsumexp(logp_K,2);
end