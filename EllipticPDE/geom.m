% This is to compute the geometric quantities including gradient and metric %
function [u,du,gFI,xL_til] = geom(y,theta,PDE,opt,sigma2y,sigma2theta)
% y: data observed at (11,11) regular mesh points
% theta: parameter of diffusive field k
if(nargin<4)
    opt=0;sigma2y=.01;sigma2theta=1;
elseif(nargin<5)
    sigma2y=.01;sigma2theta=1;
end
u=[];du=[];gFI=[];xL_til=[];

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end

N=length(y);
Nmesh=sqrt(size(PDE.p,2)); % mesh size has to be (Nmesh,Nmesh)
obs_idx=linspace(1,Nmesh,sqrt(N));
[obs_mesh1,obs_mesh2]=meshgrid(obs_idx,obs_idx);
idx=sub2ind([Nmesh,Nmesh],obs_mesh2(:),obs_mesh1(:));
s=zeros(n,N); ds=zeros(n,D*N); % store solutions
% get options for dertivatives
opt4der=[]; str4der=num2str(opt);
for i=0:2
    if ~isempty(strfind(str4der,num2str(i)))
        opt4der=[opt4der,i];
    end
end
for i=1:n
    % forward model to solve elliptic PDE given theta
    [sol,dsol]=ePDEsolver(theta(i,:)',PDE,opt4der);
    if ~isempty(sol)
        s(i,:)=sol(idx);
    end
    if ~isempty(dsol)
        dsol=dsol(idx,:)';
        ds(i,:)=dsol(:);
    end
end
ds=reshape(ds,[n*D,N]);

Err=s-repmat(y',[n,1]);
if any(ismember([0,11],opt))
    xL = -Err.^2./sigma2y/2; % (n,N)
end
if any(ismember([1,11],opt))
    dxL = -repmat(Err,[D,1]).*ds./sigma2y; % (nD,N)
end

if any(opt==0)
    loglik = sum(xL,2);
    logpri = -sum(theta.^2,2)./sigma2theta/2;
    u = -(loglik+logpri);
end
if any(opt==1)
    dloglik = sum(dxL,2);
%     dloglik = reshape(dloglik,[n,D]);
    dlogpri = -theta(:)./sigma2theta;
%     dlogpri = -theta./sigma2theta;
    du  = -(dloglik + dlogpri);
%     if size(du,1)==1
%         du=du';
%     end
end
if any(opt==11)
    xL_til=[xL;dxL];
    L_til=sum(xL_til,2);
    gFI=xL_til*xL_til'-L_til*L_til'./N;
end

end