% This is to select a subset of n points with maximum pairwise distance
% from a set of N points.
% may not be a perfect solution, but suffices the need.

function [x,i]=selectnmaxdist(X,n)

N=size(X,1);
if n>=N
    x=X;i=1:N;
else
    [D,I]=pdist2(X,X,'euclidean','largest',1);
    [~,maxp2X_id]=sort(D,'descend');
    idx_cand=[maxp2X_id;I(maxp2X_id)];
    idx_cand=unique(idx_cand(:),'stable');
    i=idx_cand(1:n);
    x=X(i,:);
end

end