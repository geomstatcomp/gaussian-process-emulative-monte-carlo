%% energy function %%
function [output] = U(y,theta,PDE,der)
% y data observed at (11,11) regular mesh points
% theta parameter of diffusive field k
if(nargin<4)
    der=0;
end

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end

N=length(y);
Nmesh=sqrt(size(PDE.p,2)); % mesh size has to be (Nmesh,Nmesh)
obs_idx=linspace(1,Nmesh,sqrt(N));
[obs_mesh1,obs_mesh2]=meshgrid(obs_idx,obs_idx);
idx=sub2ind([Nmesh,Nmesh],obs_mesh2(:),obs_mesh1(:));
s=zeros(n,N); ds=zeros(n,D*N); % store solutions
for i=1:n
    % forward model to solve elliptic PDE given theta
    [sol,dsol]=ePDEsolver(theta(i,:)',PDE,[0,der]);
    if ~isempty(sol)
        s(i,:)=sol(idx);
    end
    if ~isempty(dsol)
        dsol=dsol(idx,:)';
        ds(i,:)=dsol(:);
    end
end
ds=reshape(ds,[n*D,N]);

if der==0
    loglik = -sum((repmat(y',[n,1])-s).^2,2)/(2*.01);
    logpri = -sum(theta.^2,2)/2;
    output = -(loglik+logpri);
elseif der==1
    dloglik = sum((repmat(y',[n*D,1])-repmat(s,[D,1])).*ds,2)/.01;
    dloglik = reshape(dloglik,[n,D]);
    dlogpri = -theta;
    output  = -(dloglik + dlogpri);
    if size(output,1)==1
        output=output';
    end
else
    disp('wrong choice of der!');
end

end