% This is to solve a classic example of elliptic equation %
% d(kdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;

function u=ePDEsolver(theta,meshsz,sigma2,l)
if nargin==1
    meshsz=30;
    sigma2=1; l=0.2;
elseif nargin==2
    sigma2=1; l=0.2;
end

% define solution domain
gdm = [3 4 0 1 1 0 0 0 1 1]';
g = decsg(gdm, 'S1', ('S1')');
% plot it
% figure;
% pdegplot(g, 'edgeLabels', 'on');
% axis([-.1 1.1 -.1 1.1]);
% title 'Geometry With Edge Labels Displayed';

% creat triangular mesh
% hmax = .1; % element size
% [p,e,t] = initmesh(g, 'Hmax', hmax);
% try regular mesh
[p,e,t] = poimesh(g,meshsz,meshsz);
% figure;
% pdeplot(p,e,t);
% axis equal
% title 'Plate With Mesh'
% xlabel 'X-coordinate, meters'
% ylabel 'Y-coordinate, meters'

% Create a pde entity for a PDE with a single dependent variable
numberOfPDE = 1;
pb = pde(numberOfPDE);
% Create a geometry entity
pg = pdeGeometryFromEdges(g);
% specify boundary conditions
bc1 = pdeBoundaryConditions(pg.Edges(1),'u',@(problem,region,state)region.x);
bc3 = pdeBoundaryConditions(pg.Edges(3),'u',@(problem,region,state)1-region.x);
bc24 = pdeBoundaryConditions(pg.Edges([2,4]),'g',0);
pb.BoundaryConditions = [bc1,bc3,bc24]; % all boundary conditions

% specify coefficients
c = coeffunction(p,t,theta,sigma2,l);

% solution
u = assempde(pb,p,e,t,c,0,0);
% plot the solution
% figure(1);
% pdesurf(p,t,u);

end

function coeff = coeffunction(p,t,theta,sigma2,l)

% Triangle point indices
it1 = t(1,:);
it2 = t(2,:);
it3 = t(3,:);
% Find centroids of triangles
centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;

[D,N] = size(centroid);
dist = bsxfun(@minus,reshape(centroid',[N,1,D]),reshape(centroid',[1,N,D]));
C = sigma2.*exp(-sum(dist.^2,3)/(2*l^2));

[evc,ev] = eigs(C);
coeff = exp(sum(repmat(theta.*sqrt(diag(ev)),[1,N]).*evc'));

end

% function coeff = coeffunction(p,theta,sigma2,l)
% 
% [D,N] = size(p);
% dist = bsxfun(@minus,reshape(p',[N,1,D]),reshape(p',[1,N,D]));
% C = sigma2.*exp(-sum(dist.^2,3)/(2*l^2));
% 
% [evc,ev] = eigs(C);
% coeff = exp(sum(repmat(theta.*sqrt(diag(ev)),[1,N]).*evc'));
% 
% end