%%%% This is MCMC sampling by GPeLMC %%%%
clear;
addpath('../../');
addpath('../sampler/');
addpath('../../AutoRefinement/');

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% generate data
theta_truth=[-1 -1 0 0 1 1]'; D=length(theta_truth);
Nmesh=50;
% prepare PDE solver
PDE50=ePDEsetup(Nmesh);
u=reshape(ePDEsolver(theta_truth,PDE50),Nmesh+1,Nmesh+1);
obs_Nmesh=10; obs_idx=1+Nmesh/obs_Nmesh.*(0:obs_Nmesh);
% obs_idx=linspace(1,Nmesh+1,obs_Nmesh+1);
% model parameters
sigma2y=.1^2; sigma2theta=1;
% observations
u=u(obs_idx,obs_idx);
y=u(:)+sqrt(sigma2y).*randn((1+obs_Nmesh)^2,1);
N=length(y);

% smallest potential energy
[theta_hat,u_nadir]=fminunc(@(theta)U(y,theta,PDE50),theta_truth');

% Design information
n=30; %Ndesg=n;
% Range=repmat([-3;3],[1,D]); % range of variables
% De=repmat(Range(1,:),[n,1])+lhsdesign(n,D).*repmat(diff(Range),[n,1]); % Latin hypercube
De=repmat(theta_hat,[n,1])+.1*randn(n,D);
% [u_D,du_D]=UdU(y,De,PDE50,[0,1]);% du_D=[];
% gFI_D=gFI(y,De,PDE50);
[u_D,du_D,gFI_D]=geom(y,De,PDE50,[0,1,11],sigma2y,sigma2theta);

% % or load data
% load('ellipticPDE_Nmesh50.mat');
% Ndesg=n;

% build emulator
emu=GPe(De,u_D,du_D,1e-1*ones(D,1),gFI_D,1,[],1,1);

% sampling setting
TrjL = 2; Nleap = 8; stepsz = TrjL/Nleap;
regintvl = 20;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp,D);
poten = zeros(Nsamp,1);
logPi0Q = zeros(Nsamp,1);
logC = -u_nadir;
accp_GPeMC = 0; % online GPeMC acceptance
accp_indep = 0; % online independence sampling acceptance
acpt_GPeMC = 0; % final GPeMC acceptance rate
acpt_indep = 0; % final independence sampling acceptance
Nreg=1; % total number of regenerations
regtime = zeros(ceil(Nsamp/regintvl),1); regtime(1)=1;% regeneration times
Entropy = zeros(ceil(Nsamp/regintvl),1);
Ndesg = zeros(ceil(Nsamp/regintvl),1); Ndesg(1)=n;
doREG = 1; % switch to stop regeneration test after entropy converges

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
PDE20=ePDEsetup(meshsz);

% Initialize
% true values
% theta = theta_hat';
theta = theta_truth;
u = U(y,theta,PDE20);
% met.cholG=chol(Met(theta,N,PDE20));
% emulations
[~,du,~,G,ChS1] = emu.pred(theta',[1,2,11,21]); invG = inv(G); ChS1=reshape(ChS1,D,D^2);
met.G=G; met.ChS1=ChS1;
met.cholG = chol(G); % use emulative Fisher information for now
met.dphi=du + sum(repmat(invG(:)',[D,1]).*ChS1,2);
% entropy of the initial design
[~,~,~,~,~,mspe] = emu.pred(De,40);
Ent=sum(log(mspe));
Entropy(1)=Ent;

disp(' ');
disp('Running GPeLMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance of GPeMC: ',num2str(accp_GPeMC/100)]); accp_GPeMC=0;
        if doREG
            disp(['Current acceptance of independence sampler: ',num2str(accp_indep/100)]); accp_indep=0;
        end
    end
    
    % 1st kernel: GPeLMC
    [theta,u,met,acpY_MC] = GPeLMC(theta,u,met,@(theta,der)U(y,theta,PDE20,nargin==2),...
                                   [],...%@(theta)Met(theta,N,PDE20),... % use emulative Fisher information for acceptance test for now
                                   @(theta)emu.pred(theta',[1,2,11,21]),stepsz,Nleap);
    % online acceptance rate for GPeMC
    accp_GPeMC = accp_GPeMC + acpY_MC;
    
    if doREG
    % 2nd kernel: independence sampler Q using Mixture of Gaussian
    % mix of Gaussians
    mu=emu.De; % means of MOG
    n=size(mu,1);
    [u_mu,~,~,G_mu] = emu.pred(mu,[0,1,11]);
    wts=(exp(-u_mu)./sum(exp(-u_mu)))'; % probabilities at design points are good weights
    G_mu=permute(reshape(G_mu,[n,D,D]),[2,3,1]);
    cholG_mu=reshape(cell2mat(arrayfun(@(i)chol(G_mu(:,:,i)),1:n,'UniformOutput', false)),[D,D,n]);
    mog.mu=mu; mog.cholinvSigma=cholG_mu; mog.wts=wts;
    % prpose an indepence sample and do regeneration test
    logpiOq=-u-logpdf_MOG(theta',mu,[],cholG_mu,wts);
%     if Iter==1
%         logPi0Q(1)=logpiOq;
% %     end
%     if Nreg==1
% %         logC=logpiOq;
%         logC=-u_nadir;
%     end
    [theta_idp,u_idp,logpiOq_idp,acpY_idp,regY]=idp_MOG(mog,theta,u,logpiOq,@(theta)U(y,theta,PDE20),logC);
    if acpY_idp % theta_idp accepted
        % test regeneration
        if regY&&mod(Iter,regintvl)==0 % regeneration occurs
            disp([num2str(Nreg),'-th regeneration happens!']);
            % update design pool and emulator
            prevtour=regtime(Nreg):Iter-1;
            [emu,Ent]=emu.refine(samp(prevtour,:),poten(prevtour),@(theta,opt)geom(y,theta,PDE20,opt),10);
%             [emu,Ent]=emu.refine(@(theta,opt)UdU(y,theta,PDE20,opt),@(theta)gFI(y,theta,PDE20),samp(regtime(Nreg):Iter-1,:),poten(regtime(Nreg):Iter-1),10);
%             logPi0Q_btreg=logPi0Q(regtime(Nreg):Iter-1);offset=max(logPi0Q_btreg);logC=log(mean(exp(logPi0Q_btreg-offset)))+offset; % update logC
%             [~,modeidx]=min(poten(prevtour));logC=logPi0Q(prevtour(modeidx))-log(2);
            Nreg=Nreg+1; regtime(Nreg)=Iter; Entropy(Nreg)=Ent; Ndesg(Nreg)=size(emu.De,1);% record regeneration time and entropy
            % discard theta_idp, resampling from independence kernel Q
            acpY_idp=0;
            while ~acpY_idp
                [theta_idp,u_idp,logpiOq_idp,acpY_idp]=idp_MOG(mog,theta,u,logpiOq,@(theta)U(y,theta,PDE20),[],0);
            end
            % test if entropy converges by testing if linear regression slope is 0
            if Nreg>2
%                 s=cov(regtime(1:Nreg)/regintvl,Entropy(1:Nreg));
%                 s=cov(1:Nreg,Entropy(1:Nreg)); % more conservative
%                 df=Nreg-2; t=s(1,2)/sqrt((s(1,1)*s(2,2)-s(1,2)^2)/df);
%                 if tcdf(abs(t),df,'upper')<.05/2
%                     doREG=0; % stop regeneration
%                     disp(['Entropy reaches stationarity at iteration ', num2str(Iter),' after ',num2str(Nreg),' regnerations... Now stop adaption.']);
%                 end
                % try testing data
                testing=datasample(1:Iter-1,min([Iter-1,100]),'replace',false);
                mse=mean((emu.pred(samp(testing,:),0)-poten(testing)).^2);
%                 disp(['Mean Squared Error: ',num2str(mse)]);
                if mse<1e-2
                    doREG=0; % stop regeneration
                    disp(['MSE falls below threshold at iteration ', num2str(Iter),' after ',num2str(Nreg),' regnerations... Now stop adaption.']);
                end
            end
        end
        % update current values
        theta=theta_idp; u=u_idp; logpiOq=logpiOq_idp;
        [~,du,~,met.G,ChS1] = emu.pred(theta',[1,2,11,21]); invG = inv(met.G);
        met.ChS1=reshape(ChS1,D,D^2);
        met.dphi = du + sum(repmat(invG(:)',[D,1]).*met.ChS1,2);
%         G_true=Met(theta,N,sigma2y,sigma2theta);
        met.cholG=chol(met.G); % use emulative Fisher information for now
    end
    % online acceptance rate for independence sampler
    accp_indep = accp_indep + acpY_idp;
    end
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    samp(Iter,:) = theta';
    poten(Iter) = u;
    logPi0Q(Iter) = logpiOq;
    if Iter > NBurnIn
        acpt_GPeMC = acpt_GPeMC + acpY_MC;
        if doREG
            acpt_indep = acpt_indep + acpY_idp;
        end
    end
    
end

% burn in
samp=samp(NBurnIn+1:end,:);
poten=poten(NBurnIn+1:end);
logPi0Q=logPi0Q(NBurnIn+1:end);
% save results
time=toc;
acpt_GPeMC=acpt_GPeMC/(Nsamp-NBurnIn);
% acpt_indep=acpt_indep/(regtime(Nreg)-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['EllipticPDE_GPeLMC_D',num2str(D),'_Nleap',num2str(Nleap),'_Ndesg',num2str(Ndesg(Nreg)),'__',curTime];
save(['result/',curfile,'.mat'],'stepsz','Nleap','samp','poten','logPi0Q','emu','acpt_GPeMC','regtime','Ndesg','Entropy','acpt_indep','time');
disp(' ');
disp(['Accpetance Rate of GPeLMC: ',num2str(acpt_GPeMC)]);
disp(['Accpetance Rate of indepence kernel: ',num2str(acpt_indep)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
fig1=figure(1); set(fig1,'pos',[0 800 900 400]);
idx=floor(linspace(1,size(samp,1),1e4));
subplot(1,2,1);
plot(samp(idx,:));
subplot(1,2,2);
plotmatrix(samp(idx,:));
% plot the posterior
fig=figure('visible','off');
contourmatrix_matlab(samp);
r=600;c=800;
set(fig,'position',[0 0 c r]); 
% set(gca,'position',[0 0 c+1 r+1]);
set(gcf,'papersize',[8 6]);
set(gcf,'paperposition',[0 0 8 6]);
print(fig,'-dpdf',['./figure/',curfile,'.pdf']);