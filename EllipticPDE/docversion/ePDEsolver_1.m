% This is to solve a classic example of elliptic equation given mesh and boundary conditions %
% d(kdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;

function u=ePDEsolver(theta,PDE)
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
pb=PDE.pb; % problem (boudary condition)
rtev_KL=PDE.rtev_KL; evc_KL=PDE.evc_KL; % 6 largest eigen values/vectors of integral operator for KL expansion

% specify coefficients
c = exp(sum(repmat(theta.*rtev_KL,[1,size(evc_KL,1)]).*evc_KL'));

% solution
u = assempde(pb,p,e,t,c,0,0);
% plot the solution
% figure(1);
% pdesurf(p,t,u);

end