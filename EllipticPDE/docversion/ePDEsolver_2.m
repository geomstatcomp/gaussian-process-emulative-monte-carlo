% This is to solve a classic example of elliptic equation given mesh and boundary conditions %
% d(kdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;

function u=ePDEsolver(theta,PDE,sigma2,l)
if nargin==2
    sigma2=1; l=0.2;
end

% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
pb=PDE.pb; % problem (boudary condition)

% specify coefficients
% c = @(p,t,u,time)coeffunction(p,t,theta,sigma2,l);
% c=[11.9701    0.9574    0.9095    2.6894    0.1234   35.9415    5.8610    2.7334]; % Stiffness matrix
c = coeffunction(p,t,theta,sigma2,l); % not correct, but if we can get Stiffness matrix, we can avoid repeated calculation
% same, could be used reduce the computation


% solution
u = assempde(pb,p,e,t,c,0,0);
% plot the solution
figure;
pdesurf(p,t,u);

end

function coeff = coeffunction(p,t,theta,sigma2,l)
% Triangle point indices
it1 = t(1,:);
it2 = t(2,:);
it3 = t(3,:);
% Find centroids of triangles
centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;

[D,N] = size(centroid);
dist = bsxfun(@minus,reshape(centroid',[N,1,D]),reshape(centroid',[1,N,D]));
C = sigma2.*exp(-sum(dist.^2,3)/(2*l^2));

[evc,ev] = eig(C,'vector');
rtev = sqrt(ev(end-5:end)); evc=evc(:,end-5:end)'; % 6 largest eigen values/vectors of integral operator for KL expansion
coeff = exp(sum(repmat(theta.*rtev,[1,N]).*evc));


end