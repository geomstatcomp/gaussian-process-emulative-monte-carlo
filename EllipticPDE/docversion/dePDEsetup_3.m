% This is to set up to jointly solve a classic example of elliptic equation and its derivatives %
% d(kdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;

function dPDE=dePDEsetup(meshsz,numberOfPDE,sigma2,l)
if nargin==0
    meshsz=30; numberOfPDE=1;
    sigma2=1; l=0.2;
elseif nargin==1
    numberOfPDE=1;
    sigma2=1; l=0.2;
elseif nargin==2
    sigma2=1; l=0.2;
end

% define solution domain
gdm = [3 4 0 1 1 0 0 0 1 1]';
g = decsg(gdm, 'S1', ('S1')');
% plot it
% figure;
% pdegplot(g, 'edgeLabels', 'on');
% axis([-.1 1.1 -.1 1.1]);
% title 'Geometry With Edge Labels Displayed';

% creat triangular mesh
% hmax = .1; % element size
% [p,e,t] = initmesh(g, 'Hmax', hmax);
% try regular mesh
[p,e,t] = poimesh(g,meshsz(1),meshsz(end));
% figure;
% pdeplot(p,e,t);
% axis equal
% title 'Plate With Mesh'
% xlabel 'X-coordinate, meters'
% ylabel 'Y-coordinate, meters'

% Create a pde entity for a PDE with multiple dependent variables
pb = pde(numberOfPDE);
% Create a geometry entity
pg = pdeGeometryFromEdges(g);
% specify boundary conditions
% bc1 = pdeBoundaryConditions(pg.Edges(1),'u',@(problem,region,state)[region.x;0]);
% bc3 = pdeBoundaryConditions(pg.Edges(3),'u',@(problem,region,state)[1-region.x;0]);
bc13 = pdeBoundaryConditions(pg.Edges([1,3]),'u',zeros(numberOfPDE,1));
bc24 = pdeBoundaryConditions(pg.Edges([2,4]),'q',zeros(numberOfPDE),'g',zeros(numberOfPDE,1));
pb.BoundaryConditions = [bc13,bc24]; % all boundary conditions

% prepare for the PDE coefficients
% Triangle point indices
it1 = t(1,:);
it2 = t(2,:);
it3 = t(3,:);
% Find centroids of triangles
centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;

% covariance kernel
C = sigma2.*exp(-pdist2(centroid',centroid').^2/(2*l^2));

[evc,ev] = eigs(C); % there are some approximations behind it, I guess, check C*evc./evc! but much faster
rtev_KL = sqrt(diag(ev)); evc_KL = evc'; % ok under fixed random seed

% output
dPDE.p=p;dPDE.e=e;dPDE.t=t;
dPDE.pb=pb;
dPDE.rtev_KL=rtev_KL; dPDE.evc_KL=evc_KL;

end
