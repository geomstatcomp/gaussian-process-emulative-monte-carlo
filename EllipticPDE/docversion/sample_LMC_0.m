%%%% This is MCMC sampling by LMC %%%%
clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% % generate data
% theta_truth=[-1 -1 0 0 1 1]'; D=length(theta_truth);
% Nmesh=50;
% % prepare PDE solver
% PDE50=ePDEsetup(Nmesh);
% u=reshape(ePDEsolver(theta_truth,PDE50),Nmesh+1,Nmesh+1);
% Nmesh_obs=10; obs_idx=1+Nmesh/Nmesh_obs.*(0:Nmesh_obs);
% u=u(obs_idx,obs_idx);
% y=u(:)+.1.*randn((1+Nmesh_obs)^2,1);

% load data
load('ellipticPDE_Nmesh50.mat');
N=length(y);
Ndesg=n;

% sampling setting
TrjL = 2; Nleap = 5; stepsz = TrjL/Nleap;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp-NBurnIn,D);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
PDE20=ePDEsetup(meshsz);

% Initialize
theta = randn(D,1);
[u,du] = UdU(y,theta,PDE20,[0,1]);
[G,invG,~,ChS1,dlogrtG] = Met(theta,N,PDE20,[0,-1,3,-3]);
met.G=G; met.cholinvG=chol(invG); met.ChS1=ChS1;
met.dphi=du+dlogrtG;

disp(' ');
disp('Running LMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with LMC
    [theta,u,met,acpt_idx] = LMC(theta,u,met,@(theta,der)U(y,theta,PDE20,nargin==2),...
                                 @(theta)Met(theta,N,PDE20,[0,-1,3,-3]),stepsz,Nleap);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
% plot(samp(:,1),samp(:,2),'ro');
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['Banana_LMC_D',num2str(D),'_Nleap',num2str(Nleap),'__', curTime];
save(['result/',curfile,'.mat'],'samp','stepsz','Nleap','acpt','time');
disp(' ');
disp(['Accpetance Rate of LMC: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');