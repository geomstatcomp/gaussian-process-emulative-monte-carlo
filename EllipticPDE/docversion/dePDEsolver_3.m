% This is to get derivatives of solution to a classic example of elliptic equation given mesh and boundary conditions %
% d(kdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;
% output: d_theta u;

function du=dePDEsolver(theta,PDE,u,c,S,dPDE)
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
pb=PDE.pb; % problem (boudary condition)
rtev_KL=PDE.rtev_KL; evc_KL=PDE.evc_KL; % 6 largest eigen values/vectors of integral operator for KL expansion

% Nt = size(t,2); % number of triangles in the mesh
% coeff=-exp(sum(repmat(theta.*rtev_KL,[1,size(evc_KL,2)]).*evc_KL));
coeff=c;

du=zeros(size(p,2),length(theta));
[Q,G,H,R] = assemb(dPDE.pb,dPDE.p,dPDE.e);
figure(1);
for k=1:length(theta)
    % specify coefficients
    d_kcoeff=rtev_KL(k)*evc_KL(k,:).*coeff;

    % assembling matrices on RHS
    [K_k,F_k] = assempde(pb,p,e,t,d_kcoeff,0,0);
    % solution
    d_ku = assempde(-S,0,K_k*u-F_k,Q,G,H,R);
    du(:,k) = d_ku;
    % plot
    subplot(2,3,k);
    pdesurf(p,t,d_ku);
end

end