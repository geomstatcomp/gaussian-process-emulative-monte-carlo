% This is to set up to jointly solve a classic example of elliptic equation and its derivatives %
% d(kdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;

function dPDE=dePDEsetup(meshsz,numberOfPDE)
if nargin==0
    meshsz=30; numberOfPDE=2;
elseif nargin==1
    numberOfPDE=2;
end

% define solution domain
gdm = [3 4 0 1 1 0 0 0 1 1]';
g = decsg(gdm, 'S1', ('S1')');
% plot it
% figure;
% pdegplot(g, 'edgeLabels', 'on');
% axis([-.1 1.1 -.1 1.1]);
% title 'Geometry With Edge Labels Displayed';

% creat triangular mesh
% hmax = .1; % element size
% [p,e,t] = initmesh(g, 'Hmax', hmax);
% try regular mesh
[p,e,t] = poimesh(g,meshsz(1),meshsz(end));
% figure;
% pdeplot(p,e,t);
% axis equal
% title 'Plate With Mesh'
% xlabel 'X-coordinate, meters'
% ylabel 'Y-coordinate, meters'

% Create a pde entity for a PDE with multiple dependent variables
pb = pde(numberOfPDE);
% Create a geometry entity
pg = pdeGeometryFromEdges(g);
% specify boundary conditions
bc1 = pdeBoundaryConditions(pg.Edges(1),'u',@(problem,region,state)[region.x;0]);
bc3 = pdeBoundaryConditions(pg.Edges(3),'u',@(problem,region,state)[1-region.x;0]);
bc24 = pdeBoundaryConditions(pg.Edges([2,4]),'g',zeros(numberOfPDE,1));
pb.BoundaryConditions = [bc1,bc3,bc24]; % all boundary conditions

% output
dPDE.p=p;dPDE.e=e;dPDE.t=t;
dPDE.pb=pb;

end
