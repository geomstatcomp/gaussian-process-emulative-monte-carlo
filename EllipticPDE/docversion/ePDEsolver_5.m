% This is to solve a classic example of elliptic equation given mesh and
% boundary conditions and get the derivatives of solution wrt some design
% parameter theta.
% solution: u; derivative: pu, p2u satisfying
% d(cdu)=0;       % -d(cdpu)=d(pcdu);   % -d(cdp2u)=d(p2cdu)+2d(pcdpu);
% u(x,0)=x;       % pu(x,0)=0;          % p2u(x,0)=0;
% u(x,1)=1-x;     % pu(x,1)=0;          % p2u(x,1)=0;
% d_x u(0,y)=0;   % d_x pu(0,y)=0;      % d_x p2u(0,y)=0;
% d_x u(1,y)=0;   % d_x pu(1,y)=0;      % d_x p2u(1,y)=0;
% output: [u,p_theta u,p_ij u];

function [u,pu,p2u]=ePDEsolver(theta,PDE,opt,PLOT)
if nargin<3
    opt=0; PLOT=0;
elseif nargin<4
    PLOT=0;
end
u=[]; pu=[]; p2u=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b; pb=PDE.pb; % problems (with boudary conditions)
% 6 largest eigen values/vectors of integral operator for KL expansion
rtev_KL=PDE.rtev_KL; evc_KL=PDE.evc_KL;

% specify coefficients
c=-exp(sum(repmat(theta.*rtev_KL,[1,size(evc_KL,2)]).*evc_KL));

if any(opt==0)
    % solution of u
    [K,M,F,Q,G,H,R] = assempde(b,p,e,t,c,0,0);
    u = assempde(K,M,F,Q,G,H,R);
    % plot the solution
    if PLOT
        figure(1);
        pdesurf(p,t,u);
    end
end
if any(opt==1)
    if ~any(opt==0)
        error('u has to be solved first!');
    end
    N=length(u); D=length(theta);
    pu=zeros(N,D);
    pc=(rtev_KL*c).*evc_KL; % derivatives of coefficients wrt theta
    [Q,G,H,R] = assemb(pb,p,e); % assemble boundary condition for pu
    if any(opt==2)
        p2u=zeros(N,D,D);
        p2c=bsxfun(@times,reshape(pc,D,1,[]),reshape(repmat(rtev_KL,[1,size(evc_KL,2)]).*evc_KL,1,D,[]));
    end
    if PLOT
        fig2=figure(2);
        Ncol=ceil(sqrt(D));
        Nrow=ceil(D/Ncol);
        if any(opt==2)
            fig3=figure(3);
        end
    end
    for i=1:D
        % assembling matrices on RHS
        [K_i,F_i] = assempde(pb,p,e,t,pc(i,:),0,0);
        % solution of p_iu
        p_iu = assempde(K,0,-K_i*u+F_i,Q,G,H,R);
        pu(:,i) = p_iu;
        % plot the solutions
        if PLOT
            set(0,'CurrentFigure',fig2);
            subplot(Nrow,Ncol,i);
            pdesurf(p,t,p_iu);
        end
        % solve p_iju
        if any(opt==2)
            for j=1:i
                [K_ij,F_ij] = assempde(pb,p,e,t,p2c(i,j,:),0,0);
                p_iju = assempde(K,0,-K_ij*u+F_ij+2*(-K_i*pu(:,j)+F_i),Q,G,H,R);
                p2u(:,i,j) = p_iju; p2u(:,j,i) = p_iju;
                if PLOT
                    set(0,'CurrentFigure',fig3);
                    subplot(D,D,sub2ind([D,D],j,i))
                    pdesurf(p,t,p_iju);
                end
            end
        end
    end
end
if any(opt==2)&&~all(ismember([0,1],opt))
    error('u and pu has to be solved first!');
end

end