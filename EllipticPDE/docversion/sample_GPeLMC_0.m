%%%% This is MCMC sampling by GPeLMC %%%%
clear;
addpath('../../');
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% % generate data
% theta_truth=[-1 -1 0 0 1 1]'; D=length(theta_truth);
% Nmesh=50;
% % prepare PDE solver
% PDE50=ePDEsetup(Nmesh);
% u=reshape(ePDEsolver(theta_truth,PDE50),Nmesh+1,Nmesh+1);
% Nmesh_obs=10; obs_idx=1+Nmesh/Nmesh_obs.*(0:Nmesh_obs);
% u=u(obs_idx,obs_idx);
% y=u(:)+.1.*randn((1+Nmesh_obs)^2,1);
% 
% % Design information
% n=40; Ndesg=n;
% Range=repmat([-3;3],[1,D]); % range of variables
% De=repmat(Range(1,:),[n,1])+lhsdesign(n,D).*repmat(diff(Range),[n,1]); % Latin hypercube
% % u_D=table2array(rowfun(@(theta)U(y,theta',PDE50),table(De)));
% u_D=U(y,De,PDE50);
% du_D=[];

% load data
load('ellipticPDE_Nmesh50.mat');
Ndesg=n;

% build emulator
emu=GPe_sc(De,u_D,du_D,10*ones(D,1),gFI_D,1);

% sampling setting
TrjL = 2; Nleap = 8; stepsz = TrjL/Nleap;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp-NBurnIn,D);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate

% Initialize
% true values
theta = -ones(D,1); theta(2:2:D) = 1.5;
u = U(y,theta,sigma2y,sigma2theta);
met.cholG=chol(Met(theta,N,sigma2y,sigma2theta));
% emulations
[~,du,~,G,ChS1] = emu.pred(theta',[1,2,11,21]); invG = inv(G); ChS1=reshape(ChS1,D,D^2);
met.G=G; met.ChS1=ChS1;
met.dphi=du + sum(repmat(invG(:)',[D,1]).*ChS1,2);

disp(' ');
disp('Running GPeLMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with GPeLMC
    [theta,u,met,acpt_idx] = GPeLMC(theta,u,met,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),...
                                    @(theta)Met(theta,N,sigma2y,sigma2theta),...
                                    @(theta)emu.pred(theta',[1,2,11,21]),stepsz,Nleap);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
% plot(samp(:,1),samp(:,2),'ro');
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['Banana_GPeLMC_D',num2str(D),'_Nleap',num2str(Nleap),'_Ndesg',num2str(Ndesg),'__',curTime];
save(['result/',curfile,'.mat'],'samp','stepsz','Nleap','acpt','time');
disp(' ');
disp(['Accpetance Rate of GPeLMC: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');