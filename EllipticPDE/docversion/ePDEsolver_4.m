% This is to solve a classic example of elliptic equation given mesh and
% boundary conditions and get the derivatives of solution wrt some design
% parameter theta.
% solution: u; derivative: pu satisfying
% d(kdu)=0;             % d(pkdu+kdpu)=0; or d(kdpu)=-d(pkdu);
% u(x,0)=x;             % pu(x,0)=0;
% u(x,1)=1-x;           % pu(x,1)=0;
% d_x u(0,y)=0;         % d_x pu(0,y)=0;
% d_x u(1,y)=0;         % d_x pu(1,y)=0;
% output: [u,p_theta u];

function [u,pu]=ePDEsolver(theta,PDE,opt,PLOT)
if nargin<3
    opt=0; PLOT=0;
elseif nargin<4
    PLOT=0;
end
u=[]; pu=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b; pb=PDE.pb; % problems (with boudary conditions)
% 6 largest eigen values/vectors of integral operator for KL expansion
rtev_KL=PDE.rtev_KL; evc_KL=PDE.evc_KL;

% specify coefficients
c=-exp(sum(repmat(theta.*rtev_KL,[1,size(evc_KL,2)]).*evc_KL));

if any(opt==0)
    % solution of u
    [K,M,F,Q,G,H,R] = assempde(b,p,e,t,c,0,0);
    u = assempde(K,M,F,Q,G,H,R);
    % plot the solution
    if PLOT
        figure(1);
        pdesurf(p,t,u);
    end
end
if any(opt==1)
    if ~any(opt==0)
        error('u has to be solved first!');
    end
    D=length(theta);
    pu=zeros(length(u),D);
    pc=(rtev_KL*c).*evc_KL; % derivatives of coefficients wrt theta
    [Q,G,H,R] = assemb(pb,p,e); % assemble boundary condition for pu
    if PLOT
        figure(2);
    end
    for k=1:D
        % assembling matrices on RHS
        [K_k,F_k] = assempde(pb,p,e,t,pc(k,:),0,0);
        % solution of p_ku
        p_ku = assempde(-K,0,K_k*u-F_k,Q,G,H,R);
        pu(:,k) = p_ku;
        % plot the solutions
        if PLOT
            subplot(2,3,k);
            pdesurf(p,t,p_ku);
        end
    end
end

end