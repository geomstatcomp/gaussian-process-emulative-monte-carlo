% This is to get derivatives of solution to a classic example of elliptic equation given mesh and boundary conditions %
% d(kdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;
% output: d_theta u;

function [u,du]=dePDEsolver(theta,dPDE,sigma2,l)
if nargin==2
    sigma2=1; l=0.2;
end

% Npdes = dPDE.pb.PDESystemSize;
% prepare to solve PDE
p=dPDE.p;e=dPDE.e;t=dPDE.t; % mesh
pb=dPDE.pb; % problem (boudary condition)


d=1;
% specify coefficients
c = @(p,t,u,time)coeffunction(p,t,theta,sigma2,l,d);

% solution
u = assempde(pb,p,e,t,c,0,0);


end

function coeff = coeffunction(p,t,theta,sigma2,l,d)
% Triangle point indices
it1 = t(1,:);
it2 = t(2,:);
it3 = t(3,:);
% Find centroids of triangles
centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;

[D,N] = size(centroid);
dist = bsxfun(@minus,reshape(centroid',[N,1,D]),reshape(centroid',[1,N,D]));
C = sigma2.*exp(-sum(dist.^2,3)/(2*l^2));

[evc,ev] = eigs(C); % 6 largest eigen values/vectors of integral operator for KL expansion
rtev=sqrt(diag(ev));
evc=evc';
coeff = exp(sum(repmat(theta.*rtev,[1,N]).*evc));

dcoeff = rtev(d)*evc(d,:).*coeff; % derivative wrt d-th element of theta

coeff=[coeff;dcoeff];

end