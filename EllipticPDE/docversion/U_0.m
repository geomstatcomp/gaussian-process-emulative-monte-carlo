%% energy function %%
function [output] = U(y,theta,PDE,der)
% y data observed at (11,11) regular mesh points
% theta parameter of diffusive field k
if(nargin<4)
    der=0;
end

% forward model to solve elliptic PDE given theta
Nmesh=sqrt(size(PDE.p,2))-1; % mesh size has to be (Nmesh,Nmesh)
u=reshape(ePDEsolver(theta,PDE),Nmesh+1,Nmesh+1);
Nmesh_obs=sqrt(length(y))-1; obs_idx=1+Nmesh/Nmesh_obs.*(0:Nmesh_obs);
u=u(obs_idx,obs_idx);
u=u(:);

if der==0
    loglik = -sum((y-u).^2)/(2*.01);
    logpri = -sum(theta.^2)/2;
    output = -(loglik+logpri);
elseif der==1
    dloglik = 0; % to be calculated?
    dlogpri = -theta;
    output  = -(dloglik + dlogpri);
else
    disp('wrong choice of der!');
end

end