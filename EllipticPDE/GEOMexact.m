%% Geometric quantities includeing u,du,G,dG,ChS1,etc. %%
function [u,du,G,invG,dG,ChS1,dlogrtG] = GEOMexact(y,theta,PDE,sigma2y,sigma2theta,opt)
if(nargin<6)
    opt=0;
end
u=[];du=[];G=[];invG=[];dG=[];ChS1=[];dlogrtG=[];

N=length(y); D=length(theta);
Nmesh=sqrt(size(PDE.p,2)); % mesh size has to be (Nmesh,Nmesh)
obs_idx=linspace(1,Nmesh,sqrt(N));
[obs_mesh1,obs_mesh2]=meshgrid(obs_idx,obs_idx);
idx=sub2ind([Nmesh,Nmesh],obs_mesh2(:),obs_mesh1(:));
% forward model to solve elliptic PDE given theta
[sol,dsol,d2sol]=ePDEsolver(theta,PDE,opt);
if ~isempty(sol)
    s=sol(idx); % (N,1)
end
if ~isempty(dsol)
    ds=dsol(idx,:); % (N,D)
end
if ~isempty(d2sol)
    d2s=d2sol(idx,:,:); % (N,D,D)
end

% calcuate exact geometric quantities
if any(opt==0)
    loglik = -sum((y-s).^2)/(2*sigma2y);
    logpri = -theta'*theta/(2*sigma2theta);
    u = -(loglik+logpri);
end
if any(opt==1)
    dloglik = sum(repmat(y-s,[1,D]).*ds)'/sigma2y;
    dlogpri = -theta./sigma2theta;
    du = -(dloglik + dlogpri);
end
if any(opt==2)
    G = (ds'*ds)./sigma2y;
    G(1:D+1:D^2) = G(1:D+1:D^2) + 1/sigma2theta;
end
if any(opt==-2)
    if all(opt~=2)
        error('G has to be calculated first!');
    end
    invG = inv(G);
end
if any(opt==3)
    dG = reshape(ds'*d2s(:,:),repmat(D,1,3));
    dG = dG + permute(dG,[2,1,3]);
    dG = dG./sigma2y;
end
if any(opt==33)
    if any(opt==3)
        ChS1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
    else
        ChS1 = reshape(d2s(:,:)'*ds,repmat(D,1,3))./sigma2y;
    end
end
if any(opt==-33)
    if all(opt~=-2)
        error('invG has to be calculated first!');
    end
    if any(opt==33)
        dlogrtG = sum(repmat(invG(:)',[D,1]).*ChS1(:,:),2);
    elseif any(opt==3)
        dlogrtG = sum(repmat(invG(:),[1,D]).*reshape(dG,D^2,D))'/2;
    else
        error('dG or ChS1 has be calculated first!');
    end
end

end