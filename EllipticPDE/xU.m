%% energy function on each data item x%%
function [output] = xU(y,theta,PDE,der)
% y data observed at (11,11) regular mesh points
% theta parameter of diffusive field k
if(nargin<4)
    der=0;
end

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end

N=length(y);
Nmesh=sqrt(size(PDE.p,2))-1; % mesh size has to be (Nmesh,Nmesh)
Nmesh_obs=sqrt(N)-1;
obs_idx=1+Nmesh/Nmesh_obs.*(0:Nmesh_obs);
u=zeros(n,N); % store solutions
for i=1:n
    % forward model to solve elliptic PDE given theta
    sol=reshape(ePDEsolver(theta(i,:)',PDE),Nmesh+1,Nmesh+1);
    sol=sol(obs_idx,obs_idx);
    u(i,:)=sol(:);
end

if der==0
    loglik = -(repmat(y',[n,1])-u).^2/(2*.01); % (n,N)
    logpri = repmat(-sum(theta.^2,2)./2/N,[1,N]); % (n,N)
    output = -(loglik+logpri);
elseif der==1
    dloglik = 0; % (nD,N) % to be calculated?
    dlogpri = -repmat(theta(:)./N,[1,N]); % (nD,N)
    output  = -(dloglik+dlogpri);
else
    disp('wrong choice of der!');
end

end