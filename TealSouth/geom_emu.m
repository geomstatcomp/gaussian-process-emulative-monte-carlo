%% emulative geometric quanities including gradient and metric %%
function [u,du,d2u,G,ChS1] = geom_emu(theta,simulator_emu,sigma2theta,opt)
if nargin<4
    opt=0;
end

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end

[u,du,d2u,G,ChS1]=simulator_emu.pred(theta,opt);

if any(opt==0)
    u = -u+sum(theta.^2,2)/(2*sigma2theta);
end
if any(opt==1)
    du = reshape(-du,[],D);
    du = du+theta./sigma2theta;
    if size(du,1)==1
        du=du';
    end
end
if any(opt==2)
    du = reshape(du,[],D);
    du = du + repmat(1/sigma2theta*eye(D),n,1);
end

end