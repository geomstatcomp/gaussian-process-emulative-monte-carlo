% This is to select an initial set ofdesign points from some result %

clear;
addpath('../../');

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

num=xlsread('ExperiementOutputForEmulators_all.xlsx');
theta=num(2:end,2:10);FOPR=num(2:end,11:end-1);
truth=importdata('TS_History.obs');
FOPR_obs=truth.data(:,strcmp(truth.colheaders,'FOPR'))';
TIME=truth.data(:,strcmp(truth.colheaders,'TIME'));

[n,D]=size(theta); N=length(FOPR_obs);
% FOPR_obs=mean(FOPR); % I don't have raw data, so estimate it with all simulations
sigma_FOPR=100;

% process data
theta_mean=mean(theta); theta_std=std(theta);
theta=(theta-repmat(theta_mean,n,1))./repmat(theta_std,n,1);
sigma2theta=1;

% calculate loglikelihoods
logLIK=-(FOPR-repmat(FOPR_obs,n,1)).^2/(2*sigma_FOPR^2);
loglik=sum(logLIK,2);
gFI=logLIK*logLIK'-loglik*loglik'/N;
emu_loglik=GPe_logCL(theta,loglik,[],1e-1*ones(D,1),gFI,sigma2theta,[],1,1);

% build the emulator
n=100;
[De,I]=selectnmaxdist(theta,n);
gFI_D=gFI(I,I);
emu=GPe_logCL(De,loglik(I),[],emu_loglik.tau,gFI_D,sigma2theta,[],1,1);
% refine the emulator
for i=1:10
    disp([num2str(i),'-th batch to process...']);
    batchi=100*(i-1)+1:100*(i-1)+100;
    cand=theta(batchi,:); u_cand=loglik(batchi);
%     emu=refine_MICE(emu,@(theta,der)U_em(y,theta,sigma2y,sigma2theta,nargin==2),@(theta)gFI(y,theta,sigma2y),cand,10);
    emu=emu.refine(cand,u_cand,@(theta,opt)geom_emu(theta,emu_loglik,sigma2theta,opt),2*D+4,0,0,2);
end
% update gFI
[I_in,I_of]=ismember(emu.De,theta,'rows');
emu.gFI_D=gFI(I_of(I_in),I_of(I_in));

% emulate the derivative infromation
n=size(emu.De,1);
[I_in,I_of]=ismember(emu.De,theta,'rows');
dlogLIK=zeros(n*D,N);
for i=1:N
    emu_i=GPe_logCL(emu.De,logLIK(I_of(I_in),i),[],emu.tau,[],[],[],1,1);
    [~,dlogLIK(:,i)]=emu_i.pred(emu.De,1);
end
logLIK_til=[logLIK(I_of(I_in),:);dlogLIK];
loglik_til=sum(logLIK_til,2);
gFI_D=logLIK_til*logLIK_til'-loglik_til*loglik_til'/N;
emu.du_D=sum(dlogLIK,2);emu.gFI_D=gFI_D;
emu=emu.update(1,1);

% refined design
% De=emu.De;
% u_D=emu.u_D;du_D=emu.du_D;gFI_D=emu.gFI_D;
n=size(emu.De,1);
emu_loglik=emu;

save('TealSouth.mat','seed','n','D','N','TIME','FOPR_obs','sigma_FOPR','emu_loglik');