%% emulative gradient of energy function %%
function du = dU_emu(theta,simulator,sigma2theta)

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end

[~,dloglik] = simulator.pred(theta,1);
dloglik = reshape(dloglik,[],D);
dlogpri = -theta./sigma2theta;
du = -(dloglik+dlogpri);
if size(du,1)==1
    du=du';
end

end