% this is to summarize the numerical results (e.g. sampling efficiency) of
% different algorithms.

addpath('./result/');
addpath('./summary/');
% algorithms
alg={'RWM','GPeHMC','GPeRHMC','GPeLMC'};
Nalg=length(alg);
% store measuring statistics
AP=zeros(Nalg,1);
spiter=zeros(Nalg,1);
Eff=zeros(Nalg,3);
MinEffs=zeros(Nalg,1);
spdup=zeros(Nalg,1);

% compute satstistics
for i=1:Nalg
    [ESS,~,Times,Acpts] = CalculateStatistics(['_',alg{i},'_'],'./summary/');
    AP(i) = mean(Acpts);
    spiter(i) = mean(Times);
    Eff(i,:) = mean([ESS.min',ESS.med',ESS.max'],1);
    MinEffs(i) = Eff(i,1)/spiter(i);
    if i==1
        spdup(i) = 1;
    else
        spdup(i) = MinEffs(i)/MinEffs(1);
    end
end
spiter=spiter./1e4;

T = table(AP,spiter,Eff,MinEffs,spdup,'rownames',alg);
writetable(T,'./summary/summary.txt','WriteRowNames',true);

