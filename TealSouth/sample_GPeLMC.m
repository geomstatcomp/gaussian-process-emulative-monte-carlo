%%%% This is MCMC sampling by GPeLMC %%%%
clear;
addpath('../../');
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% load data
% load('simulator_emu.mat');
load('TealSouth.mat');
Ndesg=n;
sigma2theta=1;

% smallest potential energy
options=optimoptions('fminunc','Algorithm','trust-region','GradObj','on','display','iter');
[theta_hat,u_nadir]=fminunc(@(theta)U_emu(theta,emu_loglik,sigma2theta),zeros(D,1),options);

% sampling setting
TrjL = 2; Nleap = 2; stepsz = TrjL/Nleap;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp-NBurnIn,D);
poten = zeros(Nsamp-NBurnIn,1);
accp_GPeMC = 0; % online GPeMC acceptance
acpt_GPeMC = 0; % final GPeMC acceptance rate

% Initialize
theta = theta_hat; u = u_nadir;
[~,du,~,G,ChS1] = geom_emu(theta,emu_loglik,sigma2theta,[1,11,21]); invG = inv(G); ChS1=reshape(ChS1,D,D^2);
met.G=G; met.ChS1=ChS1;
met.cholG = chol(G); % use emulative Fisher information for now
met.dphi=du + sum(repmat(invG(:)',[D,1]).*ChS1,2);

disp(' ');
disp('Running GPeLMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance of GPeMC: ',num2str(accp_GPeMC/100)]); accp_GPeMC=0;
    end
    
    % sample with GPeLMC
    [theta,u,met,acpY_MC] = GPeLMC(theta,u,met,@(theta)U_emu(theta,emu_loglik,sigma2theta),...
                                   [],...%@(theta)Met(),... % use emulative Fisher information for acceptance test for now
                                   @(theta)geom_emu(theta,emu_loglik,sigma2theta,[1,11,21]),stepsz,Nleap);
    % online acceptance rate for GPeMC
    accp_GPeMC = accp_GPeMC + acpY_MC;
    
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        poten(Iter-NBurnIn) = u;
        acpt_GPeMC = acpt_GPeMC + acpY_MC;
    end
    
end

% save results
time=toc;
acpt_GPeMC=acpt_GPeMC/(Nsamp-NBurnIn);
% acpt_indep=acpt_indep/(regtime(Nreg)-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['TealSouth_GPeLMC_D',num2str(D),'_Nleap',num2str(Nleap),'__',curTime];
save(['result/',curfile,'.mat'],'stepsz','Nleap','samp','poten','acpt_GPeMC','time');
disp(' ');
disp(['Accpetance Rate of GPeLMC: ',num2str(acpt_GPeMC)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
fig1=figure(1); set(fig1,'pos',[0 800 900 400]);
idx=floor(linspace(1,size(samp,1),1e3));
subplot(1,2,1);
plot(samp(idx,:));
subplot(1,2,2);
plotmatrix(samp(idx,:));
% plot the posterior
% fig=figure('visible','off');
% contourmatrix_matlab(samp);
% r=600;c=800;
% set(fig,'position',[0 0 c r]); 
% % set(gca,'position',[0 0 c+1 r+1]);
% set(gcf,'papersize',[8 6]);
% set(gcf,'paperposition',[0 0 8 6]);
% print(fig,'-dpdf',['./figure/',curfile,'.pdf']);