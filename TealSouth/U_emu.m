%% energy function %%
function [u,du] = U_emu(theta,simulator,sigma2theta)
u=[];du=[];

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end

opt=[0,nargout>1];
[loglik,dloglik] = simulator.pred(theta,opt);
logpri = -sum(theta.^2,2)/(2*sigma2theta);
u = -(loglik+logpri);

if nargout>1
    dloglik = reshape(dloglik,[],D);
    dlogpri = -theta./sigma2theta;
    du = -(dloglik+dlogpri);
    if size(du,1)==1
        du=du';
    end
end

end