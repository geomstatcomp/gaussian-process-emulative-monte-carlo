%%%% This is MCMC sampling by RWM %%%%
clear;
addpath('../../');
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% load data
% load('simulator_emu.mat');
load('TealSouth.mat');
sigma2theta=1;

% smallest potential energy
options=optimoptions('fminunc','Algorithm','trust-region','GradObj','on','display','iter');
[theta_hat,u_nadir]=fminunc(@(theta)U_emu(theta,emu_loglik,sigma2theta),zeros(D,1),options);

% sampling setting
TrjL = .2; Nleap = 1; stepsz = TrjL/Nleap;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp-NBurnIn,D);
poten = zeros(Nsamp-NBurnIn,1);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate

% Initialize
theta = theta_hat; u = u_nadir;
% u = U_emu(theta,emu_loglik,sigma2theta);

disp(' ');
disp('Running RWM...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with RWM
    [theta,u,acpt_idx] = RWM(theta,u,@(theta)U_emu(theta,emu_loglik,sigma2theta),stepsz);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        poten(Iter-NBurnIn) = u;
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['TealSouth_RWM_D',num2str(D),'_Nleap',num2str(Nleap),'__', curTime];
save(['result/',curfile,'.mat'],'stepsz','Nleap','samp','poten','acpt','time');
disp(' ');
disp(['Accpetance Rate of RWM: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
fig1=figure(1); set(fig1,'pos',[0 800 900 400]);
idx=floor(linspace(1,size(samp,1),1e3));
subplot(1,2,1);
plot(samp(idx,:));
subplot(1,2,2);
plotmatrix(samp(idx,:));
% plot the posterior
% fig=figure('visible','off');
% contourmatrix(samp);
% r=600;c=800;
% set(fig,'position',[0 0 c r]); 
% % set(gca,'position',[0 0 c+1 r+1]);
% set(gcf,'papersize',[8 6]);
% set(gcf,'paperposition',[0 0 8 6]);
% print(fig,'-dpdf',['./figure/',curfile,'.pdf']);