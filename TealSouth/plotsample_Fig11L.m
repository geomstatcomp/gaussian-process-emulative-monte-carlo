% This is to plot samples in Teal South to compare their quality %

addpath('./summary/');

% algorithms
alg={'RWM','GPeHMC'};%,'GPeRHMC','GPeLMC'};
Nalg=length(alg);

fig1=figure(1); set(fig1,'pos',[0 800 700 500]); clf;

files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    subplot(2,1,i);
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_']))
            load(strcat('./summary/', files(j+2).name));
            idx=floor(linspace(1,size(samp,1),1e3));
            plot(samp(idx,:));
            set(gca,'FontSize',15);
            xlabel('iteration (thinned)','FontSize',18);ylabel('parameter','FontSize',18);
            title(alg{i},'FontSize',20);
            break;
        end
    end
end
