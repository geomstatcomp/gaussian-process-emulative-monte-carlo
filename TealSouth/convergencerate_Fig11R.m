% this is to compare the different algorithms in terms of error reducing speed.
clear;
addpath('./4convg/');

% algorithms
alg={'RWM','GPeHMC'};%,'GPeRHMC','GPeLMC'};
Nalg=length(alg);

% plot specifications
colors = distinguishable_colors(Nalg);
styles = {'-','-.','--',':','--','-','--'};

% plot
fig1=figure(1); set(fig1,'pos',[0 800 600 400]); clf;
files = dir('./4convg');
nfiles = length(files) - 2;
for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_']))
            load(strcat('./4convg/', files(j+2).name));
            semilogy(1:1000,-poten(1:1000),styles{i},'color',colors(i,:),'linewidth',2); hold on;
            break;
        end
    end
end
drawnow;
set(gca,'FontSize',15);
ylim([80,800]);
set(gca, 'ytick', 100:100:800);
xlabel('iteration','FontSize',18); ylabel('log posterior','FontSize',18);
lgnd=legend(alg,'FontSize',16,'location','SouthEast');
legend boxoff;
% set(lgnd,'color','none');
title('Convergence','FontSize',20);