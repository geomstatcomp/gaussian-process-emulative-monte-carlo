% This is the generic function to plot pairwise marginal density contours
% as a matrix to visualize high dimensional data by Shiwei Lan (2014).

function H=contourmatrix(X,Ncontour,dim)
if ~exist('kde')
    addpath('~/documents/matlab/');
end

[N,D]=size(X);
if nargin<2
    Ncontour=7; dim=1:D;
elseif nargin<3
    dim=1:D;
end

% open window for plots
H=figure(1); hold on;
L=length(dim);

for i=1:L
    subplot(L,L,sub2ind([L,L],i,i));
    [f,x]=hist(X(:,i),20);
    bar(x,f/trapz(x,f));hold on;
    [F,XI]=ksdensity(X(:,i),'npoints',20);plot(XI,F,'r-','linewidth',2);hold off;
%     histfit(X(:,i),50,'kernel');
    if i>1
        for j=1:i-1
            subplot(L,L,sub2ind([L,L],j,i));
            p=kde(X(:,[i,j])',.1);
            [h,x,y]=hist(p,20);
            contour(x,y,h,Ncontour);
        end
    end
end
hold off;

end