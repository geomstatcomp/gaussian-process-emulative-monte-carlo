% This is to refine the design points using Mutual Information for Comupter
% Experiments by Joakim Beck and Serge Guillas (2014).
% INPUTS:
% emu: emulator based on current design points
% U: function to calculate potential and its gradient on design points
% gFI: function to calcuate the generalized Fisher information
% cand: candidate sets to update the design pool
% k: number of starting points to build the new design
% K: number of points in the complement set as an environment
% OUTPUTS:
% emu: updated emulator
% Ent: entropy of the updated the design

function [emu,Ent]=refine_MICE(emu,U,gFI,cand,k,K)
if nargin<5
    k=5; % number of starting points
    K=10; % number of points in complement set
elseif nargin<6
    K=10;
end
%% initialize
De=emu.De; [n,D]=size(De);
u_D=emu.u_D; du_D=reshape(emu.du_D,[],D);
pM=emu.pM;
% Ent=Ent_cur;

%% preprocess candidate set
% combine the exisiting design points to form the new candidate set
cand=[De;cand];
% narrow down the candidate set to a spreading subset
cand=prepcand(cand,n+k);
% cand=unique(cand,'rows','stable');
% N=size(cand,1); thin_idx=floor(linspace(1,N,min([N,5*n])));
% if length(thin_idx)>n+k
%     cand=cand(datasample(thin_idx,n+k,'replace',false),:);
% else
%     cand=cand(thin_idx,:);
% end
% range of the space that has been explored
Range=[min(cand,[],1);max(cand,[],1)];
% Do this first to save computation
% create the complement set using Latin Hypercube
Dc0=repmat(Range(1,:),[K,1])+lhsdesign(K,D).*repmat(diff(Range),[K,1]); % plays a role as environment aware set, not select from them
% design information of the complement set from emulation
% [u_Dc0,du_Dc0]=emu.pred(Dc0,[0,1]); du_Dc0=reshape(du_Dc0,[],D);

%% build the initial design (k points) and emulator
% choose k starting points based on the highest expotial entropy
[~,~,~,~,~,mspe] = emu.pred(cand,40); % more trustable
[mspe,ord]=sort(mspe,'descend');
% [VAR,ord]=sort(diag(COV));
% rndord=randperm(size(cand,1));
% VAR=VAR(rndord);ord=ord(rndord);
% select the first k points and record the rest
rest=ord(k+1:end); cand_rest=cand(rest,:);
[I_inDe,I_ofDe]=ismember(cand_rest,De,'rows');
% record the design information on the rest points for future use
u_rest=u_D(I_ofDe(I_inDe)); du_rest=du_D(I_ofDe(I_inDe),:);
% calculate initial utility (entropy) function
Ent=sum(log(mspe(1:k)));
% build the initial the design pool and emulator
De(1:k,:)=cand(ord(1:k),:);
u_D(1:k)=U(De(1:k,:)); du_D(1:k,:)=U(De(1:k,:),1); du=du_D(1:k,:);
emu=GPe(De(1:k,:),u_D(1:k),du(:),emu.rho,[],[],[],1,1);

%% build the complement set
% update the candidate set
cand=cand_rest;
% design information of the candidate set
u_cand=zeros(n,1); du_cand=zeros(n,D);
u_cand(I_inDe)=u_rest; du_cand(I_inDe,:)=du_rest;
u_cand(~I_inDe)=U(cand(~I_inDe,:)); du_cand(~I_inDe,:)=U(cand(~I_inDe,:),1);
% % create the complement set using Latin Hypercube
% Dc0=repmat(Range(1,:),[K,1])+lhsdesign(K,D).*repmat(diff(Range),[K,1]); % plays a role as environment aware set, not select from them
% % design information of the complement set
u_Dc0=U(Dc0); du_Dc0=U(Dc0,1);
% total complement
Dc=[Dc0;cand]; 
u_Dc=[u_Dc0;u_cand]; du_Dc=[du_Dc0;du_cand];

%%  select new design points sequentially
for i=k+1:n
    % calculate Var(cand|De) simultaneously
    [~,~,~,~,~,mspe] = emu.pred(cand,40);
    max_id=1; emi=0; % exponential of mutual information
    Ncand=size(cand,1);
    % choose the point with maximum mutual information gain
    for j=1:Ncand
        noj=setdiff(1:K+Ncand,K+j); % indices without current point
        du_noj=du_Dc(noj,:);
        emu_noj=GPe(Dc(noj,:),u_Dc(noj),du_noj(:),emu.rho,[],[],1,0,1);
        [~,~,~,~,~,mspe_j] = emu_noj.pred(cand(j,:),40);
        emi_j=mspe(j)/mspe_j;
        if emi_j>emi
            max_id=j;emi=emi_j;
        end
    end
    % update utility (entropy) function
    Ent=Ent+log(mspe(max_id));
    % add new point to the design pool and update the design information
    De(i,:)=cand(max_id,:);
    u_D(i)=U(De(i,:)); du_D(i,:)=U(De(i,:),1); du=du_D(1:i,:);
    if i<n
        % updaate the emulator
        emu=GPe(De(1:i,:),u_D(1:i),du(:),emu.rho,[],[],[],1,1);
        % remove the added point from the candidate set
        cand=cand(setdiff(1:Ncand,max_id),:);
        % update the complement set
        mv_max=setdiff(1:K+Ncand,K+max_id); % indices after removing the maximum
        Dc=Dc(mv_max,:); u_Dc=u_Dc(mv_max); du_Dc=du_Dc(mv_max,:);
    else
        % updaate the emulator with the full design information at last run
        gFI_D=gFI(De);
        emu=GPe(De,u_D,du_D(:),emu.rho,gFI_D,pM,[],1,1);
    end
end

disp(['Entropy: ',num2str(Ent)]);

end