%%% This is to investigate the effect of gradient information on design points
%%% in emulating gradients, Hessians and Fisher information matrices on new
%%% points.

clear;
% add sarching path
addpath('~/Statistics/EQUIP/GPeHMC/code');
addpath('~/Statistics/EQUIP/GPeHMC/code/EmulativeSampling/sampler');
% addpath('../../');

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% simulated data
D=2;
N=100; C=1;
sigma2y=4; sigma2theta=1;
y=C+sqrt(sigma2y).*randn(N,1);

% range of variables
Range=[-2,-2;2,2];
dim=[1,2];

%%%% gather design points %%%%

% starting point of exploration
theta0=ones(D,1);theta0(2:2:D)=0;
thetai=fminunc(@(theta)U(y,theta,sigma2y,sigma2theta),theta0);
ui = U(y,thetai,sigma2y,sigma2theta); dui = U(y,thetai,sigma2y,sigma2theta,1);

% strat the exploration
Nall=40; Nexpl=0;
theta_all=zeros(Nall,D);
while Nexpl<Nall
    [theta,~,~,acpt_idx]=HMC(thetai,ui,dui,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),.1,20);
    if acpt_idx&&min(sqrt(sum(gsubtract(theta',theta_all(1:max([1,Nexpl]),:)).^2,2)))>.2
        Nexpl=Nexpl+1;
        theta_all(Nexpl,:)=theta;
    end
end

% save 20 points for testing
Ne=20;
test_idx=datasample(1:Nall,Ne,'replace',false); desg_idx=setdiff(1:Nall,test_idx);
Ndesg=Nall-Ne;
theta_test=theta_all(test_idx,:);
theta_desg=theta_all(desg_idx,:);

%%%% GP emulation based all points %%%%
% extended information on all points
U_all=U(y,theta_all,sigma2y,sigma2theta);

%%%% GP emulation based the design points %%%%
% extended information on design points
U_desg=U(y,theta_desg,sigma2y,sigma2theta);
dU_desg=U(y,theta_desg,sigma2y,sigma2theta,1); dU_desg=dU_desg(:);


%%%% compare emulations %%%%
linetype={'b--','r-.','g-.'};

%%%% GP emulator without derivative information %%%%
emu=GPe(theta_desg,U_desg,[],ones(D,1),[],[],[],1);
% emu=GPe_logCL(theta_desg,U_desg,[],ones(D,1),[],[],[],1);

%%%%% GP emulator with derivative information %%%%
emu_til=GPe(theta_desg,U_desg,dU_desg,ones(D,1),[],[],[],1);
% emu_til=GPe_logCL(theta_desg,U_desg,dU_desg,ones(D,1),[],[],[],1);

%%%% GP emulator with all points (no derivative information) %%%%
emu_all=GPe(theta_all,U_all,[],ones(D,1),[],[],[],1);
% emu_all=GPe_logCL(theta_all,U_all,[],ones(D,1),[],[],[],1);

%%%% compare contours %%%%
fig1=figure(1); clf;
% set(fig2,'windowstyle','docked');
set(fig1,'pos',[0 800 1000 400]);
hold on;
% partial design points (no derivative information)
subplot(1,4,1,'position',[.05,.12,.195,.8]);
ezcontour(@(theta1,theta2)exp(-emu.pred([theta1,theta2],0)),Range(:)); hold on;
title([num2str(Ndesg),'pts (no derivatives)'],'fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15)
% add design points
plot(theta_desg(:,dim(1)),theta_desg(:,dim(2)),'ko','markersize',10);

% all points (no derivative information)
subplot(1,4,2,'position',[.295,.12,.195,.8]);
ezcontour(@(theta1,theta2)exp(-emu_all.pred([theta1,theta2],0)),Range(:)); hold on;
title([num2str(Nall),'pts (no derivatives)'],'fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
% add design points
plot(theta_all(:,dim(1)),theta_all(:,dim(2)),'ko','markersize',10);

% True one
subplot(1,4,3,'position',[.54,.12,.195,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,[theta1,theta2],sigma2y,sigma2theta)),Range(:)); hold on;
title('True density','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
% % add design points
% plot(theta_desg(:,dim(1)),theta_desg(:,dim(2)),'ko','markersize',10);

% with derivative information
subplot(1,4,4,'position',[.785,.12,.195,.8]);
ezcontour(@(theta1,theta2)exp(-emu_til.pred([theta1,theta2],0)),Range(:)); hold on;
title([num2str(Ndesg),'pts (with derivatives)'],'fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
% add design points
plot(theta_desg(:,dim(1)),theta_desg(:,dim(2)),'ko','markersize',10);
% add gradients
dU_desg=reshape(dU_desg,[],D);
quiver(theta_desg(:,dim(1)),theta_desg(:,dim(2)),dU_desg(:,dim(1)),dU_desg(:,dim(2)),1,'r-','linewidth',2);