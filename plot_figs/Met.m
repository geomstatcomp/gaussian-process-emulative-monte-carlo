%% Metric %%
function [G,invG,dG,ChS1,dlogrtG] = Met(theta,N,sigma2y,sigma2theta,opt)
if(nargin<5)
    opt=0;
end
G=[];invG=[];dG=[];ChS1=[];dlogrtG=[];

D=length(theta);
dmu = ones(D,1); dmu(2:2:D) = 2.*theta(2:2:D);

if any(opt==0)
    G = N/sigma2y.*(dmu*dmu');
    G(1:D+1:D^2) = G(1:D+1:D^2) + 1/sigma2theta;
end
if any(opt==-1)
    if all(opt~=0)
        error('G has to be calculated first!');
    end
%     invG = inv(G);
    invG = sigma2theta.*(eye(D)-(dmu*dmu')./(sigma2y/N/sigma2theta+dmu'*dmu));
end
if any(opt==1)
    dG = zeros(repmat(D,1,3));
    for k=2:2:D
        dG(k,:,k) = N/sigma2y.*(2*dmu'); dG(:,k,k) = dG(:,k,k) + N/sigma2y.*(2*dmu);
    end
end
if any(opt==3)
    if all(opt~=1)
        error('dG has to be calculated first!');
    end
    ChS1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
end
if any(opt==-3)
    if all(opt~=-1)
        error('invG has to be calculated first!');
    end
    if any(opt==3)
        dlogrtG = sum(repmat(invG(:)',[D,1]).*ChS1(:,:),2);
    elseif any(opt==1)
        dlogrtG = sum(repmat(invG(:),[1,D]).*reshape(dG,D^2,D))'/2;
    else
        error('dG or ChS1 has be calculated first!');
    end
end

end