%% Hessian %%
function [H,InvH] = Hess(theta,y,sigma2y,sigma2theta,opt)
if(nargin<5)
    opt=0;
end

D=length(theta); lengthy=length(y);
mu = sum([theta(1:2:D),theta(2:2:D).^2]);
dmu = ones(D,1); dmu(2:2:D) = 2.*theta(2:2:D);

G = lengthy/sigma2y.*(dmu*dmu');
G(1:D+1:D^2) = G(1:D+1:D^2) + 1/sigma2theta;
H=G;
H(1+1+D:2*(D+1):D^2)=H(1+1+D:2*(D+1):D^2)-2*sum(y-mu)/sigma2y;
if all(opt==0)
    InvH=NaN;
else
    if any(opt==-1)
        InvH = inv(H);
    end
end

end