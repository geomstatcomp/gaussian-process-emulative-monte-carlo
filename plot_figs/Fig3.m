%%% This is to investigate the effect of number of design points in
%%% emulating gradients, Hessians and Fisher information matrices on new
%%% points.

clear;
% add sarching path
% addpath('~/Statistics/EQUIP/GPeHMC/code');
addpath('~/Statistics/EQUIP/GPeHMC/code/EmulativeSampling/sampler');
addpath('../../');

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% simulated data
D=2;
N=100; C=1;
sigma2y=4; sigma2theta=1;
y=C+sqrt(sigma2y).*randn(N,1);

% range of variables
Range=[-2,-2;2,2];
% draw posterior
dim=[1,2];
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 1000 500]);
subplot(1,2,1,'position',[.06,.12,.42,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,[theta1,theta2],sigma2y,sigma2theta)),Range(:));
title('Emulated gradient and Fisher information','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
hold on;

%%%% gather design points %%%%

% starting point of exploration
theta0=ones(D,1);theta0(2:2:D)=0;
thetai=fminunc(@(theta)U(y,theta,sigma2y,sigma2theta),theta0);
ui = U(y,thetai,sigma2y,sigma2theta); dui = U(y,thetai,sigma2y,sigma2theta,1);

% strat the exploration
Ndesg=35; Nexpl=0;
theta_desg=zeros(Ndesg,D);
while Nexpl<Ndesg
    [theta,~,~,acpt_idx]=HMC(thetai,ui,dui,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),.1,20);
    if acpt_idx&&min(sqrt(sum(gsubtract(theta',theta_desg(1:max([1,Nexpl]),:)).^2,2)))>.25
        Nexpl=Nexpl+1;
        theta_desg(Nexpl,:)=theta;
    end
end

% save 5 points for testing
Ne=5;
% test_idx=datasample(1:Ndesg,Ne,'replace',false);
test_idx=[3,12,22,33,35];
desg_idx=setdiff(1:Ndesg,test_idx);
Ndesg=Ndesg-Ne;
theta_test=theta_desg(test_idx,:);
theta_desg=theta_desg(desg_idx,:);
% plot true gradients
plot(theta_test(:,dim(1)),theta_test(:,dim(2)),'ks','markersize',10,'linewidth',2);
U_true=U(y,theta_test,sigma2y,sigma2theta);
grad=U(y,theta_test,sigma2y,sigma2theta,1);
quiver(theta_test(:,dim(1)),theta_test(:,dim(2)),grad(:,dim(1)),grad(:,dim(2)),.7,'k-','linewidth',3);
% plot natural gradients
% grad_natr=cell2mat(arrayfun(@(i)FI{i}(dim,dim)\grad(i,:)',1:Ne,'UniformOutput', false))';
% quiver(theta_test(:,dim(1)),theta_test(:,dim(2)),grad_natr(:,dim(1)),grad_natr(:,dim(2)),.5,'m-','linewidth',2);
% plot true Hessians
% hess=arrayfun(@(i)Hess(theta_test(i,:),y,sigma2y,sigma2theta),1:Ne,'UniformOutput', false); % true hessians
% arrayfun(@(i)plot2dGaussian(theta_test(i,dim),.01.*hess{i}(dim,dim),'k-'),1:Ne);
% plot true Fisher information
FI=arrayfun(@(i)Met(theta_test(i,:),N,sigma2y,sigma2theta),1:Ne,'UniformOutput', false); % Fisher
plot2dGaussian(theta_test(1,dim),.07^2.*FI{1}(dim,dim),'k-',3);

% get the legend correct
Nuse=floor(Ndesg.*[.2,.5,1]);
linetype={'b--','r-.','g-.'};
circtype={'bo','ro','go'};
for i=1:length(Nuse)
    plot([10,10],'ko','markersize',6+(i-1)*4);
    plot([10,10],[10.1,10.1],linetype{i},'linewidth',2);
end
lg_desg=[num2str(Nuse'),repmat(' design pts',size(Nuse'))];
lg_grad=[repmat('emulation (',size(Nuse')),num2str(Nuse'),repmat(' pts)',size(Nuse'))];
lg=legend('density','test pts','true gradient','true Fisher',lg_desg(1,:),lg_grad(1,:),lg_desg(2,:),lg_grad(2,:),lg_desg(3,:),lg_grad(3,:),'location','southeast');
set(lg,'fontsize',11);
legend BOXOFF;

% add other true Fisher
arrayfun(@(i)plot2dGaussian(theta_test(i,dim),.07^2.*FI{i}(dim,dim),'k-',3),1:Ne);


% plot2dGaussian(theta_test(1,dim),.07^2.*FI{1}(dim,dim),'k-',3);


%%%% GP emulation based the design points %%%%
% extended information on design points
U_desg=U(y,theta_desg,sigma2y,sigma2theta); dU_desg=U(y,theta_desg,sigma2y,sigma2theta,1);
U_til_desg=[U_desg;dU_desg(:)];
H_til_desg=[[ones(Ndesg,1),theta_desg];[zeros(Ndesg*D,1),kron(eye(D),ones(Ndesg,1))]];
% optimal gp parameters
Dist_desg=bsxfun(@minus,reshape(theta_desg,[Ndesg,1,D]),reshape(theta_desg,[1,Ndesg,D]));
rho_hat=fminunc(@(rho)-gpLik(rho,Dist_desg,H_til_desg,U_til_desg),10*ones(D,1));
% generalized Fisher information on design points for emulating FI
xU_desg=xU(y,theta_desg,sigma2y,sigma2theta); dxU_desg=xU(y,theta_desg,sigma2y,sigma2theta,1);


%%%% compare the effect of the number of design points in emulation %%%%

% number of points to use to predict
Nuse=floor(Ndesg.*[.2,.5,1]);
% linetype={'b--','r-.','g-.'};
% circtype={'bo','ro','go'};

% rho_ini=5*ones(D,1);
for i=1:length(Nuse)
% plot these design points
% if i==1
%     plot(theta_desg(1:Nuse(1),dim(1)),theta_desg(1:Nuse(1),dim(2)),'ko','markersize',10); hold on;
% else
%     plot(theta_desg(Nuse(i-1)+1:Nuse(i),dim(1)),theta_desg(Nuse(i-1)+1:Nuse(i),dim(2)),'ko','markersize',10+(i-1)*2);
% end
plot(theta_desg(1:Nuse(i),dim(1)),theta_desg(1:Nuse(i),dim(2)),'ko','markersize',6+(i-1)*4); hold on;

% calculation based on optimal GP parameter
Ui=U_desg(1:Nuse(i)); dUi=dU_desg(1:Nuse(i),:); dUi=dUi(:);
gFI_til=gFI_generic(xU_desg(1:Nuse(i),:),dxU_desg(bsxfun(@plus,(1:Nuse(i))',(0:D-1).*Ndesg),:));
% define GP emulator
emu=GPe(theta_desg(1:Nuse(i),:),Ui,dUi,rho_hat,gFI_til,sigma2theta,[],1,1);

% plot emulative gradients and Fisher information
emu.plot(theta_test,dim,[.7,.07],linetype{i});

end
% lg_desg=[num2str(Nuse'),repmat(' design pts',size(Nuse'))];
% lg_grad=[repmat('emulative grad-',size(Nuse')),num2str(Nuse')];
% legend('density','test pts','true grad','true FI',lg_desg(1,:),lg_grad(1,:),lg_desg(2,:),lg_grad(2,:),lg_desg(3,:),lg_grad(3,:),'location','southeast');
% legend BOXOFF;
hold off;



%%%% show emulation in the filed %%%%
subplot(1,2,2,'position',[.54,.12,.42,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,[theta1,theta2],sigma2y,sigma2theta)),Range(:));
title('Emulation in the field','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
hold on;

% plot these design points
plot(theta_desg(:,dim(1)),theta_desg(:,dim(2)),'ko','markersize',10);

meshx=linspace(-2,2,5);
[sX,sY]=meshgrid(meshx,meshx);
E=[sX(:),sY(:)];
m=size(E,1);

% plot true gradients and Fisher information
du_E=U(y,E,sigma2y,sigma2theta,1);
quiver(E(:,dim(1)),E(:,dim(2)),du_E(:,dim(1)),du_E(:,dim(2)),1.5,'k-','linewidth',2);
FI=arrayfun(@(i)Met(E(i,:),N,sigma2y,sigma2theta),1:m,'UniformOutput', false); % Fisher
arrayfun(@(i)plot2dGaussian(E(i,dim),.05^2.*FI{i}(dim,dim),'k-',2),1:m);

% plot emulative gradients Fisher information
emu.plot(E,dim,[1.5,.05],'--r');
hold off;