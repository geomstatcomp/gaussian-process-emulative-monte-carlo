%%%% This is to test the approximation to true density using emulation and mixture of Gaussians. %%%%
clear;
addpath('../../');
addpath('../../EmulativeSampling/sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% load data
load('Banana2D65desg.mat');
n=size(De,1);

%% plot true density
% dimension and range of variables
dim=[1,2]; Range=[-2,-2;2,2]; % range of variables
% draw posterior
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 800 400]);
subplot(1,3,1,'position',[.06,.12,.26,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,[theta1,theta2],sigma2y,sigma2theta)),Range(:));
title('Posterior Density','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
hold on;
% plot all design points
% plot(De(:,dim(1)),De(:,dim(2)),'ko','markersize',10);


%% plot emulated density
% choose number of design points to use
k=30;
% k_idx=1:k;
k_idx=datasample(1:n,k,'replace',false);
De=De(k_idx,:);
u_D=u_D(k_idx,:);
de_idx=bsxfun(@plus,k_idx',(0:D-1).*n);
du_D=du_D(de_idx(:));
if isempty(du_D)
    gFI_D=gFI_D(de_idx,de_idx);
else
    de_idx=bsxfun(@plus,k_idx',(0:D).*n);
    gFI_D=gFI_D(de_idx,de_idx);
end
% build the emulatior
emu=GPe_sc(De,u_D,du_D,10*ones(D,1),gFI_D,sigma2theta);
% plot emulated density
subplot(1,3,2,'position',[.38,.12,.26,.8]);
ezcontour(@(theta1,theta2)exp(-emu.pred([theta1,theta2],0)),Range(:));
title('Emulated Density','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
hold on;
% plot the chosen design points
plot(De(:,dim(1)),De(:,dim(2)),'ko','markersize',10);


%% plot the approximation by mixture of Gaussians
[u,~,~,G] = emu.pred(De,[0,1,11]);
wts=exp(-u)./sum(exp(-u));
G=permute(reshape(G,[k,D,D]),[2,3,1]);
COV=reshape(cell2mat(arrayfun(@(i)inv(G(:,:,i)),1:k,'UniformOutput', false)),[D,D,k]);
% plot mixture of Gaussian
subplot(1,3,3,'position',[.70,.12,.26,.8]);
ezcontour(@(theta1,theta2)exp(logmogpdf([theta1,theta2],De',COV,wts')),Range(:));
title('Approximation of MOG','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
hold on;
% plot the chosen design points
plot(De(:,dim(1)),De(:,dim(2)),'ko','markersize',10);
