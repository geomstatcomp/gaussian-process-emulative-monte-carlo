% This is to compute the generalized empirical Fisher information %

function gFI_til=gFI(y,theta,sigma2y)

[n,D]=size(theta); N=length(y);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end
mu=sum([theta(:,1:2:D),theta(:,2:2:D).^2],2);

xL = -(bsxfun(@minus,mu,y')).^2./sigma2y/2; % (n,N)
dmu = ones(n,D); dmu(:,2:2:D) = 2.*theta(:,2:2:D); dmu = repmat(dmu(:),[1,N]); % (nD,N)
dxL = -repmat(bsxfun(@minus,mu,y'),[D,1]).*dmu./sigma2y; % (nD,N)

xL_til=[xL;dxL];
L_til=sum(xL_til,2);
gFI_til=xL_til*xL_til'-L_til*L_til'./N;
% gFI_til=gFI_til./N;
end