% this is to plot the posterior of 4D banana shaped distribution %

%%%% This is MCMC sampling by HMC %%%%
clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% simulated data
D=4;
N=100; muy=1;
sigma2y=4; sigma2theta=1;
y=muy+sqrt(sigma2y).*randn(N,1);

fig1=figure(1); set(fig1,'pos',[0 800 800 400]);
% range of variables
Range=[-2,-2;2,2];
% banana
dim=[1,2];
subplot(1,3,1,'position',[.06,.12,.26,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,theta1*(1:D==dim(1))+theta2*(1:D==dim(2)),sigma2y,sigma2theta)),Range(:)); hold on;
title('Banana','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
% biscuit
dim=[1,3];
subplot(1,3,2,'position',[.38,.12,.26,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,theta1*(1:D==dim(1))+theta2*(1:D==dim(2)),sigma2y,sigma2theta)),Range(:)); hold on;
title('Biscuit','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);
% doughnut
dim=[2,4];
subplot(1,3,3,'position',[.70,.12,.26,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,theta1*(1:D==dim(1))+theta2*(1:D==dim(2)),sigma2y,sigma2theta)),Range(:)); hold on;
title('Doughnut','fontsize',20);
xlabel(['\theta_',num2str(dim(1))],'fontsize',19);ylabel(['\theta_',num2str(dim(2))],'fontsize',19,'rot',0);
set(gca,'FontSize',15);