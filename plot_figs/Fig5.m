%%%% This is to record some snapshots of the evoluation of design pool %%%%
clear;
addpath('../');
addpath('../../');
addpath('../../EmulativeSampling/sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);
% set message off
% optimoptions('fmincon','display','off');

%% simulated data
D=2;
N=100; muy=1;
sigma2y=4; sigma2theta=1;
y=muy+sqrt(sigma2y).*randn(N,1);
% load('Banana2D50desg.mat'); or load data
% load('Banana2D65desg.mat');

%% set whether to plot
graphon = 1;

% set figure
if graphon
    fig1=figure(1); clf;
%     set(fig1,'windowstyle','docked');
    set(0,'CurrentFigure',fig1);
    set(fig1,'pos',[0 800 1000 400]);
    hold on;
%     subplot(2,2,1,'position',[.08,.57,.38,.38]);
%     subplot(2,2,2,'position',[.08+.48,.57,.38,.38]);
%     subplot(2,2,3,'position',[.08,.57-.5,.38,.38]);
%     subplot(2,2,4,'position',[.08+.48,.57-.5,.38,.38]);
end

% coordiniates in metric plots
dim=[1,2];  Range=[-2,-2;2,2];

%% GPeLMC

% Design information
n=20; Ndesg=n;
Range=repmat([-2;2],[1,D]); % range of variables
% De=repmat(Range(1,:),[n,1])+lhsdesign(n,D).*repmat(diff(Range),[n,1]); % Latin hypercube
% De = repmat([1,0],[n,1])+randn(n,D);
De = repmat([-1,1.5],[n,1])+.1*randn(n,D);
u_D=U(y,De,sigma2y,sigma2theta); du_D=U(y,De,sigma2y,sigma2theta,1);
gFI_D=gFI(y,De,sigma2y);
% build emulator
% emu=GPe_sc(De,u_D,du_D(:),10*ones(D,1),gFI_D,sigma2theta);
emu=GPe(De,u_D,du_D(:),10*ones(D,1),gFI_D,sigma2theta,[],1,1);

% sampling setting
TrjL = 2; Nleap = 5; stepsz = TrjL/Nleap;
regintvl = 20;

% allocation to save
Nsamp = 300; NBurnIn = 0;
samp = zeros(Nsamp-NBurnIn,D);
accp_GPeMC = 0; % online GPeMC acceptance
accp_indep = 0; % online independence sampling acceptance
acpt = 0; % final acceptance rate
regtime = 1; % regeneration time
Ent = 0; % entropy

% Initialize
% true values
theta_cur = -ones(D,1); theta_cur(2:2:D) = 1.5;
u_cur = U(y,theta_cur,sigma2y,sigma2theta);
cholG_cur = chol(Met(theta_cur,N,sigma2y,sigma2theta));
% emualtions
[~,du,~,G_cur,ChS1_cur] = emu.pred(theta_cur',[1,2,11,21]); invG = inv(G_cur);
ChS1_cur=reshape(ChS1_cur,D,D^2);
dphi_cur=du+sum(repmat(invG(:)',[D,1]).*ChS1_cur,2);

% update plots %
if graphon
    % show design points and emulated density contour
    subplot(1,4,1,'position',[.05,.12,.195,.8]);
    ezcontour(@(theta1,theta2)exp(-emu.pred([theta1,theta2],0)),Range(:));
    title('Initial Design','fontsize',20);
    set(gca,'FontSize',15);
    xlabel(['\theta_{',num2str(dim(1)),'}'],'Fontsize',19);
    xlabh = get(gca,'XLabel');
%     set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
    ylabel(['\theta_{',num2str(dim(2)),'}'],'rot',0,'Fontsize',19);
    ylabh = get(gca,'YLabel');
%     set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);
    hold on;
    % plot the chosen design points
    plot(De(:,dim(1)),De(:,dim(2)),'ko','markersize',10);
end


disp(' ');
disp('Running GPeLMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance of GPeMC: ',num2str(accp_GPeMC/100)]); accp_GPeMC=0;
        disp(['Current acceptance of independence sampler: ',num2str(accp_indep/100)]); accp_indep=0;
    end
    
    %% 1st kernel: GPeLMC
    
    % initialization
    theta=theta_cur;
    G=G_cur;
    cholG=cholG_cur;
    ChS1=ChS1_cur;
    dphi = dphi_cur;

    % propose velocity
    z = randn(D,1);
    v = cholG\z;

    % Calculate current energy value
    E_cur  = u_cur - sum(log(diag(cholG))) + (z'*z)/2;

    % Accumulate determinant to be adjusted in acceptance rate
    dtlogJ = 0;

    % randomize the number of Leapfrog steps
    randL=ceil(rand*Nleap);

    theta_leap=theta;
    % Perform leapfrog steps
    for l = 1:randL

        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for velocity
        vChS1=reshape(v'*ChS1,D,D)';
        dtlogJ = dtlogJ - log(det(G+stepsz/2*vChS1));
        v = (G+stepsz/2*vChS1)\(G*v- stepsz/2*dphi);
        vChS1=reshape(v'*ChS1,D,D)';
        dtlogJ = dtlogJ + log(det(G-stepsz/2*vChS1));


        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for position
        theta = theta + stepsz*v;


        % Update the metric and the related quantities
        [~,du,~,G,ChS1] = emu.pred(theta',[1,2,11,21]); invG = inv(G);
        ChS1=reshape(ChS1,D,D^2);
        dphi = du + sum(repmat(invG(:)',[D,1]).*ChS1,2);

        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make aother half step for velocity
        vChS1=reshape(v'*ChS1,D,D)';
        dtlogJ = dtlogJ - log(det(G+stepsz/2*vChS1));
        v = (G+stepsz/2*vChS1)\(G*v- stepsz/2*dphi);
        vChS1=reshape(v'*ChS1,D,D)';
        dtlogJ = dtlogJ + log(det(G-stepsz/2*vChS1));
        
        % update theta_leap for plots
        theta_leap=theta;
    end

    % Calculate proposed energy value
    u = U(y,theta,sigma2y,sigma2theta);
    G_true=Met(theta,N,sigma2y,sigma2theta); cholG=chol(G_true);
    E_prp = u - sum(log(diag(cholG))) + (v'*G_true*v)/2;


    % Accept according to ratio
    logRatio = -E_prp + E_cur + real(dtlogJ);

    if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
        theta_cur=theta; u_cur=u;
        G_cur=G; cholG_cur=cholG; ChS1_cur=ChS1; dphi_cur=dphi;
        acpY = 1;
    else
        acpY = 0;
    end
    
    % online acceptance rate for GPeMC
    accp_GPeMC = accp_GPeMC + acpY;
    
    %% 2nd kernel: independence sampler using predicted Gaussian
    
    % update design pool and emulator
    if mod(Iter,regintvl) == 0
        disp('Regeneration happens!');
%         [De_new,emu,Ent]=refine_mE(De,emu,Ent,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),@(theta)gFI(y,theta,sigma2y),samp(regtime:Iter,:),3);
        [emu,Ent]=refine_MICE(emu,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),@(theta)gFI(y,theta,sigma2y),samp(regtime:Iter,:),10);
%         [emu,Ent]=refine_MIX(emu,Ent,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),@(theta)gFI(y,theta,sigma2y),samp(regtime:Iter,:),10);
        newpt=~ismember(emu.De,De,'rows');
        De=emu.De;
        regtime=Iter;
        % update plots %
        if graphon&&Iter==140
            subplot(1,4,2,'position',[.295,.12,.195,.8]);
            ezcontour(@(theta1,theta2)exp(-emu.pred([theta1,theta2],0)),Range(:));
            title('Adapting...','fontsize',20);
            set(gca,'FontSize',15);
            xlabel(['\theta_{',num2str(dim(1)),'}'],'Fontsize',19);
            xlabh = get(gca,'XLabel');
%             set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
            ylabel(['\theta_{',num2str(dim(2)),'}'],'rot',0,'Fontsize',19);
            ylabh = get(gca,'YLabel');
%             set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);
            hold on;
            % plot the chosen design points
            plot(De(newpt,dim(1)),De(newpt,dim(2)),'ro','markersize',12); hold on;
            plot(De(~newpt,dim(1)),De(~newpt,dim(2)),'ko','markersize',10);
%             pause(.5);
        end
    end
    
    
    % Start timer after burn-in
    if Iter == NBurnIn
%         disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta_cur';
        acpt = acpt + acpY;
    end
    
end

% plot final stage
subplot(1,4,3,'position',[.54,.12,.195,.8]);
ezcontour(@(theta1,theta2)exp(-emu.pred([theta1,theta2],0)),Range(:));
title('Final Design','fontsize',20);
set(gca,'FontSize',15);
xlabel(['\theta_{',num2str(dim(1)),'}'],'Fontsize',19);
xlabh = get(gca,'XLabel');
% set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
ylabel(['\theta_{',num2str(dim(2)),'}'],'rot',0,'Fontsize',19);
ylabh = get(gca,'YLabel');
% set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);
hold on;
% plot the chosen design points
plot(De(newpt,dim(1)),De(newpt,dim(2)),'ro','markersize',12); hold on;
plot(De(~newpt,dim(1)),De(~newpt,dim(2)),'ko','markersize',10);

% True posterior density
subplot(1,4,4,'position',[.785,.12,.195,.8]);
ezcontour(@(theta1,theta2)exp(-U(y,[theta1,theta2],sigma2y,sigma2theta)),Range(:));
title('True density','Fontsize',20);
set(gca,'FontSize',15);
xlabel(['\theta_{',num2str(dim(1)),'}'],'Fontsize',19);
xlabh = get(gca,'XLabel');
% set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
ylabel(['\theta_{',num2str(dim(2)),'}'],'rot',0,'Fontsize',19);
ylabh = get(gca,'YLabel');
% set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);
