%PLOT A 2D Gaussian
function plot2dGaussian(mu,Sigma,linespec,linewidth)
if nargin<3
    linespec='k-'; linewidth=2;
elseif nargin<4
    linewidth=2;
end
% for convenience of plot
mu=squeeze(mu); Sigma=squeeze(Sigma);

if(any([length(mu)~=2,size(Sigma)~=2]))
    error('Only plot 2d Gaussian!');
end

t = linspace(-pi,pi,100);
u = [cos(t); sin(t)];

[V,D] = eig(Sigma);
z = real(V*sqrt(D))*u;

hold on;
plot(mu(1)+z(1,:),mu(2)+z(2,:),linespec,'linewidth',linewidth);

end