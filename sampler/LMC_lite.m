% This is Lite version of LMC

function [q,u,du,met,acpY] = LMC_lite(q_cur,u_cur,du_cur,met_cur,U,Met,eps,L)

% initialization
q = q_cur; D = length(q);
u = u_cur;
G=met_cur.G;
invG=met_cur.invG; cholinvG=chol(invG);


% propose velocity
z = randn(D,1);
v = cholinvG'*z;

% current energy
E_cur = u + sum(log(diag(cholinvG))) + (z'*z)/2;

randL=ceil(rand*L);

v = v - eps/2.*(invG*du_cur);
for i=1:randL
    % forward half step of velocity
%     v = v - eps/2*U(q,1);
    
    % full step evolution of position
    q = q + eps.*v;
    
    % update the metric and the related quantities
    [G,invG] = Met(q);
    du = U(q,1);
    
    % backward half step of velocity
%     v = v - eps/2*U(q,1);
    if(i~=randL)
        v = v - eps.*(invG*du);
    end
    
end
v = v - eps/2.*(invG*du);

% new energy
u = U(q); cholinvG = chol(invG);
E_prp = u + sum(log(diag(cholinvG))) + (v'*G*v)/2;

% Accept according to ratio
logRatio = -E_prp + E_cur + real(dtlogJ);

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    met.G=G; met.invG=invG;
    acpY = 1;
else
    q=q_cur; u=u_cur; du=du_cur; met=met_cur;
    acpY = 0;
end


end