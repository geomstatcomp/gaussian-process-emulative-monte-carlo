%%%% This is generic RWM algorithm %%%%

function [q,u,acpt] = RWM(q_cur,u_cur,U,eps)

q = q_cur;

% sample velocity
v = randn(length(q),1);

% random walk
q = q + eps.*v;

% new energy
u = U(q);

% Accept according to ratio
logRatio = -u + u_cur;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; u = u_cur;
    acpt = 0;
end


end