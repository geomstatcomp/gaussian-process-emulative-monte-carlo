%%%% This is generic RHMC algorithm %%%%

function [q,u,met,acpY] = RHMC(q_cur,u_cur,met_cur,U,Met,eps,L,Nfpiter)

% initialization
q = q_cur; D = length(q);
u = u_cur;
G=met_cur.G;
cholG=met_cur.cholG;
invG=met_cur.invG;
dG=met_cur.dG;
dphi=met_cur.dphi;

% propose momentum
z = randn(D,1);
p = cholG'*z;

% Calculate current energy value
E_cur  = u + sum(log(diag(cholG))) + (z'*z)/2;

% randomize the number of Leapfrog steps
randL=ceil(rand*L);

% Perform leapfrog steps
for l = 1:randL
    
    %%%%%%%%%%%%%%%%%%%
    % Update momentum %
    %%%%%%%%%%%%%%%%%%%
    % Make a half step for momentum
    p_iter=p; invGp_iter=invG*p_iter;
    dK=zeros(D,1);
    for fpiter=1:Nfpiter
        for d=1:D
            dK(d)=-invGp_iter'*dG(:,:,d)*invGp_iter/2;
        end
        p_iter=p-eps/2.*(dphi+dK);
        invGp_iter=invG*p_iter;
    end
    p=p_iter;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%
    % Update q parameters %
    %%%%%%%%%%%%%%%%%%%%%%%
    % Make a full step for position
    q_iter=q; invGp_fix=invGp_iter; invGp=invGp_fix;
    for fpiter=1:Nfpiter
        q_iter=q+eps/2.*(invGp_fix+invGp);
        if fpiter~=Nfpiter
            [~,invG]=Met(q_iter);
        else
            [G,invG,dG,~,dlogrtG]=Met(q_iter);
        end
        invGp=invG*p;
    end
    q=q_iter;
    
    % Update the metric and the related quantities
    dphi = U(q,1) + dlogrtG;
    
    %%%%%%%%%%%%%%%%%%%
    % Update momentum %
    %%%%%%%%%%%%%%%%%%%
    % Make aother half step for momentum
    for d=1:D
        dK(d)=-invGp'*dG(:,:,d)*invGp/2;
    end
    p=p-eps/2.*(dphi+dK);
    
end

try
    % Calculate proposed energy value
    u = U(q); cholG = chol(G); K = (p'*invG*p)/2;
    if K<0
        cholinvGp = cholG'\p; K = cholinvGp'*cholinvGp/2;
    end
    E_prp = u + sum(log(diag(cholG))) + K;

    % Accept according to ratio
    logRatio = -E_prp + E_cur;

    if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
        met.G=G; met.cholG=cholG; met.invG=invG; met.dG=dG; met.dphi=dphi;
        acpY = 1;
    else
        q=q_cur; u=u_cur; met=met_cur;
        acpY = 0;
    end

catch
    q=q_cur; u=u_cur; met=met_cur;
    acpY = 0;
end

end
