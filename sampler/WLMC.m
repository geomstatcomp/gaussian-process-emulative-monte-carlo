%%%% This is generic wormhole LMC algorithm %%%%

function [q,u,met,acpY,jumped] = WLMC(q_cur,u_cur,met_cur,wmhole,xU,xMet,eps,L,Nfpiter)

% initialization in the augmented space
q = q_cur; D = length(q)-1;
u = u_cur;
G=met_cur.G;
cholinvG=met_cur.cholinvG;
ChS1=met_cur.ChS1;
dphi=met_cur.dphi;

% wormhole parameters
modes=wmhole.modes; K=size(modes,1); % number of modes in wormhole
worldist=wmhole.worldist;
unidrecup=wmhole.unidrecup;
whwidth=wmhole.width;

% propose velocity
z = randn(D+1,1);
v = cholinvG'*z;

% Calculate current energy value
E_cur  = u + sum(log(diag(cholinvG))) + (z'*z)/2;

% Accumulate determinant to be adjusted in acceptance rate
dtlogJ = 0;

% adjust energy gap when mode jump happens
jumped = 0; adjust = 0; dtE = 0;

% randomize the number of Leapfrog steps
randL=ceil(rand*L);

% Perform leapfrog steps
for l = 1:randL
    
    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
    % Make a half step for velocity
    vChS1=reshape(v'*ChS1(:,:),D+1,D+1)';
    dtlogJ = dtlogJ - log(det(G+eps/2.*vChS1));
    v = (G+eps/2.*vChS1)\(G*v- eps/2.*dphi);
    vChS1=reshape(v'*ChS1(:,:),D+1,D+1)';
    dtlogJ = dtlogJ + log(det(G-eps/2.*vChS1));
    
    
    %%%%%%%%%%%%%%%%%%%%%%%
    % Update q parameters %
    %%%%%%%%%%%%%%%%%%%%%%%
    % Make a full step for position
    if jumped
        q = q + eps.*v;
    else
%         try
        % build wormhole network to try jump
        q_jump = q;
        [~,closest] = min(sum((repmat(q_jump(1:D)',[K,1])-modes).^2,2));
        endL = [modes(closest,:),sign(q_jump(end))*worldist]';
        endR = [modes,-sign(q_jump(end))*worldist.*ones(K,1)]';
        whdirection = squeeze(unidrecup(closest,:,:))';
        whdirection(D+1,:) = -sign(q_jump(end)).*whdirection(D+1,:); % wormhole upwards or dnwards
        for fpiter = 1:Nfpiter
            % calculate the mollifier
            X2endL = -repmat(q_jump-endL,1,K); X2endR = -(repmat(q_jump,1,K)-endR);
            projL = sum(X2endL.*whdirection); projR = sum(X2endR.*whdirection);
            vicinity = sum(X2endL.*X2endR) + abs(projL.*projR);
            mollifier = exp(-vicinity/D./whwidth(closest,:));
%             if any(isnan(mollifier))
%                 disp('wow!');
%             end
            % decide whether to jump
            if any(isnan(mollifier))||rand<1-sum(mollifier)
                f = v;
            else
%                 disp(['mollifier: ',num2str(mollifier)]);
                jump2 = randsample(K,1,true,mollifier);
%                 disp(['Jumping to mode ' num2str(jump2)]);
                f = 2/eps.*X2endR(:,jump2);
                jumped = 1; adjprob = min([1,sum(mollifier)]);
            end
            if fpiter==1
                f_fix = f;
            end
            % update q_jump via fixed point iteration
            q_jump = q + .5*eps.*(f_fix+f);
        end
        % test whether a mode jump truely happens and adjust energy gap if so
        [~,closest_jump] = min(sum((repmat(q_jump(1:D)',[K,1])-modes).^2,2));
        if closest_jump~=closest&&jumped
            dtE = xU(q_jump) - (xU(q)+(v'*G*v)/2);
            adjust = 1;
        end
        q = q_jump;
%         catch
%             adjust=0;
%         end
    end
    
    
    % Update the metric and the related quantities
    if l~=randL
        [G,~,~,ChS1,dlogrtG] = xMet(q);
    else
        [G,invG,~,ChS1,dlogrtG] = xMet(q);
    end
    dphi = xU(q,1) + dlogrtG;
    
    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
    % Make aother half step for velocity
    vChS1=reshape(v'*ChS1(:,:),D+1,D+1)';
    dtlogJ = dtlogJ - log(det(G+eps/2.*vChS1));
    v = (G+eps/2.*vChS1)\(G*v- eps/2.*dphi);
    vChS1=reshape(v'*ChS1(:,:),D+1,D+1)';
    dtlogJ = dtlogJ + log(det(G-eps/2.*vChS1));
    
    % finish adjusting the energy gap when necessary
    if adjust
        dtE = dtE + (v'*G*v)/2;
        dtE = adjprob*dtE;
        adjust = 0;
    end
    
end

try
    % Calculate proposed energy value
    u = xU(q); cholinvG = chol(invG);
    E_prp = u + sum(log(diag(cholinvG))) + (v'*G*v)/2;

    % Accept according to ratio
    logRatio = -E_prp + E_cur + real(dtlogJ) +dtE;

    if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
        met.G=G; met.cholinvG=cholinvG; met.ChS1=ChS1; met.dphi=dphi;
        acpY = 1;
    else
        q=q_cur; u=u_cur; met=met_cur;
        acpY = 0;
    end
catch
    q=q_cur; u=u_cur; met=met_cur;
    acpY = 0;
end
    
end
