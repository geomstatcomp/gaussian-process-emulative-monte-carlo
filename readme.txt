Gaussian Process emulative Monte Carlo (GPeMC) is a class of geometric MCMC methods
that take advantage of Gaussian process emulation to speed up their computational efficiency.
The main concept is to approximate the geometric quantities using a Gaussian Process emulator 
which is conditioned on a carefully chosen design set of configuration points, 
which also determines the quality of the emulator. 
To this end we propose the use of statistical experiment design methods 
to refine a potentially arbitrarily initialized design online without 
destroying the convergence of the resulting Markov chain to the desired invariant measure.

Included in this package are codes to replicate results of the following paper:

Shiwei Lan, Tan Bui-Thanh, Mike Christie, Mark Girolami
Emulation of Higher-Order Tensors in Manifold Monte Carlo Methods for Bayesian Inverse Problems
Journal of Computational Physics, Volume 308, 1 March 2016, Pages 81-101
http://www.sciencedirect.com/science/article/pii/S0021999115008517

There are some m files in the root folder needed to run codes in subfolders:
Gpe.m, Gpe_logCL.m are class definitions of Gaussian process emulator, the later using parametrization of log-correlation length, tau.
Folders:
plog_figs: codes to plot figures included in the paper.
simulation_BBD: simulation example in section 6.1
EllipticPDE: elliptic example in section 6.2
TealSouth: real example in section 6.3
To obtain posterior samples using specific algorithm, just run ‘sample_ALGORITHM.m’. ALGORITHM could be RWM, (GPe)HMC, (GPe)RHMC, (GPe)LMC etc.
 sampler: generic sampler codes needed to above codes.

Readers may need to modify path to include all required functions when necessary. Please include proper reference if using the codes, thanks!

Shiwei Lan
9-18-2016