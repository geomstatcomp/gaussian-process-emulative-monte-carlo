%%%% GP emulator, by SHIWEI LAN 2014 %%%%
classdef GPe
    % data for GP emulator
    properties
        De; % design points (n,D)
        Dist_D; % distance array of design points (n,n,D)
        u_D; % function value (potential energy) on design points (n,1)
        du_D; % derivative information on design points (nD,1)
%         n_til; % size of vector of inputs n or n*(1+D)
        rho; % (initial) GP parameter (inverse correlation strength)
        gFI_D; % U_til*U_til'-u_til*u_til'/N, generalized (empirical) Fisher information on design points, (n_til,n_til)
        pM; % (hessian) matrix from prior to ensure positivity of the emulated Fisher 
        nugget=1e-6; % small number added to diagonal of extended correlation matrix to make it positive
        
%         H_til; % (n_til,q) regression matrix for GP mean, q = 1+2*D
        invC_til; % (n_til,n_til) inverse (generalized) correlation matrix in GP
        B_til; % (q,q) matrix for prediction, based on design points
        P_til; % (q,n_til) matrix for prediction, based on design points
        Q_til; % (n_til,n_til) matrix for prediction, based on design points
        beta_hat; % estimate of regressor for mean
        sigma2_hat; % estimate of variance of GP
        Qu_til; % term depended on design points only in prediction
    end
    methods
        % constructor
        function obj=GPe(De,u_D,du_D,rho,gFI_D,pM,nugget,optmz,storeBPQ)
            if nargin<7
                nugget=[];optmz=1;storeBPQ=0; % default to optimize rho
            elseif nargin<8
                optmz=1; storeBPQ=0; % store BPQ if predicting eFI or mspe
            elseif nargin<9
                storeBPQ=0;
                if ~isempty(gFI_D)
                    storeBPQ=1;
                end
            end
            obj.De=De;
            obj.u_D=u_D;
            obj.du_D=du_D(:);
            obj.rho=rho; % initialize rho
            obj.gFI_D=gFI_D;
            obj.pM=pM; % needed to make emulative FI positive definite
            if ~isempty(nugget)
                obj.nugget=nugget; % change the default nugget
            end
            
            [n,D]=size(De);
            if numel(pM)==1
                obj.pM=pM.*eye(D);
            end
            % H_til
            H_til=obj.gpH();
            % distance matrix
            obj.Dist_D=bsxfun(@minus,reshape(De,[n,1,D]),reshape(De,[1,n,D]));
            if optmz
                % optimize rho without derivative information if necessary
                if optmz==10||n*D>=1e4
                    disp('Large problem! Choose to optimize (inverse) correlation length without derivative information!');
                    obj.du_D=[];
                    H_til0=H_til;H_til=H_til(1:n,:);
                end
                % optimize rho
%                 [~,~,~,HessMultFcn]=obj.loglik(rho,H_til);
%                 options=optimoptions('fmincon','Algorithm','trust-region-reflective','GradObj','on','Hessian','user-supplied','HessMult',@HessMultFcn,'display','iter');
                options=optimoptions('fmincon','Algorithm','trust-region-reflective','GradObj','on','Hessian','user-supplied','display','iter');
%                 options=optimoptions('fmincon','Algorithm','trust-region-reflective','GradObj','on','display','notify');
                obj.rho=fmincon(@(rho)obj.gpLik(rho,H_til),rho,[],[],[],[],zeros(D,1),[],[],options);
                % restore derivative information for prediction
                if optmz==10&&isempty(obj.du_D)
                    obj.du_D=du_D(:);
                    H_til=H_til0;
                end
            end
            
            % B_til, P_til, Q_til based on optimized rho
            [B_til,P_til,Q_til]=obj.BPQ(H_til);
            obj.Dist_D=[]; % release the space
            if storeBPQ
                obj.B_til=B_til;
                obj.P_til=P_til;
                obj.Q_til=Q_til;
            end
            
            u_til=[u_D;du_D(:)];
            % beta_hat
            obj.beta_hat=P_til*u_til;
            % Qu_til
            obj.Qu_til=Q_til*u_til;
            % sigma2_hat
            obj.sigma2_hat=(u_til'*obj.Qu_til)/(-diff(size(H_til))-2);
            if obj.sigma2_hat<0
                warning('negative variance estimate!');
            end
        end
        % create regressor in GP
        function [H_til,H,dH,d2H]=gpH(obj,S,opt)
            H_til=[];H=[];dH=[];d2H=[];% resp. opt: 10,0,1,2
            if nargin<2
                S=[];opt=10;
            elseif nargin<3
                opt=10;
            end
            if isempty(S)
                if any(opt==10)
                    S=obj.De;
                else
                    return;
                end
            end
            
            [n,D]=size(S);
            if any(ismember([10,0],opt))
                H=[ones(n,1),S,S.^2];
            end
            if any(opt==1)||(any(opt==10)&&~isempty(obj.du_D))
                dS1=kron(eye(D),ones(n,1));
                dS2=zeros(n*D,D);dS2(logical(dS1))=2*S;
                dH=[zeros(n*D,1),dS1,dS2];
            end
            if any(opt==10)
                H_til=[H;dH];
            end
            if any(opt==2)
                eye3=zeros(D*ones(1,3)); eye3(1:(1+D+D^2):D^3)=1;
                d2S2=kron(reshape(eye3,[],D),2*ones(n,1));
                d2H=[zeros(n*D^2,1+D),d2S2];
            end
            
        end
        % create generalized correlation matrix in GP
        function [C_til,d0C_til,d1C_til,d2C_til,dC_til_rho,d2C_til_rhov]=gpC(obj,LS,RS,opt)
            C_til=[];d0C_til=[];d1C_til=[];d2C_til=[];% resp. opt: 10(40),0,1,2
            dC_til_rho=[];d2C_til_rhov=[]; % resp. opt: 11,12
            if nargin<2
                LS=[];RS=[];opt=10;
            elseif nargin<4
                opt=10;
            end
            if isempty(LS)
                if any(opt>=10)
                    LS=obj.De;
                else
                    return;
                end
            end
            if isempty(RS)
                RS=obj.De;
            end
            % preparation
            [szLS,D]=size(LS); szRS=size(RS,1); rho=obj.rho;
            if any(opt>=10)&&~isempty(obj.Dist_D)
                Dist=obj.Dist_D;
            else
                Dist=bsxfun(@minus,reshape(LS,[],1,D),reshape(RS,1,[],D)); % (m,n,D)
            end
            C=exp(-sum(bsxfun(@times,reshape(rho,[1,1,D]),Dist.^2),3)); % (m,n)
            
            if any(opt==40)
                C_til=C; return;
            end
            
            % initialize derivatives on De
            d01C=[];d11C=[];d21C=[];
            % whether use derivative information on design points
            der=~isempty(obj.du_D);
            
            if any(ismember([10:12,0],opt))
                if der
                    d01C=kron(2.*rho',C).*Dist(:,:);
                end
                d0C_til=[C,d01C];
            end
            
            if any(ismember([10:12,1,2],opt))
                if szLS==1
                    vDist=squeeze(Dist);
                else
                    if issymmetric(C)
                        vDist=-Dist;
                    else
                        vDist=permute(Dist,[2,1,3]);
                    end
                end
                vDist=vDist(:,:)'; % (mD,n)
            end
            
            if any(ismember([10:12,1],opt))
                if any(opt>=10)%||issymmetric(C)
                    d10C=d01C';
                else
                    d10C=kron(-2.*rho,C).*vDist;
                end
                if der
                    d11C=kron(-4.*rho*rho',C).*repmat(vDist,[1,D]).*repmat(Dist(:,:),[D,1]);
                    d11C=d11C+kron(diag(2.*rho),C);
                end
                d1C_til=[d10C,d11C];
            end
            
            if any(opt>=10)
                C_til=[d0C_til;d1C_til];
                sz=szLS*(1+der*D); %sz=size(C_til,1);
                C_til(1:sz+1:end)=C_til(1:sz+1:end)+obj.nugget; % nugget to ensure positivity
            end
            
            if any(opt==2)
                rDist=repmat(Dist,[D,1,1]);
                rDist=permute(rDist,[2,1,3]); rDist=rDist(:,:)'; %(mD^2,n)
                vDist2=rDist.*repmat(vDist,[D,1]); %(mD^2,n)
                d20C=kron(kron(4.*rho,rho),C).*vDist2;
                vdiagrho=diag(rho); vdiagrho=vdiagrho(:);
                d20C=d20C+kron(-2.*vdiagrho,C); % (mD^2,n)
                if der
                    d21C1_1=kron(-4.*vdiagrho*rho',C).*repmat(Dist(:,:),[D^2,1]);
%                     d21C1_2=kron(diag(-4.*rho),kron(rho,C).*vDist);
%                     d21C1_3=kron(kron(-4.*rho,diag(rho)),C).*repmat(rDist,[1,D]);
%                     d21C1=d21C1_1+d21C1_2+d21C1_3;
                    d21C1_1f=reshape(d21C1_1,szLS,D,D,szRS,D);
                    d21C1=d21C1_1f+permute(d21C1_1f,[1,3,5,4,2])+permute(d21C1_1f,[1,5,2,4,3]);
                    d21C1=reshape(d21C1,[],szRS*D);
                    d21C3=kron(kron(8.*rho,rho)*rho',C).*repmat(vDist2,[1,D]).*repmat(Dist(:,:),[D^2,1]);
                    d21C=d21C1+d21C3; % (mD^2,nD)
                end
                d2C_til=[d20C,d21C];
            end
            
            if any(ismember([11,12],opt))
                dC_til_rho=repmat(-Dist.^2,1+der*D,1+der*D).*repmat(C_til,[1,1,D]);
                if der
                    n=szLS;
                    wDist=bsxfun(@times,reshape(rho,[1,1,D]),Dist);
                    rI=kron(eye(D),ones(n,1));
                    rI01=repmat(shiftdim(rI,-1),n,1);rI10=repmat(reshape(rI,[],1,D),1,n); %rI10=permute(rI01,[2,1,3]);
                    CDist=Dist.*repmat(C,1,1,D);
                    CDist01=rI01.*repmat(CDist(:,:),1,1,D);CDist10=rI10.*repmat(-CDist(:,:)',1,1,D); %CDist10=-permute(CDist01,[2,1,3]);
                    dC_til_rho(1:n,n+1:end,:)=dC_til_rho(1:n,n+1:end,:)+2*CDist01;
                    dC_til_rho(n+1:end,1:n,:)=dC_til_rho(n+1:end,1:n,:)-2*CDist10;
                    dC_til_rho(n+1:end,n+1:end,:)=dC_til_rho(n+1:end,n+1:end,:)+2*repmat(rI01,D,1).*repmat(rI10,1,D).*repmat(C,D,D,D)...
                        -4*(repmat(CDist01,D,1).*repmat(-wDist(:,:)',1,D,D)+repmat(CDist10,1,D).*repmat(wDist(:,:),D,1,D));
                end
            end
            
            if any(opt==12)
                d2C_til_rhov=@(v)d2Cv(v);
            end
            
            function d2C_til_rhov=d2Cv(v)
                vdist2=sum(bsxfun(@times,reshape(v,[1,1,D]),Dist.^2),3);
                d2C_til_rhov=repmat(-vdist2,1+der*D,1+der*D,D).*dC_til_rho;
                if der
                    vDist=bsxfun(@times,reshape(v,[1,1,D]),Dist);
                    CDist2=Dist.^2.*repmat(C,1,1,D);
                    vDistCDist201=repmat(vDist(:,:),1,1,D).*repmat(CDist2,1,D);
                    vDistCDist210=repmat(-vDist(:,:)',1,1,D).*repmat(CDist2,D,1); %vDistCDist210=-permute(vDistCDist201,[2,1,3]);
                    d2C_til_rhov(1:n,n+1:end,:)=d2C_til_rhov(1:n,n+1:end,:)-2*vDistCDist201;
                    d2C_til_rhov(n+1:end,1:n,:)=d2C_til_rhov(n+1:end,1:n,:)+2*vDistCDist210;
                    d2C_til_rhov(n+1:end,n+1:end,:)=d2C_til_rhov(n+1:end,n+1:end,:)-repmat(2*kron(diag(v),ones(n)),1,1,D).*repmat(CDist2,D,D)...
                        +4*(repmat(vDistCDist201,D,1).*repmat(-wDist(:,:)',1,D,D)+repmat(vDistCDist210,1,D).*repmat(wDist(:,:),D,1,D))...
                        -4*(repmat(CDist01,D,1).*repmat(-vDist(:,:)',1,D,D)+repmat(CDist10,1,D).*repmat(vDist(:,:),D,1,D));
                end
            end
            
        end
        % create generalized correlation matrix in GP with 1 point E
        function [C_til,d0C_til,d1C_til,d2C_til]=gpC1E(obj,LS,RS,opt)
            C_til=[];d0C_til=[];d1C_til=[];d2C_til=[];% resp. opt: 10(40),0,1,2
            if nargin<2
                LS=[];RS=[];opt=10;
            elseif nargin<4
                opt=10;
            end
            if isempty(LS)
                if any(opt==10)
                    LS=obj.De;
                else
                    return;
                end
            end
            if isempty(RS)
                RS=obj.De;
            end
            % preparation
            [szLS,D]=size(LS); szRS=size(RS,1); rho=obj.rho;
            if szLS>1
                [C_til,d0C_til,d1C_til,d2C_til]=obj.gpC(LS,RS,opt);
                return;
            end
            if any(opt==10)&&~isempty(obj.Dist_D)
                Dist=obj.Dist_D;
            else
                Dist=repmat(LS,[szRS,1])-RS;
            end
            C=exp(-sum(repmat(rho',[szRS,1]).*Dist.^2,2))';
            
            if any(opt==40)
                C_til=C; return;
            end
            
            % initialize derivatives on De
            d01C=[];d11C=[];d21C=[];
            % whether use derivative information on design points
            der=~isempty(obj.du_D);
            
            if any(ismember([10,0],opt))
                if der
                    d01C=kron(2.*rho',C).*Dist(:)';
                end
                d0C_til=[C,d01C];
            end
            
            if any(ismember([10,1,2],opt))
                Dist2=zeros([D,szRS,D]);
                for i=1:szRS
                    Dist2(:,i,:)=Dist(i,:)'*Dist(i,:);
                end
            end
            
            if any(ismember([10,1],opt))
                if any(opt==10)%||issymmetric(C)
                    d10C=d01C';
                else
                    d10C=(-2.*rho*C).*Dist';
                end
                if der
                    d11C=kron(-4.*rho*rho',C).*Dist2(:,:);
                    d11C=d11C+kron(diag(2.*rho),C);
                end
                d1C_til=[d10C,d11C];
            end
            
            if any(opt==10)
                C_til=[d0C_til;d1C_til];
                sz=size(C_til,1);
                C_til(1:sz+1:end)=C_til(1:sz+1:end)+obj.nugget; % nugget to ensure positivity
            end
            
            if any(opt==2)
                sDist2=permute(Dist2,[2,3,1]);
                d20C=kron(4.*kron(rho,rho),C).*sDist2(:,:)';
                vdiagrho=diag(rho); vdiagrho=vdiagrho(:);
                d20C=d20C+kron(-2.*vdiagrho,C); % (mD^2,n)
                if der
                    d21C1_1=kron(-4.*vdiagrho*rho',C).*kron(ones(D^2,1),Dist(:)');
                    d21C1_2=kron(diag(-4.*rho),(rho*C).*Dist');
                    d21C1_3=kron(kron(-4.*rho,diag(rho)),C).*repmat(Dist(:,kron(1:D,ones(1,D)))',[1,D]);
                    d21C1=d21C1_1+d21C1_2+d21C1_3;
%                     d21C1_1f=reshape(d21C1_1,szLS,D,D,szRS,D);
%                     d21C1=d21C1_1f+permute(d21C1_1f,[1,3,5,4,2])+permute(d21C1_1f,[1,5,2,4,3]);
%                     d21C1=reshape(d21C1,[],szRS*D); % may be faster
                    Dist3=zeros([D^2,szRS,D]);
                    for i=1:szRS
                        Dist3(:,i,:)=sDist2(i,:)'*Dist(i,:);
                    end
                    d21C3=kron(kron(8.*rho,rho)*rho',C).*Dist3(:,:);
                    d21C=d21C1+d21C3; % (mD^2,nD)
                end
                d2C_til=[d20C,d21C];
            end
            
        end
        % generate P, Q matrices
        function [B_til,P_til,Q_til]=BPQ(obj,H_til,C_til,invC_til)
            if nargin<3
                invC_til=obj.invC_til;
                if isempty(invC_til)
                    C_til=obj.gpC();
                end
            elseif nargin<4
                invC_til=obj.invC_til;
            end
            
            if ~isempty(invC_til)
                HinvC_til=H_til'*invC_til;
            else
                HinvC_til=H_til'/C_til;
            end
            % B_til
            B_til=HinvC_til*H_til;
            % P_til
            if diff(size(H_til))<=0
                P_til=B_til\HinvC_til;
            else
%                 warning('Dengenerate B_til! Use pseudo-inverse instead!');
                P_til=pinv(B_til)*HinvC_til;
            end
            % Q_til
            if ~isempty(invC_til)
                Q_til=invC_til-HinvC_til'*P_til;
            else
                Q_til=C_til\(eye(size(C_til))-H_til*P_til);
            end
        end
        % log-likelihood of GP parameter (rho: inverse correlation strength)
        function [loglik,dloglik,d2loglik,d2loglikv]=gpLik(obj,rho,H_til)
            loglik=[];dloglik=[];d2loglik=[];d2loglikv=[];
            if nargin<3
                H_til=obj.gpH();
            end
            
            % update rho of the object
            obj.rho=rho;
            % C_til based on new rho
            [C_til,~,~,~,dC_til_rho,d2C_til_rhov]=obj.gpC([],[],10+(0:nargout-1));
            % B_til, P_til, Q_til
            [B_til,~,Q_til]=obj.BPQ(H_til,C_til);
            
            [n_til,q]=size(H_til); D=(q-1)/2;
            a=n_til-q; b=a-2;
            if b<=0
                error('Num of design pts should be at least 4 greater than (twice) dimension!');
            end
            u_til=[obj.u_D;obj.du_D(:)];
            % sigma2_hat
            Qu_til=Q_til*u_til;
            sigma2_hat=(u_til'*Qu_til)/b; % omit the constant 1/(a-2)
            
            % loglik_rho
            loglik=-a/2*log(sigma2_hat)-sum(log(diag(chol(C_til))))-sum(log(diag(chol(B_til))));
            loglik=-loglik;
            
            % dloglik_rho
            if nargout>1
                uQdC=reshape(Qu_til'*dC_til_rho(:,:),[],D);
                dsigma2_hat=-uQdC'*Qu_til/b;
                dloglik=-a/(2*sigma2_hat).*dsigma2_hat-.5*sum(reshape(sum(repmat(Q_til,[1,D]).*dC_til_rho(:,:)),[],D))';
                dloglik=-dloglik;
            end
            
            % d2loglik_rho
            if nargout>2
                d2loglikv=@(v)d2l(v);
                d2loglik=zeros(D,D);e=zeros(D,1);
                for j=1:D
                    e(j)=1;
                    d2loglik(:,j)=d2loglikv(e);
                    e(j)=0;
                end
            end
            
            function d2loglikv=d2l(v)
                d2loglikv=a/(2*sigma2_hat^2)*dsigma2_hat*(dsigma2_hat'*v);
                QdCv=Q_til*sum(bsxfun(@mtimes,reshape(v,[1,1,D]),dC_til_rho),3);
                d2Cv=d2C_til_rhov(v);
                
                dCvQdCmd2Cv=QdCv'*dC_til_rho(:,:)-d2Cv(:,:);
                d2loglikv=d2loglikv-a/(2*b*sigma2_hat)*(uQdC'*QdCv+reshape(Qu_til'*dCvQdCmd2Cv,[],D)')*Qu_til;
                d2loglikv=d2loglikv+.5*sum(reshape(sum(repmat(Q_til,[1,D]).*dCvQdCmd2Cv),[],D))';
                d2loglikv=-d2loglikv;
            end
        end
        % prediction on multiple new points (parallel)
        function [u_E,du_E,d2u_E,eFI_E,eChS1_E,mspe]=pred(obj,E,opt)
            u_E=[];du_E=[];d2u_E=[];eFI_E=[];eChS1_E=[];mspe=[]; % resp. opt: 0,1,2,11,21,40
            
            % preparation
            [m,D]=size(E);
            if m==0
                return;
            end
            beta_hat=obj.beta_hat;Qu_til=obj.Qu_til;
            % get options for dertivatives
            opt4der=[]; str4der=num2str(opt);
            for i=0:2
                if ~isempty(strfind(str4der,num2str(i)))
                    opt4der=[opt4der,i];
                end
            end
            % get correlation matrices
            [~,C_Etil,dC_Etil,d2C_Etil]=obj.gpC(E,[],opt4der);
            % get regressor matrices
            [~,H_E,dH_E,d2H_E]=obj.gpH(E,opt4der);
            
            % predictions
            if any(opt==0)
                u_E=H_E*beta_hat+C_Etil*Qu_til;
            end
            if any(opt==1)
                du_E=dH_E*beta_hat+dC_Etil*Qu_til;
            end
            if any(opt==2)
                d2u_E=d2H_E*beta_hat+d2C_Etil*Qu_til;
            end
            
            if any(ismember([11,21],opt))
                dL=dH_E*obj.P_til+dC_Etil*obj.Q_til; % (mD,n_til)
                dLgFI=dL*obj.gFI_D;
                if m>1
                    dLgFI=shiftdim(reshape(dLgFI,m,D,[]),1);
                end
            end
            if any(opt==11)
                if m==1
                    eFI_E=dLgFI*dL'+obj.pM;
                else
                    dL=shiftdim(reshape(dL,m,D,[]),1);
                    eFI_E=zeros(m*D,D);
                    for i=1:m
                        eFI_E(i+m.*(0:D-1),:)=dLgFI(:,:,i)*dL(:,:,i)'+obj.pM;
                    end
                end
            end
            if any(opt==21)
                d2L=d2H_E*obj.P_til+d2C_Etil*obj.Q_til;
                if m==1
                    eChS1_E=d2L*dLgFI';
                else
                    d2L=shiftdim(reshape(d2L,m,D^2,[]),1);
                    eChS1_E1=zeros(m*D^2,D);
                    for i=1:m
                        eChS1_E1(i+m.*(0:D^2-1),:)=d2L(:,:,i)*dLgFI(:,:,i)';
                    end
                end
            end
            if any(opt==40)
                HC_E=[H_E,C_Etil];
                if ~isempty(obj.B_til)
                    invM_til=[-inv(obj.B_til),obj.P_til;obj.P_til',obj.Q_til];
                    sigma2_hat=obj.sigma2_hat;
                    HCinvM_til=HC_E*invM_til;
                else
                    u_til=[obj.u_D;obj.du_D(:)];
                    if ~isempty(obj.invC_til)
                        [B_til,P_til,Q_til]=obj.BPQ(obj.gpH(),[],obj.invC_til);
                        invM_til=[-inv(B_til),P_til;P_til',Q_til];
                        sigma2_hat=(u_til'*Q_til*u_til)/(diff(size(P_til))-2);
                        HCinvM_til=HC_E*invM_til;
                    else
                        H_til=obj.gpH(); q=size(H_til,2);
                        M_til=[zeros(q),H_til';H_til,obj.gpC()];
                        HC_E=[HC_E;zeros(1,q),u_til']; % pad the last row to compute sigma2_hat
                        HCinvM_til=HC_E/M_til;
                    end
                end
                diagquad=sum(HC_E.*HCinvM_til,2);
                mspe=ones(m,1)-diagquad(1:m);
                if length(diagquad)==m+1
                    sigma2_hat=diagquad(end)/(-diff(size(H_til))-2);
                end
                mspe=sigma2_hat.*mspe;
                if any(mspe<0)
                    warning('negative MSPE!');
                end
            end
            
        end
        % prediction using subset of design points
        function [u_E,du_E,d2u_E,eFI_E,eChS1_E,mspe]=predlocal(obj,E,opt,K)
            u_E=[];du_E=[];d2u_E=[];eFI_E=[];eChS1_E=[];mspe=[]; % resp. opt: 0,1,2,11,21,40
            [n,D]=size(obj.De); m=size(E,1);
            if n<1+2*D
                warning('there will be degenerate matrices!');
            end
            if m==0
                return;
            end
            if nargin<4
                K=1+2*D;
            end
%             K=max([K,1+2*D]); % to avoid degeneracy
            
            % nearest K neighbours
%             [DistK,I_nearestK]=pdist2(obj.De,E,'euclidean','smallest',K);
            [DistK,I_nearestK]=pdist2(obj.De,E,@(XI,XJ)(bsxfun(@minus,XI,XJ).^2 * obj.rho),'smallest',K);
            if m>1
                [~,I_maxminK]=min(DistK,[],2);I_nearestK=unique(diag(I_nearestK(:,I_maxminK)));
            end
            % record the original emulator
            obj0=obj;
            % emulator using nearest K neighbours
            if length(I_nearestK)<n
                obj.De=obj.De(I_nearestK,:);obj.u_D=obj.u_D(I_nearestK);
                der=~isempty(obj.du_D);
                if der
                    Kd_idx=bsxfun(@plus,I_nearestK,(0:D-1).*n);
                    obj.du_D=obj.du_D(Kd_idx(:));
                end
                if ~isempty(obj.gFI_D)
                    if ~der
                        obj.gFI_D=obj.gFI_D(I_nearestK,I_nearestK);
                    else
                        Kd_idx=bsxfun(@plus,I_nearestK,(0:D).*n);
                        obj.gFI_D=obj.gFI_D(Kd_idx,Kd_idx);
                    end
                end
            end
            obj=obj.update(0,any(opt>2));
            % preparation for prediction
            beta_hat=obj.beta_hat;Qu_til=obj.Qu_til;
            % get options for dertivatives
            opt4der=[]; str4der=num2str(opt);
            for i=0:2
                if ~isempty(strfind(str4der,num2str(i)))
                    opt4der=[opt4der,i];
                end
            end
            % get correlation matrices
            [~,C_Etil,dC_Etil,d2C_Etil]=obj.gpC(E,[],opt4der);
            % get regressor matrices
            [~,H_E,dH_E,d2H_E]=obj.gpH(E,opt4der);
            
            % predictions
            if any(opt==0)
                u_E=H_E*beta_hat+C_Etil*Qu_til;
            end
            if any(opt==1)
                du_E=dH_E*beta_hat+dC_Etil*Qu_til;
            end
            if any(opt==2)
                d2u_E=d2H_E*beta_hat+d2C_Etil*Qu_til;
            end
            
            if any(ismember([11,21],opt))
                dL=dH_E*obj.P_til+dC_Etil*obj.Q_til; % (mD,n_til)
                dLgFI=dL*obj.gFI_D;
                if m>1
                    dLgFI=shiftdim(reshape(dLgFI,m,D,[]),1);
                end
            end
            if any(opt==11)
                if m==1
                    eFI_E=dLgFI*dL'+obj.pM;
                else
                    dL=shiftdim(reshape(dL,m,D,[]),1);
                    eFI_E=zeros(m*D,D);
                    for i=1:m
                        eFI_E(i+m.*(0:D-1),:)=dLgFI(:,:,i)*dL(:,:,i)'+obj.pM;
                    end
                end
            end
            if any(opt==21)
                d2L=d2H_E*obj.P_til+d2C_Etil*obj.Q_til;
                if m==1
                    eChS1_E=d2L*dLgFI';
                else
                    d2L=shiftdim(reshape(d2L,m,D^2,[]),1);
                    eChS1_E1=zeros(m*D^2,D);
                    for i=1:m
                        eChS1_E1(i+m.*(0:D^2-1),:)=d2L(:,:,i)*dLgFI(:,:,i)';
                    end
                end
            end
            if any(opt==40)
                HC_E=[H_E,C_Etil];
                if ~isempty(obj.B_til)
                    invM_til=[-inv(obj.B_til),obj.P_til;obj.P_til',obj.Q_til];
                    sigma2_hat=obj.sigma2_hat;
                    HCinvM_til=HC_E*invM_til;
                else
                    u_til=[obj.u_D;obj.du_D(:)];
                    if ~isempty(obj.invC_til)
                        [B_til,P_til,Q_til]=obj.BPQ(obj.gpH(),[],obj.invC_til);
                        invM_til=[-inv(B_til),P_til;P_til',Q_til];
                        sigma2_hat=(u_til'*Q_til*u_til)/(diff(size(P_til))-2);
                        HCinvM_til=HC_E*invM_til;
                    else
                        H_til=obj.gpH(); q=size(H_til,2);
                        M_til=[zeros(q),H_til';H_til,obj.gpC()];
                        HC_E=[HC_E;zeros(1,q),u_til']; % pad the last row to compute sigma2_hat
                        HCinvM_til=HC_E/M_til;
                    end
                end
                diagquad=sum(HC_E.*HCinvM_til,2);
                mspe=ones(m,1)-diagquad(1:m);
                if length(diagquad)==m+1
                    sigma2_hat=diagquad(end)/(-diff(size(H_til))-2);
                end
                mspe=sigma2_hat.*mspe;
                if any(mspe<0)
                    warning('negative MSPE!');
                end
            end
            
            % restore original emulator
            obj=obj0;
        end
        % update the object
        function obj=update(obj,optmz,storeBPQ)
            if nargin<2
                optmz=0;storeBPQ=0;
            elseif nargin<3
                storeBPQ=0;
            end
            
            [n,D]=size(obj.De);
            % H_til
            H_til=obj.gpH();
            % distance matrix
            if isempty(obj.Dist_D)
                obj.Dist_D=bsxfun(@minus,reshape(obj.De,[n,1,D]),reshape(obj.De,[1,n,D]));
            end
            if optmz
                % optimize rho without derivative information if necessary
                if optmz==10||n*D>=1e4
                    disp('Large problem! Choose to optimize (inverse) correlation length without derivative information!');
                    du_D=obj.du_D;obj.du_D=[];
                    H_til0=H_til;H_til=H_til(1:n,:);
                end
                % optimize rho
                options=optimoptions('fmincon','Algorithm','trust-region-reflective','GradObj','on','Hessian','user-supplied','display','notify');
%                 options=optimoptions('fmincon','Algorithm','trust-region-reflective','gradobj','on','display','notify');
                obj.rho=fmincon(@(rho)obj.gpLik(rho,H_til),obj.rho,[],[],[],[],zeros(D,1),[],[],options);
                % restore derivative information for prediction
                if optmz==10&&isempty(obj.du_D)
                    obj.du_D=du_D(:);
                    H_til=H_til0;
                end
            end

            % B_til, P_til, Q_til based on optimized rho
            [B_til,P_til,Q_til]=obj.BPQ(H_til);
            obj.Dist_D=[]; % release the space
            if storeBPQ
                obj.B_til=B_til;
                obj.P_til=P_til;
                obj.Q_til=Q_til;
            end

            u_til=[obj.u_D;obj.du_D(:)];
            % beta_hat
            obj.beta_hat=P_til*u_til;
            % Qu_til
            obj.Qu_til=Q_til*u_til;
            % sigma2_hat
            obj.sigma2_hat=(u_til'*obj.Qu_til)/(-diff(size(H_til))-2);
        end
        % refine emulator using modified MICE
        function [obj,Ent,MSPE]=refine(obj,cand,u_cand,geom,Ki,Kc,light,nuc,flexsz,mspe_thld,Nulim,PRINT)
            if nargin<5
                Ki=1+2*length(obj.tau)+2; % NO. of pts in initial design
                Kc=1; % NO. of pts in complement set but not in candidate set
                light=0; % whether to refine without derivative information
                nuc=1; % nugget for complement MSPE (conditional variance)
                flexsz=1; % allow flexible design size
                mspe_thld=1e-2; % threshold to stop growing design size
                Nulim=1e4; % upper limit of design size
                PRINT=1; % show entropy
            elseif nargin<6
                Kc=1;light=0;nuc=1;flexsz=1;mspe_thld=1e-2;Nulim=1e4;
                PRINT=1;
            elseif nargin<7
                light=0;nuc=1;flexsz=1;mspe_thld=1e-2;Nulim=1e4;
                PRINT=1;
            elseif nargin<8
                nuc=1;flexsz=1;mspe_thld=1e-2;Nulim=1e4;
                PRINT=1;
            elseif nargin<9
                flexsz=1;mspe_thld=1e-2;Nulim=1e4;
                PRINT=1;
            elseif nargin<10
                mspe_thld=1e-2;Nulim=1e4;
                PRINT=1;
            elseif nargin<11
                Nulim=1e4;PRINT=1;
            elseif nargin<12
                PRINT=1;
            end
            %%% record the initial emulator %%%
            De=obj.De; [n,D]=size(De);
            u_D=obj.u_D;
            du_D=reshape(obj.du_D,[],D);
            der=~isempty(obj.du_D)&&~light;
            
            %%% preprocess candidate set %%%
            % combine the exisiting design points to form the new candidate set
            cand=[De;cand]; u_cand=[u_D;u_cand];
            % narrow down the candidate set to a spreading subset
%             [cand,I]=X2closest(cand,n+flexsz*min([Ki,n]));
            [cand,I]=maximin(cand,n+flexsz*min([Ki,n]),obj.rho');
            Ncand=length(I);
            u_cand=u_cand(I); du_cand=[];
            if der
                du_tmp=zeros(Ncand,D);
                [I_inDe,I_ofDe]=ismember(cand,De,'rows');
                du_tmp(I_inDe,:)=du_D(I_ofDe(I_inDe),:);
                [~,du]=geom(cand(~I_inDe,:),der);
                du_tmp(~I_inDe,:)=reshape(du,[],D);
                du_cand=du_tmp; du_tmp=[];
            end
            
            %%% points in complement set but not in candidate set %%%
            % range of the space that has been explored
            Range=[min(cand,[],1);max(cand,[],1)];
            % plays a role as environment aware set, not select from them
            Dc0=repmat(Range(1,:),[Kc,1])+lhsdesign(Kc,D).*repmat(diff(Range),[Kc,1]); % using Latin Hypercube
            [u_Dc0,du_Dc0]=obj.pred(Dc0,[0,der]); du_Dc0=reshape(du_Dc0,[],D); % emulation to save computation
            
            rnd=0;
            %%% build the initial design and emulator %%%
            if rnd
                % randomly choose Ki initial points according to probability
                inidx=datasample(1:Ncand,Ki,'replace',false,'weights',exp(-u_cand));
                [~,~,~,~,~,mspe_ini] = obj.pred(cand(inidx,:),40);
            else
                % choose Ki initial points based on largest entropies
                [~,~,~,~,~,mspe_cand] = obj.pred(cand,40);
                [~,ord]=sort(mspe_cand,'descend');
                inidx=ord(1:Ki); mspe_ini=mspe_cand(inidx);
            end
            % accumulate the utility (entropy)
            Ent=sum(log(mspe_ini));
            MSPE=mspe_ini;
            
            % remove du_D if refine without it
            if light
                obj.du_D=[];
            end
            % update the design pool
            obj.De=cand(inidx,:); obj.u_D=u_cand(inidx);
            if der
                du=du_cand(inidx,:); obj.du_D=du(:);
            end
            % update the object
            obj=obj.update(1+9*light,1);
            
            
            %%% build the complement set %%%
            rest=setdiff(1:Ncand,inidx);
            % update the candidate set by removing the selected points
            cand=cand(rest,:); u_cand=u_cand(rest);
            if der
                du_cand=du_cand(rest,:);
            end
%             % create the complement set using Latin Hypercube
%             % plays a role as environment aware set, not select from them
%             Dc0=repmat(Range(1,:),[Kc,1])+lhsdesign(Kc,D).*repmat(diff(Range),[Kc,1]);
%             [u_Dc0,du_Dc0]=geom(Dc0,[0,der]); du_Dc0=reshape(du_Dc0,[],D);
            % form the total complement set by incorportating the candidate set
            Dc=[Dc0;cand]; u_Dc=[u_Dc0;u_cand]; du_Dc=[du_Dc0;du_cand];
            Ncand=Ncand-Ki;
            % initial invC_all
            invC_all=inv(obj.gpC(Dc,Dc)+(nuc-obj.nugget)*eye((Ncand+Kc)*(1+der*D))); % nugget=nuc

            %%%  select the rest of new design points sequentially %%%
            while size(obj.De,1)<Nulim&&Ncand>0
                % calculate Var(cand|De) simultaneously
                [~,~,~,~,~,mspe] = obj.pred(cand,40);
                % initialization
                obj_noj=obj;
                % choose the point with maximum mutual information gain
                for j=1:Ncand
                    noj=setdiff(1:Kc+Ncand,Kc+j); % indices without current point
                    obj_noj.De=Dc(noj,:); obj_noj.u_D=u_Dc(noj);
                    % get invC without j from invC_all
                    if der
                        du_noj=du_Dc(noj,:);
                        dj=Kc+j+(0:D).*(Kc+Ncand);nodj=setdiff(1:(Kc+Ncand)*(1+D),dj);
                        invC_noj=invC_all(nodj,nodj)-invC_all(nodj,dj)/invC_all(dj,dj)*invC_all(dj,nodj);
                    else
                        du_noj=[];
                        invC_noj=invC_all(noj,noj)-invC_all(noj,Kc+j)/invC_all(Kc+j,Kc+j)*invC_all(Kc+j,noj);
                    end
                    obj_noj.du_D=du_noj(:);
%                     if cond(invC_noj)<1e4
                        obj_noj.invC_til=invC_noj; % faster, but with increased numeric error--mitigate with larger nuc
%                     else
%                         obj_noj.invC_til=[];
%                     end
                    obj_noj.B_til=[];
%                     obj_noj.nugget=1;
%                     obj_noj=obj_noj.update(0,1); % slower, more accurate
                    [~,~,~,~,~,mspe_j] = obj_noj.pred(cand(j,:),40);
                    eMI_j=mspe(j)/mspe_j;
                    if j==1||eMI_j>eMI
                        max_id=j;eMI=eMI_j; % exponential of mutual information
                        invC_maxj=invC_noj; % record invM_noj corresponding to max_id
                    end
                end
                % update the design pool by adding this new point
                obj.De=[obj.De;cand(max_id,:)]; obj.u_D=[obj.u_D;u_cand(max_id)];
                if der
                    du=[reshape(obj.du_D,[],D);du_cand(max_id,:)]; obj.du_D=du(:);
                end
                % update invC_all
                invC_all=invC_maxj;
                % accumulate the utility (entropy)
                mspe_max=mspe(max_id);
                Ent=Ent+log(mspe_max);
                MSPE=[MSPE;mspe_max];
                if flexsz&&mspe_max<mspe_thld
                    break;
                end
                % update the emulator for the next run (optional optimization?)
%                 obj=obj.update(1+9*light,1);
                obj=obj.update(1-light,1);
                % update the candidate set by removing the selected point
                mv_max=setdiff(1:Ncand,max_id);
                cand=cand(mv_max,:); Ncand=Ncand-1;
                u_cand=u_cand(mv_max);
                if der
                    du_cand=du_cand(mv_max,:);
                end
                % update the complement set
                Dc=[Dc0;cand]; u_Dc=[u_Dc0;u_cand]; du_Dc=[du_Dc0;du_cand];
            end
            % update the emulated FI
            if light&&~isempty(du_D)
                [~,obj.du_D,obj.gFI_D]=geom(obj.De,[1,11]);
            else
                [~,~,obj.gFI_D]=geom(obj.De,10+der); % 10 is faster
            end
            % update the emulator for the last time
            obj=obj.update(1+9*light,1);
            
            % print result
            if PRINT
                disp([num2str(size(obj.De,1)),' design points attained with entropy: ',num2str(Ent)]);
            end
end
        % plot
        function plot(obj,E,dim,scale,linespecs,opt)
            if nargin<6
                opt=[0,1,11];
            end
            hold on;
            if any(opt==0)
                % plot evaluation points
                plot(E(:,dim(1)), E(:,dim(2)),'ks','markersize',10,'linewidth',2);
            end
            % make predictions
            [~,du_E,d2u_E,eFI_E]=obj.pred(E,opt);
            [m,D]=size(E);
            if any(opt==1)
                du_E=reshape(du_E,[m,D]);
                % plot emulative gradients
                quiver(E(:,dim(1)),E(:,dim(2)),du_E(:,dim(1)),du_E(:,dim(2)),scale(1),linespecs,'linewidth',2);
            end
            if any(opt==2)
                d2u_E=reshape(d2u_E,[m,D,D]);
                % plot emulative Hessians
                arrayfun(@(i)plot2dGaussian(E(i,dim),scale(2)^2.*d2u_E(i,dim,dim),linespecs),1:m);
            end
            if any(opt==11)
                eFI_E=reshape(eFI_E,[m,D,D]);
                % plot emulative empirical Fisher
                arrayfun(@(i)plot2dGaussian(E(i,dim),scale(2)^2.*eFI_E(i,dim,dim),linespecs),1:m);
            end
        end
    end
end


%%%% Generic function to select a spreading subset from candidate set %%%%
function [x,I]=X2closest(X,n,wts)
if nargin<3
    wts=[];
end
% remove the duplicates
[X,uI]=unique(X,'rows','stable');
N=size(X,1);

if n>=N
    x=X; I=uI;
else
    thin_idx=floor(linspace(1,N,min([N,3*n]))); N=length(thin_idx);
    X=X(thin_idx,:);% thinning if too many points
    % pairwise distance
    if isempty(wts)
        pDist=pdist2(X,X);
    else
        pDist=pdist2(X,X,@(XI,XJ)(sqrt(bsxfun(@minus,XI,XJ).^2 * wts')));
    end
    pDist(1:N+1:end)=+Inf;
    want_list=1:N;
    for i=1:N-n
        [~,cI]=min(pDist(:));
        [I,J]=ind2sub(N-i+1,cI);
        if sum(pDist(I,1:N-i+1~=I))<sum(pDist(J,1:N-i+1~=J)) % delete a pt
            pDist(I,:)=[];pDist(:,I)=[]; 
            want_list(I)=[];
        else
            pDist(J,:)=[];pDist(:,J)=[];
            want_list(J)=[];
        end
    end
    x=X(want_list,:); I=uI(thin_idx(want_list));
end    

end

function [x,I]=maximin(X,n,wts)
if nargin<3
    wts=[];
end
% remove the duplicates
[X,uI]=unique(X,'rows','stable');
N=size(X,1);

if n>=N
    x=X; I=uI;
else
    thin_idx=floor(linspace(1,N,min([N,5*n]))); N=length(thin_idx);
    X=X(thin_idx,:);% thinning if too many points
    % pairwise distance
    if isempty(wts)
        pDist=pdist2(X,X);
    else
        pDist=pdist2(X,X,@(XI,XJ)(sqrt(bsxfun(@minus,XI,XJ).^2 * wts')));
    end
    want_list=zeros(n,1);
    % choose the first one
    [~,cI]=max(pDist(:));
    [I,J]=ind2sub(N,cI);
    if sum(pDist(I,1:N~=I))<sum(pDist(J,1:N~=J))
        want_list(1)=I;
    else
        want_list(1)=J;
    end
    % choose the rest
    for i=2:n
        idx_rest=setdiff(1:N,want_list(1:i-1));
        invout=min(pDist(want_list(1:i-1),idx_rest),[],1);
        [~,max_id]=max(invout);
        want_list(i)=idx_rest(max_id);
    end
    x=X(want_list,:); I=uI(thin_idx(want_list));
end    

end

%%%% Generic function to plot 2D Gaussian %%%%
function plot2dGaussian(mu,Sigma,linespec,linewidth)
if nargin<3
    linespec='k-'; linewidth=2;
elseif nargin<4
    linewidth=2;
end
% for convenience of plot
mu=squeeze(mu); Sigma=squeeze(Sigma);

if(any([length(mu)~=2,size(Sigma)~=2]))
    error('Only plot 2d Gaussian!');
end

t = linspace(-pi,pi,100);
u = [cos(t); sin(t)];

[V,D] = eig(Sigma);
z = real(V*sqrt(D))*u;

hold on;
plot(mu(1)+z(1,:),mu(2)+z(2,:),linespec,'linewidth',linewidth);

end