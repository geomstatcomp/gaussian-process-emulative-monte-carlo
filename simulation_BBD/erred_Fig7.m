% this is to compare the different algorithms in terms of error reducing speed.

addpath('./result/');
addpath('./erred_0/');
% addpath('~/Statistics/Projects/Multi-Mode/code/SungjinAhn_Code_RegDartMCMC/00_utils');

% algorithms
% alg={'RWM','HMC','RHMC','LMC','GPeHMC','GPeRHMC','GPeLMC'};
alg={'RWM','HMC','GPeHMC','RHMC','GPeRHMC','LMC','GPeLMC'};
Nalg=length(alg);

% plot specifications
colors = distinguishable_colors(Nalg);
styles = {'-','-.','--','-','--',':','--'};

% result of long HMC run
load('./erred_0/Banana_longHMC_D4_Nleap40__2015_ 1_ 6_ 0_55_ 6.mat','samp');
true_mean=mean(samp);
true_cov=cov(samp);
% store measuring statistics
Npt=1000/5;
est_mean=zeros([size(true_mean),Npt,Nalg]);
est_cov=zeros([size(true_cov),Npt,Nalg]);
REM=zeros(Npt,Nalg);
REC=zeros(Npt,Nalg);
timeser=zeros(Npt,Nalg);

% compute estimates
files = dir('./erred_0');
nfiles = length(files) - 3;
for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j+3).name,['_',alg{i},'_']))
            load(strcat('./erred_0/', files(j+3).name));
            for k=1:Npt
                est_mean(:,:,k,i)=mean(samp(1:iters(k),:));
                REM(k,i)=norm(est_mean(:,:,k,i)-true_mean)/norm(true_mean);
                est_cov(:,:,k,i)=cov(samp(1:iters(k),:));
                REC(k,i)=norm(est_cov(:,:,k,i)-true_cov)/norm(true_cov);
            end
            break;
        end
    end
    timeser(:,i)=times(1:Npt);
end

% plot REM
fig1=figure(1); set(fig1,'pos',[0 800 800 500]); clf;

subplot(2,1,1,'position',[.1,.57,.7,.36]);
for i=1:Nalg
    semilogy(timeser(:,i),REM(:,i), styles{i},'color',colors(i,:),'linewidth',2); hold on;
end
drawnow;
set(gca,'FontSize',15);
xlim([0,1000]);ylim([2e-2,3]);
xlabel('Seconds','FontSize',19); ylabel('Relative Error of Mean','FontSize',19); 
title('Error Reducing','FontSize',20);

% plot REC
subplot(2,1,2,'position',[.1,.1,.7,.36]);
for i=1:Nalg
    semilogy(timeser(:,i),REC(:,i), styles{i},'color',colors(i,:),'linewidth',2); hold on;
end
drawnow;
set(gca,'FontSize',15);
xlim([0,1000]);ylim([2e-2,2]);
xlabel('Seconds','FontSize',19); ylabel('Relative Error of Covariance','FontSize',19); 

lgnd=legend(alg,'FontSize',14,'location','EastOutside');
set(lgnd,'position',[.84,.25,.12,.5]);