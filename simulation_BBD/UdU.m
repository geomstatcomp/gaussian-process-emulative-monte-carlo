%% energy function and its gradient %%
function [u,du] = UdU(y,theta,sigma2y,sigma2theta)
u=[];du=[];

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end
mu=sum([theta(:,1:2:D),theta(:,2:2:D).^2],2);

logpri = -sum(theta.^2,2)./sigma2theta/2;
loglik = -sum((bsxfun(@minus,mu,y')).^2,2)./sigma2y/2;
u = -(loglik+logpri);
    
if nargout>1
    dlogpri = -theta./sigma2theta;
    dmu = ones(n,D); dmu(:,2:2:D) = 2.*theta(:,2:2:D);
    dloglik = -repmat(sum(bsxfun(@minus,mu,y'),2),1,D).*dmu./sigma2y;
    du  = -(dloglik + dlogpri);
    if size(du,1)==1
        du=du';
    end
end

end