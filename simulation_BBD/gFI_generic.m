% This is to compute the generalized empirical Fisher information %

function[gFI_til]=gFI_generic(xU,dxU)

N=size(xU,2);
xU_til=[xU;dxU];
u_til=sum(xU_til,2);
gFI_til=xU_til*xU_til'-u_til*u_til'./N;

end