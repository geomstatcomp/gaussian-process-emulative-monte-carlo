%%%% Geometric quantities includeing u,du,G,dG,ChS1,etc. using empirical Fisher %%%%
function [u,du,G,invG,dG,ChS1,dlogrtG] = GEOMeFI(y,theta,sigma2y,sigma2theta,opt)
if nargin<5
    opt=0;
end
% resp. opt: 0,1,2,-2,3,33,-33
u=[];du=[];G=[];invG=[];dG=[];ChS1=[];dlogrtG=[];

N=length(y);D=length(theta);
mu=sum([theta(1:2:D);theta(2:2:D).^2]);

if any(opt==0)
    ll=-sum((y-mu).^2)/sigma2y/2; % scalar
end
if any(abs(opt)>=1)
    dmu=ones(1,D); dmu(2:2:D)=2.*theta(2:2:D);
    misfit=(y-mu)./sigma2y;
    Dll=misfit*dmu; % (N,D) matrix
    dll=mean(Dll);
end
if any(abs(opt)>2)
    d2mu=zeros(1,D); d2mu(2:2:D)=2; d2mu=diag(d2mu);
    dmu2=dmu'*dmu;
    D2ll=misfit*d2mu(:)'-repmat(dmu2(:)'./sigma2y,N,1); % (N,D^2) matrix
end



if any(opt==0)
    logpri = -sum(theta.^2)./sigma2theta/2;
    loglik = ll;
    u = -(loglik+logpri);
end
if any(opt==1)
    dlogpri = -theta./sigma2theta;
    dloglik = N*dll';
    du  = -(dloglik + dlogpri);
end


if any(ismember([2,-2],opt))
    Dll2=Dll'*Dll;
end
if any(ismember([3,33,-33],opt))
    d2ll=reshape(mean(D2ll),[],D);
    D2llDll = reshape(D2ll'*Dll,D,D,D);
    d2lldll = reshape(d2ll(:)*dll,D,D,D);
end

if any(opt==2)
    eF=Dll2-N*(dll'*dll);
    G=eF;
    G(1:D+1:D^2) = G(1:D+1:D^2) + 1/sigma2theta;
end
if any(opt==-2)
    invA = inv(Dll2 + 1/sigma2theta*eye(D)); invAdll=invA*dll';
    invG = invA + invAdll*invAdll'*N/(1-dll*invAdll*N);
%     invG = inv(G);
end
if any(ismember([3,33],opt))
    ChS1 = D2llDll-N*d2lldll;
end
if any(opt==3)
    dG = permute(ChS1,[2,3,1]) + permute(ChS1,[1,3,2]);
end
if any(opt==-33)
    if any(opt==33)
        dlogrtG = sum(repmat(invG(:)',[D,1]).*ChS1(:,:),2);
    elseif any(opt==3)
        dlogrtG = sum(repmat(invG(:),[1,D]).*reshape(dG,D^2,D))'/2;
    end
end



end