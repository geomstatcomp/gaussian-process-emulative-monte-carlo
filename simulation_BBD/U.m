%% energy function %%
function [output] = U(y,theta,sigma2y,sigma2theta,der)
if(nargin<5)
    der=0;
end

[n,D]=size(theta);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end
mu=sum([theta(:,1:2:D),theta(:,2:2:D).^2],2);

if der==0
    logpri = -sum(theta.^2,2)./sigma2theta/2;
    loglik = -sum((bsxfun(@minus,mu,y')).^2,2)./sigma2y/2;
    output = -(loglik+logpri);
elseif der==1
    dlogpri = -theta./sigma2theta;
    dmu = ones(n,D); dmu(:,2:2:D) = 2.*theta(:,2:2:D);
    dloglik = -repmat(sum(bsxfun(@minus,mu,y'),2),1,D).*dmu./sigma2y;
    output  = -(dloglik + dlogpri);
    if size(output,1)==1
        output=output';
    end
else
    disp('wrong choice of der!');
end

end