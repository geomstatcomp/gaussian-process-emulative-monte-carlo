% This is to select an initial set of design points from some result %
addpath('../../');
addpath('../../AutoRefinement/');

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% simulated data
D=4;
N=3e6; muy=0;
sigma2y=1e4; sigma2theta=1;
y=muy+sqrt(sigma2y).*randn(N,1);

% load the result of HMC for a long run
% load('./result/Banana_HMC_D4_Nleap30__2014_12_31_ 1_ 9_32.mat');
% load('./result/Banana_LMC_D4_Nleap10__2014_12_31_ 2_18_53.mat');

% thinning
idx=floor(linspace(1,size(samp,1),1000));
% select design points
n=30;
De=selectnmaxdist(samp(idx,:),n);
% calculate the information on design points
% u_D=U(y,De,sigma2y,sigma2theta); du_D=U(y,De,sigma2y,sigma2theta,1);
[u_D,du_D,gFI_D]=geom(y,De,sigma2y,sigma2theta,[0,1,11]);

% build and refine emulator
samp=samp(idx,:); poten=poten(idx);
% emu=GPe(De,u_D,du_D,ones(D,1),gFI_D,sigma2theta,[],1,1);
emu=GPe_logCL(De,u_D,du_D,ones(D,1),gFI_D,sigma2theta,[],1,1);
for i=1:10
    disp([num2str(i),'-th batch to process...']);
    batchi=100*(i-1)+1:100*(i-1)+100;
    cand=samp(batchi,:); u_cand=poten(batchi);
%     emu=refine_MICE(emu,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),@(theta)gFI(y,theta,sigma2y),cand,10);
    emu=emu.refine(cand,u_cand,@(theta,opt)geom(y,theta,sigma2y,sigma2theta,opt),D+4,0);
end

% refined design
De=emu.De;
u_D=emu.u_D;du_D=emu.du_D;gFI_D=emu.gFI_D;
n=size(De,1);
% if isempty(du_D)
%     [u_D,du_D,gFI_D]=geom(De,Ufunc4eachobs,[0,1,11]);
%     emu.du_D=du_D;emu.gFI_D=gFI_D;
% end

% save the data
save(['Banana_D',num2str(D),'_Ndesg',num2str(n),'.mat'],'seed','D','N','y','muy','sigma2y','sigma2theta','n','De','u_D','du_D','gFI_D');