%%%% This is MCMC sampling by GPeHMC %%%%
clear;
addpath('../../');
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% simulated data
% D=4;
% N=3e6; muy=0;
% sigma2y=1e4; sigma2theta=1;
% y=muy+sqrt(sigma2y).*randn(N,1);
% or load data
load('Banana_D4_Ndesg40.mat');

% smallest potential energy
[theta_hat,u_nadir]=fminunc(@(theta)U(y,theta,sigma2y,sigma2theta),zeros(D,1));

% % Design information
% n=30;
% De=repmat(theta_hat',[n,1])+.2*randn(n,D);
% [u_D,du_D]=geom(y,De,sigma2y,sigma2theta,[0,1]);

% build emulator
% emu=GPe(De,u_D,du_D,ones(D,1),[],[],[],1,1);
emu=GPe_logCL(De,u_D,du_D,ones(D,1),[],[],[],1,1);

% sampling setting
TrjL = 1; Nleap = 30; stepsz = TrjL/Nleap;
regintvl = 20;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp,D);
poten = zeros(Nsamp,1);
logPi0Q = zeros(Nsamp,1);
accp_GPeMC = 0; % online GPeMC acceptance
accp_indep = 0; % online independence sampling acceptance
acpt_GPeMC = 0; % final GPeMC acceptance rate
acpt_indep = 0; % final independence sampling acceptance
Nreg=1; % total number of regenerations
regtime = zeros(ceil(Nsamp/regintvl),1); regtime(1)=1;% regeneration times
Entropy = zeros(ceil(Nsamp/regintvl),1);
Ndesg = zeros(ceil(Nsamp/regintvl),1); Ndesg(1)=n;
doREG = 0; % switch to stop regeneration test after entropy converges

% Initialize
theta = theta_hat; u = u_nadir;
du = U(y,theta,sigma2y,sigma2theta,1);
% entropy of the initial design
[~,~,~,~,~,mspe] = emu.pred(De,40);
Ent=sum(log(mspe));
Entropy(1)=gather(Ent);

disp(' ');
disp('Running GPeHMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance of GPeMC: ',num2str(accp_GPeMC/100)]); accp_GPeMC=0;
        if doREG
            disp(['Current acceptance of independence sampler: ',num2str(accp_indep/100)]); accp_indep=0;
        end
    end
    
    % 1st kernel: GPeHMC
    [theta,u,du,acpY_MC] = GPeHMC(theta,u,du,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),...
                                   @(theta)emu.pred(theta',1),stepsz,Nleap);
    % online acceptance rate for GPeMC
    accp_GPeMC = accp_GPeMC + acpY_MC;
    
    if doREG
    % 2nd kernel: independence sampler Q using Mixture of Gaussian
    % mix of Gaussians
    if Iter==1||Iter==regtime(Nreg)+1
%         K=D;
        K=size(emu.De,1);
%         [~,I_nearestK]=pdist2(emu.De,theta','euclidean','smallest',K);
%         [~,I_nearestK]=pdist2(emu.De,theta',@(XI,XJ)(sqrt(bsxfun(@minus,XI,XJ).^2 * exp(-emu.tau))),'smallest',K);
%         I_nearestK=datasample(1:size(emu.De,1),K,'replace',false,'weights',exp(-emu.u_D));
        I_nearestK=1:K;
        mu=emu.De(I_nearestK,:); u_mu = emu.u_D(I_nearestK); %logC=-min(u_mu);
        G_mu=repmat(eye(D),[1,1,K]);
        wts=(exp(-u_mu)./sum(exp(-u_mu)))'; % probabilities at design points are good weights
        G_mu=permute(reshape(G_mu,[K,D,D]),[2,3,1]);
        cholG_mu=reshape(cell2mat(arrayfun(@(i)chol(G_mu(:,:,i)),1:K,'UniformOutput', false)),[D,D,K]);
        mog.mu=mu; mog.cholinvSigma=cholG_mu; mog.wts=wts;
    end
    % prpose an indepence sample and do regeneration test
    logpiOq=-u-logpdf_MOG(theta',mu,[],cholG_mu,wts);
    if Iter==1
        logPi0Q(1)=logpiOq;
    end
    if Nreg==1
        logC=logpiOq;
%         logC=-u_nadir;
    end
    [theta_idp,u_idp,logpiOq_idp,acpY_idp,regY]=idp_MOG(mog,theta,u,logpiOq,@(theta)U(y,theta,sigma2y,sigma2theta),logC);
    if acpY_idp % theta_idp accepted
        % test regeneration
        if regY&&(Iter<=100||mod(Iter,regintvl)==0) % regeneration occurs
            disp([num2str(Nreg),'-th regeneration happens!']);
            % update design pool and emulator
            [emu,Ent]=emu.refine(samp(regtime(Nreg):Iter-1,:),poten(regtime(Nreg):Iter-1),@(theta,opt)geom(y,theta,sigma2y,sigma2theta,opt),10);
%             logPi0Q_btreg=logPi0Q(regtime(Nreg):Iter-1);offset=max(logPi0Q_btreg);logC=log(mode(exp(logPi0Q_btreg-offset)))+offset; % update logC
            Nreg=Nreg+1; regtime(Nreg)=Iter; Entropy(Nreg)=gather(Ent); Ndesg(Nreg)=size(emu.De,1);% record regeneration time and entropy
            % discard theta_idp, resampling from independence kernel Q
            acpY_idp=0;
            while ~acpY_idp
                [theta_idp,u_idp,logpiOq_idp,acpY_idp]=idp_MOG(mog,theta,u,logpiOq,@(theta)U(y,theta,sigma2y,sigma2theta),[],0);
            end
            % test if entropy converges by testing if linear regression slope is 0
            if Nreg>2
%                 s=cov(regtime(1:Nreg)/regintvl,Entropy(1:Nreg));
                s=cov(1:Nreg,Entropy(1:Nreg)); % more conservative
                df=Nreg-2; t=s(1,2)/sqrt((s(1,1)*s(2,2)-s(1,2)^2)/df);
                if tcdf(abs(t),df,'upper')<.05/2
                    doREG=0; % stop regeneration
                    disp(['Entropy reaches stationarity at iteration ', num2str(Iter),' after ',num2str(Nreg),' regnerations... Now stop adaption.']);
                end
%                 % try testing data
%                 testing=datasample(1:Iter-1,min([Iter-1,100]),'replace',false);
% %                 testing=1:Iter-1;
%                 mse=mean((emu.pred(samp(testing,:),0)-poten(testing)).^2);
%                 disp(['Mean Squared Error: ',num2str(mse)]);
%                 if mse<1e-2
%                     doREG=0; % stop regeneration
%                     disp(['MSE falls below threshold at iteration ', num2str(Iter),' after ',num2str(Nreg),' regnerations... Now stop adaption.']);
%                 end
            end
        end
        % update current values
        theta=theta_idp; u=u_idp; logpiOq=logpiOq_idp;
        [~,du] = emu.pred(theta',1);
    end
    % online acceptance rate for independence sampler
    accp_indep = accp_indep + acpY_idp;
    end
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    samp(Iter,:) = theta';
    poten(Iter) = u;
    if doREG
        logPi0Q(Iter) = logpiOq;
    end
    if Iter > NBurnIn
        acpt_GPeMC = acpt_GPeMC + acpY_MC;
        if doREG
            acpt_indep = acpt_indep + acpY_idp;
        end
    end
    
end

% burn in
samp=samp(NBurnIn+1:end,:);
poten=poten(NBurnIn+1:end);
logPi0Q=logPi0Q(NBurnIn+1:end);
% save results
time=toc;
acpt_GPeMC=acpt_GPeMC/(Nsamp-NBurnIn);
acpt_indep=acpt_indep/(regtime(Nreg)-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['Banana_GPeHMC_D',num2str(D),'_Nleap',num2str(Nleap),'_Ndesg',num2str(Ndesg(Nreg)),'__',curTime];
save(['result/',curfile,'.mat'],'stepsz','Nleap','samp','poten','logPi0Q','emu','acpt_GPeMC','regtime','Ndesg','Entropy','acpt_indep','time');
disp(' ');
disp(['Accpetance Rate of GPeHMC: ',num2str(acpt_GPeMC)]);
disp(['Accpetance Rate of indepence kernel: ',num2str(acpt_indep)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
idx=floor(linspace(1,size(samp,1),1e3));
% dim=unique([1,2,floor(D/2),D]);
dim=1:D;
fig1=figure(1); set(fig1,'pos',[0 800 900 400]);
subplot(1,2,1);
plot(samp(idx,dim));
subplot(1,2,2);
plotmatrix(samp(idx,dim));
% plot the posterior
% fig=figure('visible','off');
% contourmatrix_matlab(samp(:,dim));
% r=600;c=800;
% set(fig,'position',[0 0 c r]); 
% % set(gca,'position',[0 0 c+1 r+1]);
% set(gcf,'papersize',[8 6]);
% set(gcf,'paperposition',[0 0 8 6]);
% print(fig,'-dpdf',['./figure/',curfile,'.pdf']);