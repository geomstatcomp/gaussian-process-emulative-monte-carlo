% This is to compute the geometric quantities including gradient and metric %

function [u,du,gFI,xL_til]=geom(y,theta,sigma2y,sigma2theta,opt)
if nargin<5
    opt=0;
end
u=[];du=[];gFI=[];xL_til=[];

[n,D]=size(theta); N=length(y);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end
mu=sum([theta(:,1:2:D),theta(:,2:2:D).^2],2);
Err=bsxfun(@minus,mu,y');

if any(ismember([0,10,11],opt))
    xL = -Err.^2./sigma2y/2; % (n,N)
end
dxL=[];
if any(ismember([1,11],opt))
    dmu = ones(n,D); dmu(:,2:2:D) = 2.*theta(:,2:2:D); dmu = repmat(dmu(:),[1,N]); % (nD,N)
    dxL = -repmat(Err,[D,1]).*dmu./sigma2y; % (nD,N)
end

if any(opt==0)
    loglik = sum(xL,2);
    logpri = -sum(theta.^2,2)./sigma2theta/2;
    u = -(loglik+logpri);
end
if any(opt==1)
    dloglik = sum(dxL,2);
    dlogpri = -theta(:)./sigma2theta;
    du  = -(dloglik + dlogpri);
%     if size(du,1)==1
%         du=du';
%     end
end
if any(ismember([10,11],opt))
    xL_til=[xL;dxL];
    L_til=sum(xL_til,2);
    gFI=xL_til*xL_til'-L_til*L_til'./N;
end

end