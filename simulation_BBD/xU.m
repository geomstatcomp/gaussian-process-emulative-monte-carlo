%% energy function on each data item x%%
function [output] = xU(y,theta,sigma2y,sigma2theta,der)
if(nargin<5)
    der=0;
end

[n,D]=size(theta); N=length(y);
if D==1&&n>1
    theta=theta';
    [n,D]=size(theta);
end
mu=sum([theta(:,1:2:D),theta(:,2:2:D).^2],2);

if der==0
    logpri = repmat(-sum(theta.^2,2)./sigma2theta/2/N,[1,N]); % (n,N)
    loglik = -(bsxfun(@minus,mu,y')).^2./sigma2y/2; % (n,N)
    output = -(loglik+logpri);
elseif der==1
    dlogpri = -theta./sigma2theta; dlogpri = repmat(dlogpri(:)./N,[1,N]); % (nD,N)
    dmu = ones(n,D); dmu(:,2:2:D) = 2.*theta(:,2:2:D); dmu = repmat(dmu(:),[1,N]); % (nD,N)
    dloglik = -repmat(bsxfun(@minus,mu,y'),[D,1]).*dmu./sigma2y; % (nD,N)
    output  = -(dloglik+dlogpri);
else
    disp('wrong choice of der!');
end

end