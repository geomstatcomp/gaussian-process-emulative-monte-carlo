%%%% This is MCMC sampling by RWM %%%%
clear;
addpath('../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% simulated data
D=4;
N=3e6; muy=0;
sigma2y=1e4; sigma2theta=1;
y=muy+sqrt(sigma2y).*randn(N,1);

% smallest potential energy
[theta_hat,u_nadir]=fminunc(@(theta)U(y,theta,sigma2y,sigma2theta),zeros(D,1));

% sampling setting
TrjL = .03; Nleap = 1; stepsz = TrjL/Nleap;

% allocation to save
Nsamp = 15000; NBurnIn = 5000;
samp = zeros(Nsamp-NBurnIn,D);
poten = zeros(Nsamp-NBurnIn,1);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate

% Initialize
theta = theta_hat; u = u_nadir;

disp(' ');
disp('Running RWM...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with RWM
    [theta,u,acpt_idx] = RWM(theta,u,@(theta)U(y,theta,sigma2y,sigma2theta),stepsz);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        poten(Iter-NBurnIn) = u;
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['Banana_RWM_D',num2str(D),'_Nleap',num2str(Nleap),'__', curTime];
save(['result/',curfile,'.mat'],'stepsz','Nleap','samp','poten','acpt','time');
disp(' ');
disp(['Accpetance Rate of RWM: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
idx=floor(linspace(1,size(samp,1),1e3));
% dim=unique([1,2,floor(D/2),D]);
dim=1:D;
fig1=figure(1); set(fig1,'pos',[0 800 900 400]);
subplot(1,2,1);
plot(samp(idx,dim));
subplot(1,2,2);
plotmatrix(samp(idx,dim));
% plot the posterior
% fig=figure('visible','off');
% contourmatrix_matlab(samp(:,dim));
% r=600;c=800;
% set(fig,'position',[0 0 c r]); 
% % set(gca,'position',[0 0 c+1 r+1]);
% set(gcf,'papersize',[8 6]);
% set(gcf,'paperposition',[0 0 8 6]);
% print(fig,'-dpdf',['./figure/',curfile,'.pdf']);