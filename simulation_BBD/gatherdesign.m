% This is to gather design points for BBD %
clear;
addpath('../sampler/');

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2014);
RandStream.setGlobalStream(seed);

% simulated data
D=4;
N=3e6; muy=0;
sigma2y=1e4; sigma2theta=1;
y=muy+sqrt(sigma2y).*randn(N,1);

%%%% gather design points %%%%

% smallest potential energy
[theta,u]=fminunc(@(theta)U(y,theta,sigma2y,sigma2theta),zeros(D,1));
du = U(y,theta,sigma2y,sigma2theta,1);
% [G,invG,~,ChS1,dlogrtG] = Met(theta,N,sigma2y,sigma2theta,[0,-1,3,-3]);
% met.G=G; met.cholinvG=chol(invG); met.ChS1=ChS1;
% met.dphi=du+dlogrtG;

% strat the exploration
Ndesg=40; Nexpl=0;
De=zeros(Ndesg,D); u_D=zeros(Ndesg,1); du_D=zeros(Ndesg,D);
while Nexpl<Ndesg
    [theta,u,du,acpt_idx]=HMC(theta,u,du,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),1/30,30);
%     [theta,u,met,acpt_idx] = LMC(theta,u,met,@(theta,der)U(y,theta,sigma2y,sigma2theta,nargin==2),...
%                                  @(theta)Met(theta,N,sigma2y,sigma2theta,[0,-1,3,-3]),1/10,10);
    if acpt_idx&&pdist2(De(1:max([1,Nexpl]),:),theta','euclidean','smallest',1)>1
        Nexpl=Nexpl+1;
        De(Nexpl,:)=theta; u_D(Nexpl)=u; du_D(Nexpl,:)=du;
        disp([num2str(Nexpl),'-th design point found!']);
    end
end
n=Ndesg;

% plot the design
plotmatrix(De);

% design information
[~,~,gFI_D]=geom(y,De,sigma2y,sigma2theta,11);

% save the design
save(['Banana_D',num2str(D),'_Ndesg',num2str(Ndesg),'.mat'],'D','N','muy','sigma2y','sigma2theta','y','Ndesg','n','De','u_D','du_D','gFI_D');